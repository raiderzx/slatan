<?php
	class productClass{
		public $PRDCODE;
		public $CODE;
		public $PRDNAME;
		public $PRDDESC;
		public $PRDCAT;
		public $UNIT;
		public $PRDIMG_PATH;
		public $PRICE;
		public $LAST_COST;
		public $AVG_COST;
		public $STATUS;

		public function queryAllProduct(){
			$sql = "SELECT a.`PRDCODE`, a.`CODE`, a.`PRDCAT`, b.`CATNAME`, a.`PRDNAME`, a.`UNIT`, a.`PRICE` FROM masproduct a LEFT JOIN masprdcats b ON a.PRDCAT = b.PRDCAT WHERE STATUS = '1'";
			$result = mysql_query($sql);
			$numRows = mysql_num_rows($result);

			if($numRows == 0){
				echo "<h4>ยังไม่มีรายการสั่งซื้อสินค้า</h4>";
			}else{
				echo "<table id='example1' class='table table-bordered table-striped table-hover table-responsive'>";
					echo "<thead>";
						echo "<tr>";
							echo "<th>CODE</th>";
							echo "<th>หมวดสินค้า</th>";
							echo "<th>ชื่อสินค้า</th>";
							//echo "<th>หน่วย</th>";
							echo "<th>ราคา</th>";
							echo "<th>ดูข้อมูล</th>";
							echo "<th>เลือกสินค้า</th>";
						echo "</tr>";
					echo "</thead>";
					echo "<tbody>";
						while($rows = mysql_fetch_array($result)){
							$PRDCODE = $rows["PRDCODE"];
							$CODE = $rows["CODE"];
							$PRDCAT = $rows["PRDCAT"];
							$CATNAME = $rows["CATNAME"];
							$PRDNAME = $rows["PRDNAME"];
							$UNIT = $rows["UNIT"];
							$PRICE = $rows["PRICE"];

							echo "<tr>";
								echo "<td>".$CODE."<input type='hidden' value=\"$PRDCODE\">"."</td>";
								echo "<td>$CATNAME</td>";
								echo "<td>$PRDNAME</td>";
								//echo "<td>$UNIT</td>";
								echo "<td>".number_format($PRICE, 2)."</td>";
								//echo "<td>$PRICE</td>";
								echo "<td><a class='btn btn-default' href=\"productDisplay.php?PRDCODE=$PRDCODE\" target='_blank'><i class='fa fa-eye'></i> ดูข้อมูล</a></td>";
								echo "<td><button class='btn btn-success prdcode' data-code=\"$CODE\"><i class='fa fa-cart-plus'> เลือก</button></td>";
							echo "</tr>";
						}
					echo "</tbody>";
					echo "<tfoot>";
						echo "<tr>";
							echo "<th>CODE</th>";
							echo "<th>หมวดสินค้า</th>";
							echo "<th>ชื่อสินค้า</th>";
							//echo "<th>หน่วย</th>";
							echo "<th>ราคา</th>";
							echo "<th>ดูข้อมูล</th>";
							echo "<th>เลือกสินค้า</th>";
						echo "</tr>";
					echo "</tfoot>";
				echo "</table>";
			}
		}

		public function queryAllProductWithEditBtn(){
			$sql = "SELECT a.`PRDCODE`, a.`CODE`, a.`PRDCAT`, b.`CATNAME`, a.`PRDNAME`, a.`UNIT`, a.`PRICE`, a.`STATUS` FROM masproduct a LEFT JOIN masprdcats b ON a.PRDCAT = b.PRDCAT";
			$result = mysql_query($sql);
			$numRows = mysql_num_rows($result);

			if($numRows == 0){
				echo "<h4>ยังไม่มีรายการสินค้า</h4>";
			}else{
				echo "<table id='example1' class='table table-bordered table-striped table-hover'>";
					echo "<thead>";
						echo "<tr>";
							echo "<th>CODE</th>";
							echo "<th>หมวดสินค้า</th>";
							echo "<th>ชื่อสินค้า</th>";
							echo "<th>หน่วย</th>";
							echo "<th>ราคา</th>";
							echo "<th>สถานะการเปิดใช้งาน</th>";
							echo "<th>action</th>";
						echo "</tr>";
					echo "</thead>";
					echo "<tbody>";
						while($rows = mysql_fetch_array($result)){
							$PRDCODE = $rows["PRDCODE"];
							$CODE = $rows["CODE"];
							$PRDCAT = $rows["PRDCAT"];
							$CATNAME = $rows["CATNAME"];
							$PRDNAME = $rows["PRDNAME"];
							$UNIT = $rows["UNIT"];
							$PRICE = $rows["PRICE"];
							$STATUS = $rows["STATUS"];

							echo "<tr class='prdcode' data-code=\"$CODE\">";
								echo "<form action=''>";
								echo "<td>".$CODE."</td>";
								echo "<td>$CATNAME</td>";
								echo "<td>$PRDNAME</td>";
								echo "<td>$UNIT</td>";
								
								echo "<td>".number_format($PRICE, 2)."</td>";
								
								if($STATUS == 0){
									echo "<td>ปิดการใช้งาน</td>";
								}else{
									echo "<td>เปิดการใช้งาน</td>";
								}
								
								echo "<td><a class='btn btn-warning' href='prdManageEdit.php?PRDCODE=$PRDCODE'>แก้ไขข้อมูล</a></td>";
								echo "</form>";
							echo "</tr>";
						}
					echo "</tbody>";
					echo "<tfoot>";
						echo "<tr>";
							echo "<th>CODE</th>";
							echo "<th>หมวดสินค้า</th>";
							echo "<th>ชื่อสินค้า</th>";
							echo "<th>หน่วย</th>";
							echo "<th>ราคา</th>";
							echo "<th>action</th>";
						echo "</tr>";
					echo "</tfoot>";
				echo "</table>";
			}
		}

		//ใช้ในหน้าดูข้อมูลสินค้า
		public function selectGalleryPic($PRDCODE){
			$sql = "SELECT PRD_IMAGE_ID, PRD_IMAGE_PATH FROM prdimage WHERE PRDCODE = '$PRDCODE'";
			$result= mysql_query($sql);
			$numRows = mysql_num_rows($result);
			
			if($numRows == 0){
				echo "<h3>สินค้านี้ยังไม่มีรูปภาพ</h3>";
			}else{
				echo "<div class='row'>";
					echo "<div class='col-xs-2'>";
						while($rows = mysql_fetch_array($result)){
							$PRD_IMAGE_PATH = $rows["PRD_IMAGE_PATH"];

							//echo "<img src=\"prdPic/$PRD_IMAGE_PATH\" width='50px' height='50px'>"; data-toggle='magnify' 
							echo "<img class='img-rounded img-responsive toggle-img' data-path='$PRD_IMAGE_PATH' src=\"../prdPic/$PRD_IMAGE_PATH\">";
							echo "<br>";
						}
					echo "</div>";
					echo "<div class='col-xs-10'>";
						echo "<div class='mg-image'>";
						echo "<img class='img-rounded img-responsive' data-toggle='magnify' src=\"../prdPic/$PRD_IMAGE_PATH\" id='picZoom'>";
						echo "</div>";
					echo "</div>";
				echo "</div>";	

				/*
				//carousel
				echo "<div id='carousel-example-generic' class='carousel slide' data-ride='carousel'>";
					echo "<ol class='carousel-indicators'>";
						for($i = 1; $i <= $numRows; $i++){
							if($i == 1){
				  				echo "<li data-target='#carousel-example-generic' data-slide-to=\"$i\" class='active'></li>";
					  		}else{
					  			echo "<li data-target='#carousel-example-generic' data-slide-to=\"$i\"></li>";
					  		}
						}
					echo "</ol>";

					$i = 0;
					echo "<div class='carousel-inner' role='listbox'>";
						while($rows = mysql_fetch_array($result)){
							$PRD_IMAGE_PATH = $rows["PRD_IMAGE_PATH"];

							if($i == 0){
					  			echo "<div class='item active'>";
							      	echo "<img src=\"prdPic/$PRD_IMAGE_PATH\" alt='...'>";
							      	echo "<div class='carousel-caption'>";
							        //...
							      	echo "</div>";
							    echo "</div>";
					  		}else{
					  			echo "<div class='item'>";
							      	echo "<img src=\"prdPic/$PRD_IMAGE_PATH\" alt='...''>";
							      	echo "<div class='carousel-caption'>";
							        //...
							      	echo "</div>";
							    echo "</div>";
					  		}
					  		$i++;
					  	}
					echo "</div>";

					echo "<a class='left carousel-control' href='#carousel-example-generic' role='button' data-slide='prev'>";
					    echo "<span class='glyphicon glyphicon-chevron-left' aria-hidden='true'></span>";
					    echo "<span class='sr-only'>Previous</span>";
				  	echo "</a>";
				  	echo "<a class='right carousel-control' href='#carousel-example-generic' role='button' data-slide='next'>";
					    echo "<span class='glyphicon glyphicon-chevron-right' aria-hidden='true'></span>";
					    echo "<span class='sr-only'>Next</span>";
				  	echo "</a>";
				echo "</div>";
				*/

				/*
				//list Pic
				while($rows = mysql_fetch_array($result)){
					$PRD_IMAGE_PATH = $rows["PRD_IMAGE_PATH"];

					echo "<div class='col-xs-12 img-hover'>";
						//echo "<div class='thumbnail'>";
							echo "<img class='img-thumbnail img-responsive' src=\"prdPic/$PRD_IMAGE_PATH\" width='50%'>";
						//echo "</div>";
					echo "</div>";
				}
				*/
			}
		}

		public function selectToEdit($PRDCODE){
			$sql = "SELECT * FROM masproduct WHERE PRDCODE = '$PRDCODE'";
			$result = mysql_query($sql);
			$numRows = mysql_num_rows($result);

			if($numRows == 0){
				echo '<script type="text/javascript">alert("ไม่มีสินค้ารหัสดังกล่าว");</script>';
			}else{
				while($rows = mysql_fetch_array($result)){
					$PRDCODE = $rows["PRDCODE"];
					$CODE = $rows["CODE"];
					$PRDNAME = $rows["PRDNAME"];
					$PRDDESC = $rows["PRDDESC"];
					$PRDCAT = $rows["PRDCAT"];
					//$PRDIMG_PATH = $rows["PRDIMG_PATH"];
					$LAST_COST = $rows["LAST_COST"];
					$AVG_COST = $rows["AVG_COST"];
					$UNIT = $rows["UNIT"];
					$PRICE = $rows["PRICE"];
					$STATUS = $rows["STATUS"];
				}
				$this->PRDCODE = $PRDCODE;
				$this->CODE = $CODE;
				$this->PRDNAME = $PRDNAME;
				$this->PRDDESC = $PRDDESC;
				$this->PRDCAT = $PRDCAT;
				//$this->PRDIMG_PATH = $PRDIMG_PATH;
				$this->LAST_COST = $LAST_COST;
				$this->AVG_COST = $AVG_COST;
				$this->UNIT = $UNIT;
				$this->PRICE = $PRICE;
				$this->STATUS = $STATUS;
			}
		}
		
		//หน้าแก้ไขสินค้า
		public function selectPRDPic($PRDCODE){
			$sql = "SELECT PRD_IMAGE_ID, PRD_IMAGE_PATH FROM prdimage WHERE PRDCODE = '$PRDCODE'";
			$result= mysql_query($sql);
			$numRows = mysql_num_rows($result);
			
			if($numRows == 0){
				echo "ยังไม่มีรูปภาพสำหรับสินค้านี้";
			}else{
				$i = 1;
				while($rows = mysql_fetch_array($result)){
					$PRD_IMAGE_ID = $rows["PRD_IMAGE_ID"];
					$PRD_IMAGE_PATH = $rows["PRD_IMAGE_PATH"];
					
					if(($i % 3) == 1){
						//echo "1=".$i%3;
						echo "<div class='row'>";
							echo "<div class='col-sm-4'>";
								echo "<form action='' method='post'>";
									echo "<div class='thumbnail'>";
										echo "<img class='img-thumbnail img-responsive' src=\"../prdPic/$PRD_IMAGE_PATH\">";
										echo "<div class='caption'>";
											echo "<input type='submit' class='btn btn-danger btn-block' value='ลบรูปภาพ' name='delPRDIMG' onclick=\"return confirm('ท่านต้องการลบรูปสินค้าดังกล่าวใช่หรือไม่?')\">";
											echo "<input type='hidden' value=\"$PRD_IMAGE_ID\" name='picID'>";
										echo "</div>";
									echo "</div>";
								echo "</form>";
							echo "</div>";
							
							//$i = $i + 1;
					}elseif (($i % 3) == 2){
						//echo "1=".$i%3;
						echo "<div class='col-sm-4'>";
							echo "<form action='' method='post'>";
								echo "<div class='thumbnail'>";
									echo "<img class='img-thumbnail img-responsive' src=\"../prdPic/$PRD_IMAGE_PATH\">";
									echo "<div class='caption'>";
										echo "<input type='submit' class='btn btn-danger btn-block' value='ลบรูปภาพ' name='delPRDIMG' onclick=\"return confirm('ท่านต้องการลบรูปสินค้าดังกล่าวใช่หรือไม่?')\">";
										echo "<input type='hidden' value=\"$PRD_IMAGE_ID\" name='picID'>";
									echo "</div>";
								echo "</div>";
							echo "</form>";
						echo "</div>";
						
						//$i = $i + 1;
					}elseif (($i % 3) == 0){
							//echo "1=".$i%3;
							echo "<div class='col-sm-4'>";
								echo "<form action='' method='post'>";
									echo "<div class='thumbnail'>";
										echo "<img class='img-thumbnail img-responsive' src=\"../prdPic/$PRD_IMAGE_PATH\">";
										echo "<div class='caption'>";
											echo "<input type='submit' class='btn btn-danger btn-block' value='ลบรูปภาพ' name='delPRDIMG' onclick=\"return confirm('ท่านต้องการลบรูปสินค้าดังกล่าวใช่หรือไม่?')\">";
											echo "<input type='hidden' value=\"$PRD_IMAGE_ID\" name='picID'>";
										echo "</div>";
									echo "</div>";
								echo "</form>";
							echo "</div>";
						echo "</div>";
						
						//$i = $i + 1;
					}
					
					$i++;
					/*
					if($i == $numRows AND ($i % 3) != 0){
						echo "</div>";
					}
					*/
				}
			}
		}

		public function prdcatCmbbox($selectedPRDCAT){
			$sql = "SELECT * FROM masprdcats";
			$result = mysql_query($sql);

			while($rows = mysql_fetch_array($result)){
				$PRDCAT = $rows["PRDCAT"];
				$CATNAME = $rows["CATNAME"];

				echo "<option value='$PRDCAT'>$CATNAME</option>";
			}
		}

		public function prdcatCmbboxSelected($selectedPRDCAT){
			$sql = "SELECT * FROM masprdcats";
			$result = mysql_query($sql);

			while($rows = mysql_fetch_array($result)){
				$PRDCAT = $rows["PRDCAT"];
				$CATNAME = $rows["CATNAME"];

				if($PRDCAT == $selectedPRDCAT){
					echo "<option value='$PRDCAT' selected>$CATNAME</option>";
				}else{
					echo "<option value='$PRDCAT'>$CATNAME</option>";
				}
			}
		}

		public function editPrdinfo($PRDCODE, $CODE, $PRDNAME, $PRDDESC, $PRDCAT, $UNIT, $PRICE, $LAST_COST, $AVG_COST, $STATUS){
			try {
				$sql = "UPDATE masproduct SET CODE = '$CODE', PRDNAME = '$PRDNAME', PRDDESC = '$PRDDESC', PRDCAT = '$PRDCAT', UNIT = '$UNIT', PRICE = '$PRICE', LAST_COST = '$LAST_COST', AVG_COST = '$AVG_COST', STATUS = '$STATUS' WHERE PRDCODE = '$PRDCODE'";
				$result = mysql_query($sql);
				return true;
			} catch (Exception $e) {
				echo 'Caught exception: ',  $e->getMessage(), "\n";
				return false;
			}
			
		}
		
		public function addPicToDB($PRDCODE, $PRD_IMAGE_PATH){
			try {
				$sql = "INSERT INTO prdimage (PRDCODE, PRD_IMAGE_PATH, STATUS) VALUES ('$PRDCODE', '$PRD_IMAGE_PATH', '1');";
				$result = mysql_query($sql);
				return true;
			} catch (Exception $e) {
				return false;
			}
		}

		public function addPrdinfo($CODE, $PRDNAME, $PRDDESC, $PRDCAT, $UNIT, $PRICE, $LAST_COST, $AVG_COST, $STATUS){
			$sql = "SELECT CODE FROM masproduct WHERE CODE = '$CODE'";
			$result = mysql_query($sql);
			$numRows = mysql_num_rows($result);

			if($numRows > 0){
				echo "ไม่สามารถใช้ CODE สินค้านี้ได้ เนื่องจากมีอยู่ในระบบอยู่แล้ว";
				return false;
			}else{
				$sql2 = "INSERT INTO masproduct (CODE, PRDNAME, PRDDESC, PRDCAT, UNIT, PRICE, LAST_COST, AVG_COST, STATUS) VALUES ('$CODE', '$PRDNAME', '$PRDDESC', '$PRDCAT', '$UNIT', '$PRICE', '$LAST_COST', '$AVG_COST', '$STATUS');";
				$result2 = mysql_query($sql2);
				
				$justInsertPRDCODE = mysql_insert_id();
				
				return $justInsertPRDCODE;
				//$sql3 = "INSERT INTO prdimage (PRDCODE, PRD_IMAGE_PATH, STATUS) VALUES ('$justInsertPRDCODE', '$PRDIMG_PATH', '1');";
				//return true;
			}
		}
	}
?>