<?php
	class dateManage{
		public function convertDateBD($dateTime){
			$dateTimeExplode = explode(" ", $dateTime);
			$date = $dateTimeExplode[0];
			$time = $dateTimeExplode[1];

			$dateExplode = explode('-', $date);
			$timeExplode = explode(':', $time);

			//convert year
			$bdYear = $dateExplode[0] + 543;
			//convert month
			switch ($dateExplode[1]) {
				case 01:
				case 1:
					$bdMonth = 'มกราคม';
					break;
				case 02:
				case 2:
					$bdMonth = 'กุมภาพันธ์';
					break;
				case 03:
				case 3:
					$bdMonth = 'มีนาคม';
					break;
				case 04:
				case 4:
					$bdMonth = 'เมษายน';
					break;
				case 05:
				case 5:
					$bdMonth = 'พฤษภาคม';
					break;
				case 06:
				case 6:
					$bdMonth = 'มิถุนายน';
					break;
				case 07:
				case 7:
					$bdMonth = 'กรกฎาคม';
					break;
				case 08:
				case 8:
					$bdMonth = 'สิงหาคม';
					break;
				case 09:
				case 9:
					$bdMonth = 'กันยายน';
					break;
				case 10:
					$bdMonth = 'ตุลาคม';
					break;
				case 11:
					$bdMonth = 'พฤศจิกายน';
					break;
				case 12:
					$bdMonth = 'ธันวาคม';
					break;
			}
			$bdDate = $dateExplode[2];
			$time = $time;
			
			return $bdDate." ".$bdMonth." ".$bdYear;
		}
	}

?>