<?php
	Class userClass{
		public $USER_ID;
		public $USER_PASSWORD;
		public $USER_NAME;
		public $USER_LNAME;
		public $USER_COMPANY;
		public $USER_ADDRESS;
		public $USER_TEL;
		public $USER_EMAIL;
		public $USER_EDIT_DATE;
		public $STATUS;
		
		public function userList(){
			$sql = "SELECT `USER_ID`, `USER_ACCOUNT`, CONCAT_WS(' ', USER_NAME, USER_LNAME) as 'NAME', `USER_COMPANY`, `USER_TEL`, `USER_EMAIL`, `STATUS` FROM user";
			$result=mysql_query($sql);
			$numRow = mysql_num_rows($result);
			
			if($numRow == 0){
				echo "<script language='javascript'>";
				echo "alert('ยังไม่มีผู้ใช้งานอยู่ในระบบ')";
				echo  "</script>";
				//echo "<script language=\"javascript\">window.location='".basename($_SERVER['PHP_SELF'])."'</script>";
			}else{
				echo "<table id='example1' class='table table-bordered table-striped table-hover'>";
					echo "<thead>";
						echo "<tr>";
							echo "<th>รหัสผู้ใช้</th>";
							echo "<th>USERNAME</th>";
							echo "<th>ชื่อผู้ใช้</th>";
							echo "<th>หน่วยงาน</th>";
							echo "<th>โทรศัพท์</th>";
							echo "<th>อีเมล์</th>";
							echo "<th>สถานะ</th>";
							echo "<th>action</th>";
						echo "</tr>";
					echo "</thead>";
					echo "<tbody>";
				
					while($fetcharr=mysql_fetch_array($result)){
						$USER_ID = $fetcharr['USER_ID'];
						$USER_ACCOUNT = $fetcharr['USER_ACCOUNT'];
						$NAME = $fetcharr['NAME'];
						$USER_COMPANY = $fetcharr['USER_COMPANY'];
						$USER_TEL = $fetcharr['USER_TEL'];
						$USER_EMAIL = $fetcharr['USER_EMAIL'];
						$STATUS = $fetcharr['STATUS'];
						
						echo "<tr>";
							echo "<td>".$USER_ID."</td>";
							echo "<td>$USER_ACCOUNT</td>";
							echo "<td>$NAME</td>";
							echo "<td>$USER_COMPANY</td>";
							echo "<td>$USER_TEL</td>";
							echo "<td>$USER_EMAIL</td>";
							if($STATUS == 1){
								echo "<td class='success'><center>เปิดใช้งาน</center></td>";
							}else{
								echo "<td class='danger'><center>ระงับการใช้งาน</center></td>";
							}
							echo "<td><a href='userEdit.php?USER_ID=$USER_ID' class='btn btn-warning'><i class='fa fa-eye'> แก้ไขข้อมูล</a></td>";
						echo "</tr>";
					}
					echo "</tbody>";
					echo "<tfoot>";
						echo "<tr>";
							echo "<th>รหัสผู้ใช้</th>";
							echo "<th>USERNAME</th>";
							echo "<th>ชื่อผู้ใช้</th>";
							echo "<th>หน่วยงาน</th>";
							echo "<th>โทรศัพท์</th>";
							echo "<th>อีเมล์</th>";
							echo "<th>สถานะ</th>";
							echo "<th>action</th>";
						echo "</tr>";
					echo "</tfoot>";
				echo "</table>";
			}
		}
		
		public function selectUserToEdit($USER_ID){
			$sql = "SELECT * FROM user WHERE USER_ID = '$USER_ID'";
			$result = mysql_query($sql);
			$numRows = mysql_num_rows($result);

			if($numRows == 0){
				echo '<script type="text/javascript">alert("ไม่มีผู้ใช้ดังกล่าวในระบบ");</script>';
			}else{
				while($rows = mysql_fetch_array($result)){
					$USER_ID = $rows["USER_ID"];
					$USER_ACCOUNT = $rows["USER_ACCOUNT"];
					$USER_PASSWORD = $rows["USER_PASSWORD"];
					$USER_NAME = $rows["USER_NAME"];
					$USER_LNAME = $rows["USER_LNAME"];
					$USER_COMPANY = $rows["USER_COMPANY"];
					$USER_ADDRESS = $rows["USER_ADDRESS"];
					$USER_TEL = $rows["USER_TEL"];
					$USER_EMAIL = $rows["USER_EMAIL"];
					$USER_ADD_DATE = $rows["USER_ADD_DATE"];
					$STATUS = $rows["STATUS"];
				}
				$this->USER_ID = $USER_ID;
				$this->USER_ACCOUNT = $USER_ACCOUNT;
				$this->USER_PASSWORD = $USER_PASSWORD;
				$this->USER_NAME = $USER_NAME;
				$this->USER_LNAME = $USER_LNAME;
				$this->USER_COMPANY = $USER_COMPANY;
				$this->USER_ADDRESS = $USER_ADDRESS;
				$this->USER_TEL = $USER_TEL;
				$this->USER_EMAIL = $USER_EMAIL;
				$this->USER_ADD_DATE = $USER_ADD_DATE;
				$this->STATUS = $STATUS;
			}
		}
		
		public function editUserInfo($USER_ID, $USER_PASSWORD, $USER_NAME, $USER_LNAME, $USER_COMPANY, $USER_ADDRESS, $USER_TEL, $USER_EMAIL, $STATUS){
			$sql = "UPDATE user SET USER_PASSWORD = '$USER_PASSWORD', USER_NAME = '$USER_NAME', USER_LNAME = '$USER_LNAME', USER_COMPANY = '$USER_COMPANY', USER_ADDRESS = '$USER_ADDRESS', USER_TEL = '$USER_TEL', USER_EMAIL = '$USER_EMAIL', USER_EDIT_DATE = NOW(), STATUS = '$STATUS' WHERE USER_ID = '$USER_ID'";
			echo "alert('แก้ไขข้อมูลผู้ใช้เรียบร้อยแล้ว')";
			$result = mysql_query($sql);
			echo "<script language='javascript'>";
			echo  "</script>";
		}

		public function addUserInfo($USER_ACCOUNT, $USER_PASSWORD, $USER_NAME, $USER_LNAME, $USER_COMPANY, $USER_ADDRESS, $USER_TEL, $USER_EMAIL){
			$sql = "INSERT INTO user (USER_ACCOUNT, USER_PASSWORD, USER_NAME, USER_LNAME, USER_COMPANY, USER_ADDRESS, USER_TEL, USER_EMAIL, USER_ADD_DATE, USER_EDIT_DATE, STATUS) VALUES ('$USER_ACCOUNT', '$USER_PASSWORD', '$USER_NAME', '$USER_LNAME', '$USER_COMPANY', '$USER_ADDRESS', '$USER_TEL', '$USER_EMAIL', NOW(), null, '1');";
			$result = mysql_query($sql);
			echo "<script language='javascript'>";
			echo "alert('เพิ่มผู้ใช้เรียบร้อยแล้ว')";
			echo  "</script>";
		}

		public function adminLogin($USER_ACCOUNT, $USER_PASSWORD){
			$sql = "SELECT * FROM admin WHERE `USER_ACCOUNT` = '$USER_ACCOUNT' AND `USER_PASSWORD` = '$USER_PASSWORD';";
			$result=mysql_query($sql);
			$numRow = mysql_num_rows($result);
			
			if($numRow == 0){
				echo "<script language='javascript'>";
				echo "alert('ท่านกรอก username หรือ password ไม่ถูกต้อง โปรดกรุณาระบุใหม่อีกครั้ง')";
				echo  "</script>";
				echo "<script language=\"javascript\">window.location='".basename($_SERVER['PHP_SELF'])."'</script>";
			}else{
				while($fetcharr=mysql_fetch_array($result)){
					$ADMIN_USER_ID = $fetcharr['USER_ID'];
					$ADMIN_USER_ACCOUNT = $fetcharr['USER_ACCOUNT'];
					$ADMIN_USER_PASSWORD = $fetcharr['USER_PASSWORD'];
					$ADMIN_USER_NAME = $fetcharr['USER_NAME'].' '.$fetcharr['USER_LNAME'];
					$ADMIN_STATUS = $fetcharr['STATUS'];
				}
				if($ADMIN_STATUS == 1){
					$_SESSION['ADMIN_USER_ID'] = $ADMIN_USER_ID;
					$_SESSION['ADMIN_USER_ACCOUNT'] = $ADMIN_USER_ACCOUNT;
					$_SESSION['ADMIN_USER_NAME'] = $ADMIN_USER_NAME;
					$_SESSION['ADMIN_STATUS'] = $ADMIN_STATUS;
					echo "<script language=\"javascript\">window.location='".basename("dashboard.php")."'</script>";
				}else{
					echo "<script language='javascript'>";
					echo "alert('บัญชีใช้งานของท่านถูกระงับการใช้งาน โปรดติดต่อผู้ดูแลระบบ')";
					echo  "</script>";
					echo "<script language=\"javascript\">window.location='".basename($_SERVER['PHP_SELF'])."'</script>";
				}
				
			}
		}
		
		public function getUserInfo($USER_ID){
			$sql = "SELECT USER_NAME, USER_LNAME, USER_ADDRESS, USER_TEL, USER_EMAIL FROM user WHERE USER_ID = '$USER_ID'";
			$result = mysql_query($sql);
			$numRow = mysql_num_rows($result);
			
			if($numRow == 0){
				echo "<script language='javascript'>";
				echo "alert('ไม่พบรายละเอียดข้อมูลของผู้ใช้รายนี้')";
				echo  "</script>";
				echo "<script language=\"javascript\">window.location='".basename($_SERVER['PHP_SELF'])."'</script>";
			}else{
				while($fetcharr=mysql_fetch_array($result)){
					$USER_NAME = $fetcharr['USER_NAME'];
					$USER_LNAME = $fetcharr['USER_LNAME'];
					$USER_ADDRESS = $fetcharr['USER_ADDRESS'];
					$USER_TEL = $fetcharr['USER_TEL'];
					$USER_EMAIL = $fetcharr['USER_EMAIL'];
				}
				
				$this->NAME = $USER_NAME;
				$this->LNAME = $USER_LNAME;
				$this->ADDRESS = $USER_ADDRESS;
				$this->TEL = $USER_TEL;
				$this->EMAIL = $USER_EMAIL;
			}
		}
	}
?>