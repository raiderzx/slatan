<?php
	Class picManageClass{
		public function uploadPrdPic($fileNameTmp, $fileNameRaw, $prdCode){
			$subFileType = explode(".", $fileNameRaw);

			$fileType = $subFileType[1];

			$randomFileName = mt_rand(1, 99);
			$fileName = $prdCode.'r'.$randomFileName;

			if($fileType == 'jpg' OR $fileType == 'JPG' OR $fileType == 'jpeg' OR $fileType == 'JPEG' OR $fileType == 'PNG' OR $fileType == 'png'){
				if(move_uploaded_file($fileNameTmp, "../prdPic/".$fileName.'.'.$fileType)){
					return $fileName.'.'.$fileType;
					//echo $fileName.'.'.$fileType.'hey';
				}else{
					echo "<script language='javascript'>";
					echo "alert('เกิดข้อผิดพลาดในการอัพโหลดไฟล์ โปรดลองใหม่อีกครั้ง')";
					echo  "</script>";
					return "";
				}
			}else{
				echo "<script language='javascript'>";
				echo "alert('โปรดอัพโหลดเฉพาะรูปภาพนามสกุล .jpg หรือ .png เท่านั้น')";
				echo  "</script>";
			}
		}

		public function deletePrdPic($prdCode, $prdImageID){
			$sql = "SELECT PRD_IMAGE_PATH FROM prdimage WHERE PRDCODE = '$prdCode' AND PRD_IMAGE_ID = '$prdImageID'";
			$result = mysql_query($sql);
			while($fetcharr=mysql_fetch_array($result)){
            	$PRD_IMAGE_PATH = $fetcharr['PRD_IMAGE_PATH'];

            	$pathToDeleteFile = '../prdPic/'.$PRD_IMAGE_PATH;
            	unlink("$pathToDeleteFile");
            }

			$sql2 = "DELETE FROM prdimage WHERE PRD_IMAGE_ID = '$prdImageID'";
			$result2 = mysql_query($sql2);
		}

		public function uploadPOConfirmPic($fileNameTmp, $fileNameRaw, $orderid){
			$subFileType = explode(".", $fileNameRaw);

			$fileType = $subFileType[1];

			$randomFileName = mt_rand(1, 99);
			$fileName = $orderid.'r'.$randomFileName;

			if($fileType == 'jpg' OR $fileType == 'JPG' OR $fileType == 'jpeg' OR $fileType == 'JPEG' OR $fileType == 'pdf' OR $fileType == 'PDF'){
				if(move_uploaded_file($fileNameTmp, "../poConfirm/".$fileName.'.'.$fileType)){
					return $fileName.'.'.$fileType;
					//echo $fileName.'.'.$fileType.'hey';
				}else{
					echo "<script language='javascript'>";
					echo "alert('เกิดข้อผิดพลาดในการอัพโหลดไฟล์ โปรดลองใหม่อีกครั้ง')";
					echo  "</script>";
					return "";
				}
			}else{
				echo "<script language='javascript'>";
				echo "alert('โปรดอัพโหลดเฉพาะรูปภาพนามสกุล .jpg หรือ .pdf เท่านั้น')";
				echo  "</script>";
			}
		}

		public function deletePOConfirmPic($orderid){
			$sql = "SELECT CONFIRM_PATH FROM prdorder WHERE ORDERID = '$orderid'";
			$result = mysql_query($sql);
			while($fetcharr=mysql_fetch_array($result)){
            	$CONFIRM_PATH = $fetcharr['CONFIRM_PATH'];

            	$pathToDeleteFile = '../poConfirm/'.$CONFIRM_PATH;
            	unlink("$pathToDeleteFile");
            }

			$sql2 = "UPDATE prdorder SET CONFIRM_PATH = null WHERE ORDERID = '$orderid'";
			$result2 = mysql_query($sql2);
		}
	}
?>