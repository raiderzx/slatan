<?php
	class prdorderClass{
		public $ORDERID;
		public $D_ORDER;
		public $CONFIRM_PATH;
		public $DR_PROVIDER;
		public $NAME;
		public $USER_COMPANY;
		public $USER_ADDRESS;
		public $USER_TEL;
		public $USER_EMAIL;
		public $CONFIRM_STATUS;
		public $STATUS;
		
		//query all prdorder //for admin
		public function queryAllPrdorder(){
			$sql = "SELECT a.`ORDERID`, a.`DR_PROVIDER`, b.`USER_COMPANY`, a.`D_ORDER`, a.`COMMENTORDER`, a.`CONFIRM_STATUS`, a.`STATUS` FROM `prdorder` a LEFT JOIN user b ON a.`DR_PROVIDER` = b.`USER_ID`";
			$result = mysql_query($sql);
			$numRows = mysql_num_rows($result);

			if($numRows == 0){
				echo "<h4>ยังไม่มีรายการสั่งซื้อสินค้า</h4>";
			}else{
				echo "<table id='example1' class='table table-bordered table-striped table-hover'>";
					echo "<thead>";
			            echo "<tr>";
			              	echo "<th>ID</th>";
			              	echo "<th>User</th>";
			              	echo "<th>Date Order</th>";
							echo "<th>หมายเหตุ</th>";
			              	echo "<th>การรับรอง</th>";
			              	echo "<th>STATUS</th>";
			              	echo "<th>จัดการ</th>";
			            echo "</tr>";
			        echo "</thead>";

				while($rows = mysql_fetch_array($result)){
					$ORDERID = $rows["ORDERID"];
					$DR_PROVIDER = $rows["DR_PROVIDER"];
					$USER_COMPANY = $rows["USER_COMPANY"];
					$D_ORDER = $rows["D_ORDER"];
					$COMMENTORDER = $rows["COMMENTORDER"];
					$CONFIRM_STATUS = $rows["CONFIRM_STATUS"];
					$STATUS = $rows["STATUS"];

					echo "<tr>";
		              	echo "<td>$ORDERID</td>";
		              	echo "<td>$USER_COMPANY</td>";
		              	echo "<td>$D_ORDER</td>";
						echo "<td>$COMMENTORDER</td>";

		              	if($CONFIRM_STATUS == 1){
							echo "<td class='success'>";
							echo "<center>";
							echo "OrderComplete";
							echo "</center>";
							echo "</td>";
		              	}else{
							echo "<td class='danger'>";
							echo "<center>";
							echo "PendingOrder";
							echo "</center>";
							echo "</td>";
		              	}

		              	if($STATUS == 1){
							echo "<td class='success'>";
							echo "<center>";
							echo "Active";
							echo "</center>";
							echo "</td>";
		              	}else{
							echo "<td class='danger'>";
							echo "<center>";
							echo "Cancel";
							echo "</center>";
							echo "</td>";
		              	}

		              	echo "<td><a class='btn btn-default' href='prdorderDt.php?ORDERID=$ORDERID'>ดูข้อมูล</a></td>";
		            echo "</tr>";
				}
				echo "</table>";
			}
		}
		
		//query all prdorder //for user
		public function queryPrdorderByUser($userid){
			$sql = "SELECT * FROM prdorder WHERE DR_PROVIDER = '$userid' ORDER BY ORDERID DESC";
			$result = mysql_query($sql);
			$numRows = mysql_num_rows($result);

			if($numRows == 0){
				echo "<h4>ยังไม่มีรายการสั่งซื้อสินค้า</h4>";
			}else{
				echo "<table id='example1' class='table table-bordered table-striped table-hover'>";
					echo "<thead>";
			            echo "<tr>";
			              	echo "<th>ID</th>";
			              	//echo "<th>User</th>";
			              	echo "<th>Date Order</th>";
			              	echo "<th>หมายเหตุ</th>";
			              	echo "<th>การรับรอง</th>";
			              	echo "<th>สถานะ</th>";
			              	echo "<th>ดูข้อมูล</th>";
			            echo "</tr>";
			        echo "</thead>";

				while($rows = mysql_fetch_array($result)){
					$ORDERID = $rows["ORDERID"];
					//$DR_PROVIDER = $rows["DR_PROVIDER"];
					$D_ORDER = $rows["D_ORDER"];
					$COMMENTORDER = $rows["COMMENTORDER"];
					$CONFIRM_STATUS = $rows["CONFIRM_STATUS"];
					$STATUS = $rows["STATUS"];

					echo "<tr>";
		              	echo "<td>$ORDERID</td>";
		              	//echo "<td>$DR_PROVIDER</td>";
		              	echo "<td>$D_ORDER</td>";
		              	echo "<td>$COMMENTORDER</td>";

		              	if($CONFIRM_STATUS == 1){
							echo "<td class='success'>";
							echo "<center>";
							echo "OrderComplete";
							echo "</center>";
							echo "</td>";
		              	}else{
							echo "<td class='danger'>";
							echo "<center>";
							echo "PendingOrder";
							echo "</center>";
							echo "</td>";
		              	}
						
		              	if($STATUS == 1){
							echo "<td class='success'>";
							echo "<center>";
							echo "Active";
							echo "</center>";
							echo "</td>";
		              	}else{
							echo "<td class='danger'>";
							echo "<center>";
							echo "Cancel";
							echo "</center>";
							echo "</td>";
		              	}

		              	echo "<td><a class='btn btn-default' href='prdorderHistory.php?ORDERID=$ORDERID'>ดูข้อมูล</a></td>";
		            echo "</tr>";
				}
				echo "</table>";
			}
		}
		
		public function showDateOrderAndDateDeliver($orderid, $dateOrder, $dateDeliver){
			echo "<table class='table table-responsive'>";
				echo "<tr>";
					echo "<td><b>ORDERID</b></td>";
					echo "<td>$orderid</td>";
				echo "</tr>";
				echo "<tr>";
					echo "<td><b>วันสั่งสินค้า</b></td>";
					echo "<td>$dateOrder</td>";
				echo "</tr>";
				/*
				echo "<tr>";
					echo "<td><b>วันส่งสินค้าเวชภัณฑ์</b></td>";
					echo "<td>$dateDeliver</td>";
				echo "</tr>";
				*/
			echo "</table>";
		}

		//ดึงรายละเอียด prdorder เป็นรายตัว //for user
		public function queryPrdorderInfo($ORDERID, $USER_ID){
			$sql = "SELECT * FROM prdorder WHERE ORDERID = '$ORDERID' AND DR_PROVIDER = '$USER_ID' AND `STATUS` = '1'";
			$result = mysql_query($sql);
			$numRows = mysql_num_rows($result);
			
			if($numRows == 0){
				$this->ID = "ไม่พบรหัสดังกล่าว";
				$this->ORDERID = "ไม่พบรหัสการสั่งสินค้าดังกล่าว";
				$this->CONFIRM_PATH = "";
			}else{
				while($rows = mysql_fetch_array($result)){
					$ORDERID = $rows["ORDERID"];
					$DR_PROVIDER = $rows["DR_PROVIDER"];
					$CONFIRM_PATH = $rows["CONFIRM_PATH"];
					$D_ORDER = $rows["D_ORDER"];
					$STATUS = $rows["STATUS"];
				}
				$this->ORDERID = $ORDERID;
				$this->D_ORDER = $D_ORDER;
				$this->CONFIRM_PATH = $CONFIRM_PATH;
				$this->STATUS = $STATUS;
			}
		}
		
		//ดึงรายละเอียด prdorderdt //for user
		public function queryPrdorderDt($ORDERID, $USER_ID){
			$sql = "SELECT a.ORDERID, a.DR_PROVIDER, b.ID, b.PRDCODE, b.QTY, c.PRDCODE, c.CODE, c.PRDNAME, c.PRDCAT, c.PRICE, c.UNIT, d.CATNAME FROM prdorder a LEFT JOIN prdorderdt b ON a.ORDERID = b.ORDERID LEFT JOIN masproduct c ON b.PRDCODE = c.PRDCODE LEFT JOIN masprdcats d ON c.PRDCAT = d.PRDCAT WHERE a.ORDERID = '$ORDERID' AND a.DR_PROVIDER = '$USER_ID'";
			$result = mysql_query($sql);
			$numRows = mysql_num_rows($result);

			if($numRows == 0){
				echo "<h3>ไม่พบข้อมูลผลิตภัณฑ์ที่ท่านสั่งซื้อ</h3>";
			}else{
				echo "<table class='table table-striped'>";
	                echo "<thead>";
	                  	echo "<tr>";
	                  		echo "<th><center>Product Code</center></th>";
		                    echo "<th><center>Product</center></th>";
		                    echo "<th><center>Qty</center></th>";
		                    echo "<th><center>Unit</center></th>";
		                    echo "<th><center>Price</center></th>";
		                    echo "<th><center>Subtotal</center></th>";
	                  	echo "</tr>";
	                echo "</thead>";
	                echo "<tbody>";

	                $TOTAL = 0;
                	while($rows = mysql_fetch_array($result)){
                		$QTY = $rows["QTY"];
						$PRDNAME = $rows["PRDNAME"];
						$CODE = $rows["CODE"];
						$UNIT = $rows["UNIT"];
						$PRICE = $rows["PRICE"];

						echo "<tr>";
						echo "<td>$CODE</td>";
	                    echo "<td>$PRDNAME</td>";
	                    echo "<td style='text-align:right;'>$QTY</td>";
	                    echo "<td>$UNIT</td>";
	                    echo "<td style='text-align:right;'>".number_format($PRICE, 2)."</td>";
	                    $subtotal = $QTY * $PRICE;
	                    echo "<td style='text-align:right;'>".number_format($subtotal, 2)."</td>";
                  		echo "</tr>";
                  		$TOTAL = $TOTAL + $subtotal;
					}

	                echo "</tbody>";
					/*
	                echo "<tfoot>";
	                	echo "<tr>";
	                		echo "<th colspan='5' style='text-align:right;'>Total</th>";
	                		echo "<th style='text-align:right;'>".number_format($TOTAL, 2)."</th>";
	                	echo "</tr>";
	                echo "</tfoot>";
					*/
              	echo "</table>";
			}
		}

		public function queryPrdorderDtSumPrice($ORDERID, $USER_ID){
			$sql = "SELECT a.ORDERID, a.DR_PROVIDER, b.ID, b.PRDCODE, b.QTY, c.PRDCODE, c.CODE, c.PRDNAME, c.PRDCAT, c.PRICE, c.UNIT, d.CATNAME FROM prdorder a LEFT JOIN prdorderdt b ON a.ORDERID = b.ORDERID LEFT JOIN masproduct c ON b.PRDCODE = c.PRDCODE LEFT JOIN masprdcats d ON c.PRDCAT = d.PRDCAT WHERE a.ORDERID = '$ORDERID' AND a.DR_PROVIDER = '$USER_ID'";
			$result = mysql_query($sql);

			$TOTAL = 0;
			while($rows = mysql_fetch_array($result)){
        		$QTY = $rows["QTY"];
				$PRDNAME = $rows["PRDNAME"];
				$CODE = $rows["CODE"];
				$UNIT = $rows["UNIT"];
				$PRICE = $rows["PRICE"];

                $subtotal = $QTY * $PRICE;
          		$TOTAL = $TOTAL + $subtotal;
			}
			//$VAT = $TOTAL * 0.07;
			$subTotal = $TOTAL / 1.07;
			$vatTotal = $TOTAL - $subTotal;
			
			//echo "<p class='lead'>กำหนดชำระ 2/22/2014</p>";
  			echo "<div class='table-responsive'>";
                echo "<table class='table'>";
                  	echo "<tr>";
	                    echo "<th style='width:35%''>Subtotal:</th>";
	                    echo "<td>".'฿ '.number_format($subTotal, 2)."</td>";
                  	echo "</tr>";
                  	echo "<tr>";
	                    echo "<th>Tax (7.0%)</th>";
	                    echo "<td>".'฿ '.number_format($vatTotal, 2)."</td>";
                  	echo "</tr>";
                  	echo "<tr>";
	                    echo "<th>Total:</th>";
	                    echo "<td>".'฿ '.number_format($TOTAL, 2)."</td>";
                  	echo "</tr>";
					echo "<tr>";
	                    echo "<th>จำนวนเงินตัวอักษร:</th>";
	                    echo "<td>".$this->numToThai($TOTAL)."</td>";
                  	echo "</tr>";
                echo "</table>";
  			echo "</div>";
		}
		
		public function numToThai($number){
			$t1 = array("ศูนย์", "หนึ่ง", "สอง", "สาม", "สี่", "ห้า", "หก", "เจ็ด", "แปด", "เก้า");
			$t2 = array("เอ็ด", "ยี่", "สิบ", "ร้อย", "พัน", "หมื่น", "แสน", "ล้าน");
			$zerobahtshow = 0; // ในกรณีที่มีแต่จำนวนสตางค์ เช่น 0.25 หรือ .75 จะให้แสดงคำว่า ศูนย์บาท หรือไม่ 0 = ไม่แสดง, 1 = แสดง
			(string) $number;
			$number = explode(".", $number);
			if(!empty($number[1])){
				if(strlen($number[1]) == 1){
				$number[1] .= "0";
				}elseif(strlen($number[1]) > 2){
					if($number[1]{2} < 5){
						$number[1] = substr($number[1], 0, 2);
					}else{
						$number[1] = $number[1]{0}.($number[1]{1}+1);
					}
				}
			}
			for($i=0; $i<count($number); $i++){
				$countnum[$i] = strlen($number[$i]);
				if($countnum[$i] <= 7){
					$var[$i][] = $number[$i];
				}else{
					$loopround = ceil($countnum[$i]/6);
					for($j=1; $j<=$loopround; $j++){
						if($j == 1){
							$slen = 0;
							$elen = $countnum[$i]-(($loopround-1)*6);
						}else{
							$slen = $countnum[$i]-((($loopround+1)-$j)*6);
							$elen = 6;
						}
						$var[$i][] = substr($number[$i], $slen, $elen);
					}
				}
				$nstring[$i] = "";
				for($k=0; $k<count($var[$i]); $k++){
					if($k > 0) $nstring[$i] .= $t2[7];
					$val = $var[$i][$k];
					$tnstring = "";
					$countval = strlen($val);
					for($l=7; $l>=2; $l--){
						if($countval >= $l){
							$v = substr($val, -$l, 1);
								if($v > 0){
									if($l == 2 && $v == 1){
										$tnstring .= $t2[($l)];
									}elseif($l == 2 && $v == 2){
										$tnstring .= $t2[1].$t2[($l)];
									}else{
										$tnstring .= $t1[$v].$t2[($l)];
									}
								}
						}
					}
					if($countval >= 1){
						$v = substr($val, -1, 1);
						if($v > 0){
							if($v == 1 && $countval > 1 && substr($val, -2, 1) > 0){
								$tnstring .= $t2[0];
							}else{
								$tnstring .= $t1[$v];
							}
						}
					}
				$nstring[$i] .= $tnstring;
				}
			}
			$rstring = "";
			if(!empty($nstring[0]) || $zerobahtshow == 1 || empty($nstring[1])){
				if($nstring[0] == "") $nstring[0] = $t1[0];
				$rstring .= $nstring[0]."บาท";
			}
			if(count($number) == 1 || empty($nstring[1])){
				$rstring .= "ถ้วน";
			}else{
				$rstring .= $nstring[1]."สตางค์";
			}
			return $rstring;
		}

		public function queryPrdorderInfoByAdmin($ORDERID){
			$sql = "SELECT a.`DR_PROVIDER`, CONCAT_WS(' ', b.USER_NAME, b.USER_LNAME) as 'NAME', b.`USER_COMPANY`, b.`USER_ADDRESS`, b.`USER_TEL`, b.`USER_EMAIL`, a.`D_ORDER`, a.`CONFIRM_PATH`, a.`CONFIRM_STATUS` FROM prdorder a left JOIN user b ON a.DR_PROVIDER = b.USER_ID WHERE a.`ORDERID` = '$ORDERID'";
			$result = mysql_query($sql);
			$numRows = mysql_num_rows($result);

			if($numRows == 0){
				echo "ไม่พบข้อมูลรายการสั่งซื้อนี้";
			}else{
				while($rows = mysql_fetch_array($result)){
                	$DR_PROVIDER = $rows["DR_PROVIDER"];
                	$NAME = $rows["NAME"];
                	$USER_COMPANY = $rows["USER_COMPANY"];
                	$USER_ADDRESS = $rows["USER_ADDRESS"];
                	$USER_TEL = $rows["USER_TEL"];
                	$USER_EMAIL = $rows["USER_EMAIL"];
                	$D_ORDER = $rows["D_ORDER"];
                	$CONFIRM_PATH = $rows["CONFIRM_PATH"];
                	$CONFIRM_STATUS = $rows["CONFIRM_STATUS"];
                }
				$this->ORDERID = $ORDERID;
                $this->DR_PROVIDER = $DR_PROVIDER;
				$this->NAME = $NAME;
				$this->USER_COMPANY = $USER_COMPANY;
				$this->USER_ADDRESS = $USER_ADDRESS;
				$this->USER_TEL = $USER_TEL;
				$this->USER_EMAIL = $USER_EMAIL;
				$this->D_ORDER = $D_ORDER;
				$this->CONFIRM_PATH = $CONFIRM_PATH;
				$this->CONFIRM_STATUS = $CONFIRM_STATUS;
			}
		}

		public function updatePOConfirmPic($CONFIRMPIC_PATH, $ORDERID){
			$sql = "UPDATE prdorder SET CONFIRM_PATH = '$CONFIRMPIC_PATH' WHERE ORDERID = '$ORDERID'";
			$result = mysql_query($sql);
			return true;
		}

		public function changeConfirmStat($ORDERID, $confirmStat){
			$sql = "UPDATE prdorder SET CONFIRM_STATUS = '$confirmStat' WHERE ORDERID = '$ORDERID'";
			$result = mysql_query($sql);
			return true;
		}

		public function cancelOrder($ORDERID){
			$sql = "UPDATE prdorder SET STATUS = '0' WHERE ORDERID = '$ORDERID'";
			$result = mysql_query($sql);
			return true;
		}
	}
?>