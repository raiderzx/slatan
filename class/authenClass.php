<?php
	Class authenClass{
		public $NAME;
		public $LNAME;
		public $USER_COMPANY;
		public $ADDRESS;
		public $TEL;
		public $EMAIL;
		
		public function userLogin($USER_ACCOUNT, $USER_PASSWORD){
			$sql = "SELECT * FROM user WHERE `USER_ACCOUNT` = '$USER_ACCOUNT' AND `USER_PASSWORD` = '$USER_PASSWORD';";
			$result=mysql_query($sql);
			$numRow = mysql_num_rows($result);
			
			if($numRow == 0){
				echo "<script language='javascript'>";
				echo "alert('ท่านกรอก username หรือ password ไม่ถูกต้อง โปรดกรุณาระบุใหม่อีกครั้ง')";
				echo  "</script>";
				echo "<script language=\"javascript\">window.location='".basename($_SERVER['PHP_SELF'])."'</script>";
			}else{
				while($fetcharr=mysql_fetch_array($result)){
					$USER_ID = $fetcharr['USER_ID'];
					$USER_ACCOUNT = $fetcharr['USER_ACCOUNT'];
					$USER_PASSWORD = $fetcharr['USER_PASSWORD'];
					$USER_NAME = $fetcharr['USER_NAME'].' '.$fetcharr['USER_LNAME'];
					$STATUS = $fetcharr['STATUS'];
				}
				if($STATUS == 1){
					$_SESSION['USER_ID'] = $USER_ID;
					$_SESSION['USER_ACCOUNT'] = $USER_ACCOUNT;
					$_SESSION['USER_NAME'] = $USER_NAME;
					$_SESSION['STATUS'] = $STATUS;
					echo "<script language=\"javascript\">window.location='".basename("prdorderHistoryList.php")."'</script>";
				}else{
					echo "<script language='javascript'>";
					echo "alert('บัญชีใช้งานของท่านถูกระงับการใช้งาน โปรดติดต่อผู้ดูแลระบบ')";
					echo  "</script>";
					echo "<script language=\"javascript\">window.location='".basename($_SERVER['PHP_SELF'])."'</script>";
				}
				
			}
		}
		
		public function manufacLogin($USER_ACCOUNT, $USER_PASSWORD){
			$sql = "SELECT * FROM user_manufacturer WHERE `USERFAC_ACCOUNT` = '$USER_ACCOUNT' AND `USERFAC_PASSWORD` = '$USER_PASSWORD';";
			$result = mysql_query($sql);
			$numRow = mysql_num_rows($result);
			
			if($numRow == 0){
				echo "<script language='javascript'>";
				echo "alert('ท่านกรอก username หรือ password ไม่ถูกต้อง โปรดกรุณาระบุใหม่อีกครั้ง')";
				echo  "</script>";
				echo "<script language=\"javascript\">window.location='".basename($_SERVER['PHP_SELF'])."'</script>";
			}else{
				while($fetcharr=mysql_fetch_array($result)){
					$USERFAC_ID = $fetcharr['USERFAC_ID'];
					$USERFAC_ACCOUNT = $fetcharr['USERFAC_ACCOUNT'];
					$USERFAC_PASSWORD = $fetcharr['USERFAC_PASSWORD'];
					$USERFAC_NAME = $fetcharr['USERFAC_NAME'].' '.$fetcharr['USERFAC_LNAME'];
					$STATUS = $fetcharr['STATUS'];
				}
				if($STATUS == 1){
					$_SESSION['USERFAC_ID'] = $USERFAC_ID;
					$_SESSION['USERFAC_ACCOUNT'] = $USERFAC_ACCOUNT;
					$_SESSION['USERFAC_NAME'] = $USERFAC_NAME;
					$_SESSION['STATUS'] = $STATUS;
					echo "<script language=\"javascript\">window.location='".basename("manufacPriceList.php")."'</script>";
				}else{
					echo "<script language='javascript'>";
					echo "alert('บัญชีใช้งานของท่านถูกระงับการใช้งาน โปรดติดต่อผู้ดูแลระบบ')";
					echo  "</script>";
					echo "<script language=\"javascript\">window.location='".basename($_SERVER['PHP_SELF'])."'</script>";
				}
				
			}
		}

		public function adminLogin($USER_ACCOUNT, $USER_PASSWORD){
			$sql = "SELECT * FROM admin WHERE `USER_ACCOUNT` = '$USER_ACCOUNT' AND `USER_PASSWORD` = '$USER_PASSWORD';";
			$result=mysql_query($sql);
			$numRow = mysql_num_rows($result);
			
			if($numRow == 0){
				echo "<script language='javascript'>";
				echo "alert('ท่านกรอก username หรือ password ไม่ถูกต้อง โปรดกรุณาระบุใหม่อีกครั้ง')";
				echo  "</script>";
				echo "<script language=\"javascript\">window.location='".basename($_SERVER['PHP_SELF'])."'</script>";
			}else{
				while($fetcharr=mysql_fetch_array($result)){
					$ADMIN_USER_ID = $fetcharr['USER_ID'];
					$ADMIN_USER_ACCOUNT = $fetcharr['USER_ACCOUNT'];
					$ADMIN_USER_PASSWORD = $fetcharr['USER_PASSWORD'];
					$ADMIN_USER_NAME = $fetcharr['USER_NAME'].' '.$fetcharr['USER_LNAME'];
					$ADMIN_STATUS = $fetcharr['STATUS'];
				}
				if($ADMIN_STATUS == 1){
					$_SESSION['ADMIN_USER_ID'] = $ADMIN_USER_ID;
					$_SESSION['ADMIN_USER_ACCOUNT'] = $ADMIN_USER_ACCOUNT;
					$_SESSION['ADMIN_USER_NAME'] = $ADMIN_USER_NAME;
					$_SESSION['ADMIN_STATUS'] = $ADMIN_STATUS;
					echo "<script language=\"javascript\">window.location='".basename("dashboard.php")."'</script>";
				}else{
					echo "<script language='javascript'>";
					echo "alert('บัญชีใช้งานของท่านถูกระงับการใช้งาน โปรดติดต่อผู้ดูแลระบบ')";
					echo  "</script>";
					echo "<script language=\"javascript\">window.location='".basename($_SERVER['PHP_SELF'])."'</script>";
				}
				
			}
		}
		
		public function getUserInfo($USER_ID){
			$sql = "SELECT USER_NAME, USER_LNAME, USER_COMPANY, USER_ADDRESS, USER_TEL, USER_EMAIL FROM user WHERE USER_ID = '$USER_ID'";
			$result = mysql_query($sql);
			$numRow = mysql_num_rows($result);
			
			if($numRow == 0){
				echo "<script language='javascript'>";
				echo "alert('ไม่พบรายละเอียดข้อมูลของผู้ใช้รายนี้')";
				echo  "</script>";
				echo "<script language=\"javascript\">window.location='".basename($_SERVER['PHP_SELF'])."'</script>";
			}else{
				while($fetcharr=mysql_fetch_array($result)){
					$USER_NAME = $fetcharr['USER_NAME'];
					$USER_LNAME = $fetcharr['USER_LNAME'];
					$USER_COMPANY = $fetcharr['USER_COMPANY'];
					$USER_ADDRESS = $fetcharr['USER_ADDRESS'];
					$USER_TEL = $fetcharr['USER_TEL'];
					$USER_EMAIL = $fetcharr['USER_EMAIL'];
				}
				
				$this->NAME = $USER_NAME;
				$this->LNAME = $USER_LNAME;
				$this->USER_COMPANY = $USER_COMPANY;
				$this->ADDRESS = $USER_ADDRESS;
				$this->TEL = $USER_TEL;
				$this->EMAIL = $USER_EMAIL;
			}
		}
	}
?>