<!DOCTYPE html>
<html>
	<?php
		session_start();
		include "include/cdb.php";
		include "../class/prdorderClass.php";
		include "../class/authenClass.php";
		if($_SESSION['USER_ID'] != "" and $_SESSION['USER_NAME'] != "" and $_SESSION['STATUS'] == '1'){
	?>
	<head>
		<meta charset="UTF-8">
		<title>Order Print</title>
		<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
		<!-- Bootstrap 3.3.2 -->
		<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<!-- Font Awesome Icons -->
		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
		<!-- Ionicons -->
		<link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />
		<!-- Theme style -->
		<link href="dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body onload="window.print();">
		<div class="wrapper">
			<?php
				$objQueryPrdorderInfo = new prdorderClass;
				$objQueryPrdorderInfo->queryPrdorderInfo($_GET['ORDERID'], $_SESSION['USER_ID']);
				
				$objGetUserinfo = new authenClass;
				$objGetUserinfo->getUserInfo($_SESSION['USER_ID']);
			?>
			<!-- Main content -->
			<section class="invoice">
			<!-- title row -->
				<div class="row">
					<div class="col-xs-12">
						<h2 class="page-header">
							<i class="fa fa-globe"></i> บริษัทสลาตันร่วมทุนจำกัด Slatan Co., Ltd.
							<!--<small class="pull-right">Date: 2/10/2014</small>-->
						</h2>
					</div><!-- /.col -->
				</div>
				<!-- info row -->
				<div class="row invoice-info">
					<div class="col-sm-4 invoice-col">
						ชื่อ-ที่อยู่ผู้สั่งสินค้า
						<address>
							<strong><?php echo $objGetUserinfo->NAME." ".$objGetUserinfo->LNAME;?></strong><br>
									<?php echo $objGetUserinfo->ADDRESS."<br>".$objGetUserinfo->TEL." ".$objGetUserinfo->EMAIL;?>
						</address>
					</div><!-- /.col -->
					<!--
					<div class="col-sm-4 invoice-col">
						To
						<address>
						  <strong>John Doe</strong><br>
						  795 Folsom Ave, Suite 600<br>
						  San Francisco, CA 94107<br>
						  Phone: (555) 539-1037<br/>
						  Email: john.doe@example.com
						</address>
					</div>--><!-- /.col -->
					<div class="col-sm-4 invoice-col">
						<b>Invoice # ...?</b><br/>
				        <b>Order ID:</b> <?php echo $objQueryPrdorderInfo->ORDERID; ?><br/>
						<b>Order Date:</b> <?php echo $objQueryPrdorderInfo->D_ORDER; ?>
					</div><!-- /.col -->
				</div><!-- /.row -->

				<!-- Table row -->
				<div class="row">
					<div class="col-xs-12 table-responsive">
						<?php
							$objQueryPrdorderdt = new prdorderClass;
							$objQueryPrdorderdt->queryPrdorderDt($_GET['ORDERID'], $_SESSION['USER_ID']);
						?>
					</div><!-- /.col -->
				</div><!-- /.row -->

				<div class="row">
					<!-- accepted payments column -->
					<div class="col-xs-6">
						<!--
						<p class="lead">Payment Methods:</p>
						<img src="dist/img/credit/visa.png" alt="Visa"/>
						<img src="dist/img/credit/mastercard.png" alt="Mastercard"/>
						<img src="dist/img/credit/american-express.png" alt="American Express"/>
						<img src="dist/img/credit/paypal2.png" alt="Paypal"/>
						<p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
						  Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, weebly ning heekya handango imeem plugg dopplr jibjab, movity jajah plickers sifteo edmodo ifttt zimbra.
						</p>
						-->
					</div><!-- /.col -->
					<div class="col-xs-6">
						<?php
	            			$objQueryPrdorderdt->queryPrdorderDtSumPrice($_GET['ORDERID'], $_SESSION['USER_ID']);
	            		?>
					</div><!-- /.col -->
				</div><!-- /.row -->
			</section><!-- /.content -->
		</div><!-- ./wrapper -->
		<!-- AdminLTE App -->
		<script src="dist/js/app.min.js" type="text/javascript"></script>
	</body>
	<?php
		}else{
			echo "<script language='javascript'>";
            echo "alert('โปรดกรุณาเข้าสู่ระบบก่อนการใช้งาน')";
            echo  "</script>";
            echo "<script language=\"javascript\">window.location='".basename("index.php")."'</script>";
		}
	?>
</html>