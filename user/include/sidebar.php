<?php
	//<!-- =============================================== -->

	//<!-- Left side column. contains the sidebar -->
	echo "<aside class='main-sidebar'>";
	//<!-- sidebar: style can be found in sidebar.less -->
		echo "<section class='sidebar'>";
			//<!-- Sidebar user panel -->
			echo "<div class='user-panel'>";
				echo "<div class='pull-left info'>";
					echo "<p>".$_SESSION['USER_NAME']."</p>";

					echo "<a href='#'><i class='fa fa-circle text-success'></i> Online</a>";
				echo "</div>";
			echo "</div>";
			//<!-- search form -->
			//<!-- /.search form -->
			//<!-- sidebar menu: : style can be found in sidebar.less -->
			echo "<ul class='sidebar-menu'>";
				echo "<li class='header'>MAIN NAVIGATION</li>";
				//echo "<li><a href='dashboard.php'><i class='fa fa-dashboard'></i> Dashboard</a></li>";
				echo "<li><a href='prdOrder.php'><i class='fa fa-cart-plus'></i> สั่งซื้อสินค้า</a></li>";
				echo "<li><a href='prdorderHistoryList.php'><i class='fa fa-history'></i> ประวัติการสั่งซื้อ</a></li>";
				/*
				echo "<li class='treeview'>";
					echo "<a href='#'>";
						echo "<i class='fa fa-dashboard'></i> <span>สมัครงาน</span> <i class='fa fa-angle-left pull-right'></i>";
					echo "</a>";
					echo "<ul class='treeview-menu'>";
						echo "<li><a href='jobListAdd.php'><i class='fa fa-circle-o'></i> จัดการข้อมูลงาน</a></li>";
						echo "<li><a href='applicantList.php'><i class='fa fa-circle-o'></i> ดูผู้สมัคร</a></li>";
					echo "</ul>";
				echo "</li>";
				*/
			echo "</ul>";
		echo "</section>";
		//<!-- /.sidebar -->
	echo "</aside>";
?>