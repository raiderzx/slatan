<!DOCTYPE html>
<html>
	<?php
		session_start();
		include "include/cdb.php";
		include "../class/productClass.php";
		if($_SESSION['USER_ID'] != "" and $_SESSION['USER_NAME'] != "" and $_SESSION['STATUS'] == '1'){
	?>
	<head>
		<meta charset="UTF-8">
		<title>Slatan's Product Order</title>
		<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
		<!-- Bootstrap 3.3.2 -->
		<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<!-- Font Awesome Icons -->
		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
		<!-- Ionicons -->
		<link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />
		<!-- DATA TABLES -->
		<link href="plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
		<!-- Theme style -->
		<link href="dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
		<!-- AdminLTE Skins. Choose a skin from the css/skins 
			 folder instead of downloading all of them to reduce the load. -->
		<link href="dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="skin-blue">
		<!-- Site wrapper -->
		<div class="wrapper">
			<?php
				include "include/topbar.php";
				include "include/sidebar.php";

				$objShowProduct = new productClass;

				//ใช้ฟังก์ชั่นนี้ได้ เพราะมันดึงข้อมูลมาเหมือนกัน
				$objShowProduct->selectToEdit($_GET['PRDCODE']);
			?>

			<!-- =============================================== -->

			<!-- Right side column. Contains the navbar and content of the page -->
			<div class="content-wrapper">
				<!-- Content Header (Page header) -->
				<section class="content-header">
					<h1>
						รายละเอียดสินค้า
						<small>ข้อมูลต่างๆของสินค้า</small>
					</h1>
					<ol class="breadcrumb">
						<li><a href="dashboard.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
						<li class='active'><a><i class="fa fa-cube"></i> ข้อมูลสินค้า</a></li>
					</ol>
				</section>

				<!-- Main content -->
				<section class="content">
					<div class='row'>
						<div class='col-md-12'>
							<div class="box">
							  	<div class="box-header with-border">
							    	<h3 class="box-title">รายละเอียด</h3>
							  	</div><!-- /.box-header -->
							  	<div class="box-body">
							    	<table class='table table-bordered'>
							    		<?php
							    			if($objShowProduct->PRDIMG_PATH != ""){
							    				echo "<tr>";
													echo "<td>รูป</td>";
													echo "<td><img src=\"prdPic/$objShowProduct->PRDIMG_PATH\" class='img-thumbnail' /></td>";
												echo "</tr>";
							    			}
							    		?>
										<tr>
											<td>ชื่อสินค้า</td>
											<td><?php echo $objShowProduct->PRDNAME; ?></td>
										</tr>
										<tr>
											<td>Code สินค้า</td>
											<td><?php echo $objShowProduct->CODE; ?></td>
										</tr>
										<tr>
											<td>รายละเอียดสินค้า</td>
											<td><?php echo $objShowProduct->PRDDESC; ?></td>
										</tr>
										<tr>
											<td>หมวดสินค้า</td>
											<td><?php echo $objShowProduct->PRDCAT; ?></td>
										</tr>
										<tr>
											<td>หน่วย</td>
											<td><?php echo $objShowProduct->UNIT; ?></td>
										</tr>
										<tr>
											<td>ราคา</td>
											<td><?php echo $objShowProduct->PRICE; ?></td>
										</tr>
									</table>
							  	</div><!-- /.box-body -->
							</div><!-- /.box -->
							
							
						</div>
					</div>
				</section><!-- /.content -->
			</div><!-- /.content-wrapper -->

			
		</div><!-- ./wrapper -->

		<!-- jQuery 2.1.3 -->
		<script src="plugins/jQuery/jQuery-2.1.3.min.js"></script>
		<!-- Bootstrap 3.3.2 JS -->
		<script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
		<!-- SlimScroll -->
		<script src="plugins/slimScroll/jquery.slimScroll.min.js" type="text/javascript"></script>
		<!-- FastClick -->
		<script src='plugins/fastclick/fastclick.min.js'></script>
		<!-- AdminLTE App -->
		<script src="dist/js/app.min.js" type="text/javascript"></script>
		<!-- AdminLTE for demo purposes -->
		<script src="dist/js/demo.js" type="text/javascript"></script>
	</body>
	<?php
		}else{
			echo "<script language='javascript'>";
            echo "alert('โปรดกรุณาเข้าสู่ระบบก่อนการใช้งาน')";
            echo  "</script>";
            echo "<script language=\"javascript\">window.location='".basename("index.php")."'</script>";
		}
	?>
</html>