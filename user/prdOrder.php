<!DOCTYPE html>
<html>
	<?php
		session_start();
		include "include/cdb.php";
		include "../class/productClass.php";
		if($_SESSION['USER_ID'] != "" and $_SESSION['USER_NAME'] != "" and $_SESSION['STATUS'] == '1'){
	?>
	<head>
		<meta charset="UTF-8">
		<title>Salatan's Product Order</title>
		<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
		<!-- Bootstrap 3.3.2 -->
		<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<!-- Font Awesome Icons -->
		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
		<!-- Ionicons -->
		<link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />
		<!-- DATA TABLES -->
		<link href="plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
		<!-- Theme style -->
		<link href="dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
		<!-- AdminLTE Skins. Choose a skin from the css/skins 
			 folder instead of downloading all of them to reduce the load. -->
		<link href="dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="skin-blue">
		<!-- Site wrapper -->
		<div class="wrapper">
			<?php
				include "include/topbar.php";
				include "include/sidebar.php";
			?>

			<!-- =============================================== -->

			<!-- Right side column. Contains the navbar and content of the page -->
			<div class="content-wrapper">
				<!-- Content Header (Page header) -->
				<section class="content-header">
					<h1>
						รายการสั่งซื้อ
						<small>ดูข้อมูลรายการสั่งซื้อจาก Partner</small>
					</h1>
					<ol class="breadcrumb">
						<!--<li><a href="dashboard.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>-->
						<li class='active'><a href="prdorder.php"><i class="fa fa-cart-plus"></i> รายการสั่งซื้อ</a></li>
					</ol>
				</section>

				<!-- Main content -->
				<section class="content">
					<div class='row'>
						<div class='col-sm-6'>
							<div class="box">
								<div class="box-header">
									<h3 class="box-title">รายการสินค้า</h3>
								</div><!-- /.box-header -->
								<div class="box-body">
									<?php
										$objQueryProduct = new productClass();
										$objQueryProduct->queryAllProduct();
									?>
								</div><!-- /.box-body -->
							</div><!-- /.box -->
						</div>
						
						<div class='col-sm-6'>
							<div class="box">
								<div class="box-header">
									<h3 class="box-title">รายการสินค้า</h3>
								</div><!-- /.box-header -->
								<div class="box-body">
									<table id='selectedProduct' class='table table-bordered table-striped table-responsive'>
										<thead>
											<tr>
												<th colspan='6'>จำนวนรวม = <span id='totalQuan'></span></th>
												<th colspan='2'>ราคารวม = <span id='totalPrice'></span></th>
											</tr>
											<tr>
												<th>CODE</th>
												<th>หมวดสินค้า</th>
												<th>ชื่อสินค้า</th>
												<th>หน่วย</th>
												<th>ราคา</th>
												<th>จำนวน</th>
												<th>ราคาต่อสินค้า</th>
												<th>นำออก</th>
											</tr>
										</thead>
										<tbody>
										</tbody>
										<tfoot>
											<tr>
												<th>CODE</th>
												<th>หมวดสินค้า</th>
												<th>ชื่อสินค้า</th>
												<th>หน่วย</th>
												<th>ราคา</th>
												<th>จำนวน</th>
												<th>ราคาต่อสินค้า</th>
												<th>นำออก</th>
											</tr>
											<tr>
												<th colspan='6'>จำนวนรวม = <span id='totalQuan'></span></th>
												<th colspan='2'>ราคารวม = <span id='totalPrice'></span></th>
											</tr>
											<tr>
												<td colspan='8'><button class='btn btn-primary btn-block' id='checkOut'><i class='fa fa-cart-plus'></i> Check Out</button></td>
											</tr>
										</tfoot>
									</table>
								</div><!-- /.box-body -->
							</div><!-- /.box -->
						</div>
					</div>
				</section><!-- /.content -->
			</div><!-- /.content-wrapper -->

			
		</div><!-- ./wrapper -->

		<!-- jQuery 2.1.3 -->
		<script src="plugins/jQuery/jQuery-2.1.3.min.js"></script>
		<!-- Bootstrap 3.3.2 JS -->
		<script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
		<!-- SlimScroll -->
		<script src="plugins/slimScroll/jquery.slimScroll.min.js" type="text/javascript"></script>
		<!-- FastClick -->
		<script src='plugins/fastclick/fastclick.min.js'></script>
		<!-- AdminLTE App -->
		<script src="dist/js/app.min.js" type="text/javascript"></script>
		<!-- AdminLTE for demo purposes -->
		<script src="dist/js/demo.js" type="text/javascript"></script>
		
		<script>
			$(document).ready(function(){
				$("body").css("display", "none");
				$("body").fadeIn(300);
				
				//click tr and get data of that row
				$(".prdcode").click(function(){
					var code = $(this).data('code');
					var foundPrdcode = 0;
					
					//check selected table to prevent duplicate product
					$("#selectedProduct tr td:nth-child(1)").each(function () {
						eachPrdcodeInTable = $(this).html();
						if(code === eachPrdcodeInTable){
							foundPrdcode = parseInt(foundPrdcode) + 1;
						}
					});
					
					if(foundPrdcode > 0){
						//if duplicate
						alert("ไม่สามารถเพิ่มได้ เนื่องจากมีสินค้านี้อยู่ในรายการที่ท่านเลือกแล้ว");
					}else{
						//if not duplicate draw data from db
						$.ajax({
							url: "../ajax/queryProductData.php?code="+code,
							dataType: "JSON",
							success: function(json){
								var priceComma = json.PRICE.replace(/\B(?=(\d{3})+(?!\d))/g, ",");

								//append data to selected table
								$("#selectedProduct").find('tbody').append(
									$('<tr id="choosingProduct">').append(
										//$('<td>').append($('<img>').attr('src', 'img.png').text('Image cell'))
										$('<td id="CODE">').append(json.CODE),
										$('<td>').append(json.CATNAME),
										$('<td>').append(json.PRDNAME),
										$('<td>').append(json.UNIT),
										$('<td id="PRICE">').append(priceComma).append($("<input id='priceHidden' type='hidden'>").attr('value', json.PRICE)),
										//$('<td id="PRICE">').append($("<input type='hidden'>").attr('value', json.PRICE)),
										//$('<td id="PRICE">').append(json.PRICE),
										$('<td>').append("<input type='text' id='pieceOfProduct' name='pieceOfProduct' style='width: 45px;'>"),
										$('<td style="text-align: right;">').append("<span id='pricePerProduct'></span>").append($("<input id='pricePerProductHidden' type='hidden'>")),
										$('<td id="unselectedPrd">').append("<button class='btn btn-small btn-danger'><i class='fa fa-close'></i></button>")
									)
								);
							}
						});
					}
					
				});
				
				//when key qty of product
				$(document).on('keyup', '#pieceOfProduct' ,function() {
					/*
				    var $price = $(this).closest("tr")   // Finds the closest row <tr> 
                       .find("#PRICE")     // Gets a descendent with class="nr"
                       .text();         // Retrieves the text within <td>
                    */
    				//alert($price);       // Outputs the answer

    				var $price = $(this).closest("tr").find("#priceHidden").val();
    				var $qty = $(this).closest('#pieceOfProduct').val();
    				var $priceCal = $price * $qty;
    				
    				//$(this).closest("tr").find("#pricePerProduct").text($price * $qty);
    				$(this).closest("tr").find("#pricePerProduct").text($priceCal.toString().replace(/,/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ","));
    				$(this).closest("tr").find("#pricePerProductHidden").val($priceCal);
					
					//sum price
					allPrice = 0;
					/*
					$("#selectedProduct tr td:nth-child(7)").each(function () {
						eachPrice = $(this).text();
						if(eachPrice == ""){
							eachPrice = 0;
						}
						allPrice = parseFloat(allPrice) + parseFloat(eachPrice);
					});
					*/
					$("#selectedProduct tr td:nth-child(7)").each(function () {
						eachPrice = $(this).closest("tr").find("#pricePerProductHidden").val();
						if(eachPrice == ""){
							eachPrice = 0;
						}
						allPrice = parseFloat(allPrice) + parseFloat(eachPrice);
					});
					$("#totalPrice").text(allPrice.toString().replace(/,/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ","));
					$("tfoot > tr > th > span#totalPrice").text(allPrice.toString().replace(/,/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ","));		//footer
					
					//sum product qty
					allPiece = 0;
					$("#selectedProduct tr td:nth-child(6)").each(function () {
						eachPiece = $(this).closest('td').find($("input[name=pieceOfProduct]")).val();
						if(eachPiece == ""){
							eachPiece = 0;
						}
						allPiece = parseInt(allPiece) + parseInt(eachPiece);
					});
					$("#totalQuan").text(allPiece.toString().replace(/,/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ","));
					$("tfoot > tr > th > span#totalQuan").text(allPiece.toString().replace(/,/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ","));		//footer
				});
				
				
				//when click remove product button
				$(document).on('click', '#unselectedPrd' ,function() {
    				$(this).closest("tr").remove();
					
					var $price = $(this).closest("tr")   // Finds the closest row <tr> 
                       .find("#PRICE")     // Gets a descendent with class="nr"
                       .text();         // Retrieves the text within <td>
    				//alert($price);       // Outputs the answer
    				var $qty = $(this).closest('#pieceOfProduct').val()
    				//alert($price * $qty);
    				$(this).closest("tr").find("#pricePerProduct").text($price * $qty);
					
					//sum price
					allPrice = 0;
					$("#selectedProduct tr td:nth-child(7)").each(function () {
						eachPrice = $(this).text();
						if(eachPrice == ""){
							eachPrice = 0;
						}
						allPrice = parseFloat(allPrice) + parseFloat(eachPrice);
					});
					$("#totalPrice").text(allPrice);
					$("tfoot > tr > th > span#totalPrice").text(allPrice);		//footer
					
					//sum product qty
					allPiece = 0;
					$("#selectedProduct tr td:nth-child(6)").each(function () {
						eachPiece = $(this).closest('td').find($("input[name=pieceOfProduct]")).val();
						if(eachPiece == ""){
							eachPiece = 0;
						}
						allPiece = parseInt(allPiece) + parseInt(eachPiece);
					});
					$("#totalQuan").text(allPiece);
					$("tfoot > tr > th > span#totalQuan").text(allPiece);		//footer
				});
				
				//check out
				$(document).on('click', '#checkOut' ,function() {
					//confirm box

					if (confirm('ท่านต้องการส่งคำสั่งซื้อใช่หรือไม่?')) {
				        var prdCodeArray = [];
						var prdQty = [];
						
						//get product's code value
	    				$("#selectedProduct tr td:nth-child(1)").each(function () {
							CODE = $(this).text();
							if(CODE != " Check Out"){
								//alert(CODE);
								prdCodeArray.push(CODE);
								//alert(prdCodeArray);
							}
						});
						
						//get QTY
						$("#selectedProduct tr td:nth-child(6)").each(function () {
							qty = $(this).closest('td').find($("input[name=pieceOfProduct]")).val();
							if(qty == ""){
								qty = 0;
							}
							//alert(eachPiece);
							prdQty.push(qty);
							//alert(prdQty);
						});
						
						//send to php
						var prdCodeJSON = JSON.stringify(prdCodeArray);
						var prdQtyJSON = JSON.stringify(prdQty);
						//alert(prdCodeJSON);
						//alert(prdQtyJSON);
						$.ajax({
							type: 'POST',
							url: '../ajax/insertPrdOrder.php',
							data: {'prdorder': prdCodeJSON, 'quantity': prdQtyJSON},
							success: function(json) {
								if(confirm(json.COMPLETE)){
									$("#selectedProduct > tbody").empty();
									$("#totalPrice").empty();
									$("tfoot > tr > th > span#totalPrice").empty();		//footer
									$("#totalQuan").empty();
									$("tfoot > tr > th > span#totalQuan").empty();		//footer
									
									window.open("prdorderdtPrint.php?ORDERID="+json.URL, "_blank");
									//window.location = "prdorderdtPrint.php?ORDERID="+json.URL;
								}
								/*
								alert(msg);
								if(msg == "ท่านได้ทำการสั่งซื้อสินค้าสำเร็จแล้ว สามารถตรวจสอบรายการสั่งซื้อย้อนหลังได้ที่เมนู > ประวัติการสั่งซื้อ"){
									$("#selectedProduct > tbody").empty();
									$("#totalPrice").empty();
									$("tfoot > tr > th > span#totalPrice").empty();		//footer
									$("#totalQuan").empty();
									$("tfoot > tr > th > span#totalQuan").empty();		//footer
								}
								*/
							}
						});
				    }

					
				});
			});
		</script>
		
		<script src="plugins/ckeditor/ckeditor.js"></script>
        <script src="plugins/ckeditor/adapters/jquery.js"></script>
		<script>
            $( document ).ready( function() {
                $( 'textarea#job_detail' ).ckeditor();
            } );

            CKEDITOR.config.toolbar = [
                { name: 'document', groups: [ 'mode', 'document', 'doctools' ], items: [ 'Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates' ] },
                { name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
                { name: 'editing', groups: [ 'find', 'selection', 'spellchecker' ], items: [ 'Find', 'Replace', '-', 'SelectAll', '-', 'Scayt' ] },
                { name: 'forms', items: [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },
                '/',
                { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] },
                { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language' ] },
                { name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },
                { name: 'insert', items: [ 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe' ] },
                //{ name: 'insert', items: [ 'Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe' ] },
                '/',
                { name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
                { name: 'colors', items: [ 'TextColor', 'BGColor' ] },
                { name: 'tools', items: [ 'Maximize', 'ShowBlocks' ] },
                { name: 'others', items: [ '-' ] },
                //{ name: 'about', items: [ 'About' ] }
            ];
        </script>
		
		
		<!-- DATA TABES SCRIPT -->
		<script src="plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
		<script src="plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
		<!-- page script -->
		<script type="text/javascript">
		  	$(function () {
				$("#example1").dataTable();
				//$("#selectedProduct").dataTable();
				$('#example2').dataTable({
					"bPaginate": true,
					"bLengthChange": false,
					"bFilter": false,
					"bSort": true,
					"bInfo": true,
					"bAutoWidth": false
				});
		  	});
		</script>
	</body>
	<?php
		}else{
			echo "<script language='javascript'>";
            echo "alert('โปรดกรุณาเข้าสู่ระบบก่อนการใช้งาน')";
            echo  "</script>";
            echo "<script language=\"javascript\">window.location='".basename("index.php")."'</script>";
		}
	?>
</html>