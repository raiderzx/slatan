<!DOCTYPE html>
<html>
	<?php
		session_start();
		include "include/cdb.php";
		include "../class/productClass.php";
		include "../class/picManageClass.php";
		if($_SESSION['ADMIN_USER_ID'] != "" and $_SESSION['ADMIN_USER_NAME'] != "" and $_SESSION['ADMIN_STATUS'] == '1'){
	?>
	<head>
		<meta charset="UTF-8">
		<title>Slatan's Product Order</title>
		<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
		<!-- Bootstrap 3.3.2 -->
		<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<!-- Font Awesome Icons -->
		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
		<!-- Ionicons -->
		<link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />
		<!-- Theme style -->
		<link href="dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
		<!-- AdminLTE Skins. Choose a skin from the css/skins 
			 folder instead of downloading all of them to reduce the load. -->
		<link href="dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="skin-blue">
		<!-- Site wrapper -->
		<div class="wrapper">
			<?php
				include "include/topbar.php";
				include "include/sidebar.php";

				//$refreshPageURL = "prdManageEdit.php?PRDCODE=".$_GET['PRDCODE'];
			?>

			<!-- =============================================== -->

			<!-- Right side column. Contains the navbar and content of the page -->
			<div class="content-wrapper">
				<!-- Content Header (Page header) -->
				<section class="content-header">
					<h1>
						เพิ่มข้อมูลสินค้า
						<small>เพิ่มข้อมูลสินค้าที่ให้ partner สั่ง</small>
					</h1>
					<ol class="breadcrumb">
						<li><a href="dashboard.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
						<li class='active'><a href="prdManage.php"><i class="fa fa-cubes"></i> เพิ่มรายการสินค้า</a></li>
					</ol>
				</section>

				<!-- Main content -->
				<section class="content">
					<div class='row'>

						<div class='col-md-12'>
							<div class="box">
				                <div class="box-header">
				                  	<h3 class="box-title">เพิ่ม</h3>
				                </div><!-- /.box-header -->
				                <form role="form" enctype="multipart/form-data" action='' method='post'>
				                <div class="box-body table-responsive no-padding">
					                <div class="box-body">
					                    <div class="form-group">
					                      	<label for="code">CODE สินค้า</label>
					                      	<input type="text" class="form-control" id="code" placeholder="รหัสสินค้า" name='CODE'>
					                    </div>
					                    <div class="form-group">
					                      	<label for="prdname">ชื่อสินค้า</label>
					                      	<input type="text" class="form-control" id="prdname" placeholder="ชื่อสินค้า" name='PRDNAME'>
					                    </div>
					                    <div class="form-group">
					                      	<label for="prddescription">รายละเอียดสินค้า</label>
					                      	<textarea class="form-control" id="prddescription" placeholder="รายละเอียดสินค้า" name='PRDDESC'></textarea>
					                    </div>
					                    <!--prdcat-->
					                    <div class="form-group">
					                      	<label for="prdcat">หมวดสินค้า</label>
					                      	<select class="form-control" id="prdcat" name='PRDCAT'>
					                      	<?php
					                      		$objQueryPrdcat = new productClass;
					                      		$objQueryPrdcat->prdcatCmbbox();
					                      	?>
					                      	</select>
					                    </div>
					                    <div class="form-group">
					                      	<label for="unit">หน่วยสินค้า</label>
					                      	<input type="text" class="form-control" id="unit" placeholder="หน่วยสินค้า" name='UNIT'>
					                    </div>
					                    <div class="form-group">
					                      	<label for="prdpic">รูปภาพสินค้า</label>
					                      	<input type='file' id='prdpic' name='PRDIMG_PATH'>
					                      	<input type='hidden' value='noPic' name='picStatus'>
					                    </div>
					                    <div class="form-group">
					                      	<label for="price">ราคาสินค้า</label>
					                      	<input type="text" class="form-control" id="price" placeholder="ราคาสินค้า" name='PRICE'>
					                    </div>
					                    <div class="form-group">
					                      	<label for="last_cost">ทุน</label>
					                      	<input type="text" class="form-control" id="last_cost" placeholder="ทุนสินค้า" name='LAST_COST'>
					                    </div>
					                    <div class="form-group">
					                      	<label for="avg_cost">ทุนเฉลี่ย</label>
					                      	<input type="text" class="form-control" id="avg_cost" placeholder="ทุนเฉลี่ยสินค้า" name='AVG_COST'>
					                    </div>
					                    <div class="checkbox">
					                      	<label>
					                      		<input type='checkbox' name='STATUS' value='status' checked> สถานะการเปิดใช้งาน
					                      	</label>
					                    </div>
					                </div><!-- /.box-body -->
					                <div class="box-footer">
					                    <button type="submit" class="btn btn-primary" name='addPrdData'>เพิ่มข้อมูล</button>
					                </div>
				                </div><!-- /.box-body -->
				                <?php
				                	if(isset($_POST['addPrdData'])){
				                		$objAddPrd = new productClass();

				                		if(isset($_POST['STATUS'])){
				                			$status = 1;
				                		}else{
				                			$status = 0;
				                		}

				                		$justInsertPRDCODE = $objAddPrd->addPrdinfo($_POST['CODE'], $_POST['PRDNAME'], $_POST['PRDDESC'], $_POST['PRDCAT'], $_POST['UNIT'], $_POST['PRICE'], $_POST['LAST_COST'], $_POST['AVG_COST'], $status);

				                		if(!empty($_FILES['PRDIMG_PATH']['name'])){
				                			$objUploadPicGetPath = new picManageClass;
				                			$filePath = $objUploadPicGetPath->uploadPrdPic($_FILES['PRDIMG_PATH']['tmp_name'], $_FILES['PRDIMG_PATH']['name'], $justInsertPRDCODE);

				                			$objInsertPic = new productClass();
				                			$addPicStatus = $objInsertPic->addPicToDB($justInsertPRDCODE, $filePath);
				                		}

				                		if(isset($justInsertPRDCODE) OR ($addPicStatus == true)){
				                			echo '<script type="text/javascript">alert("ท่านได้เพิ่มสินค้าเข้าสู่ระบบเรียบร้อยแล้ว");</script>';
				                			echo "<script language=\"javascript\">window.location='".basename("prdManage.php")."'</script>";
				                		}else{
				                			echo '<script type="text/javascript">alert("ไม่สามารถเพิ่มข้อมูลสินค้าได้ เนื่องจาก CODE สินค้านี้มีอยู่ในระบบแล้ว");</script>';
				                			echo "<script language=\"javascript\">window.location='".basename("prdAdd.php")."'</script>";
				                		}
				                	}
				                ?>
				                </form>
				            </div><!-- /.box -->
						</div>

					</div>
				</section><!-- /.content -->
			</div><!-- /.content-wrapper -->

			
		</div><!-- ./wrapper -->

		<!-- jQuery 2.1.3 -->
		<script src="plugins/jQuery/jQuery-2.1.3.min.js"></script>
		<!-- Bootstrap 3.3.2 JS -->
		<script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
		<!-- SlimScroll -->
		<script src="plugins/slimScroll/jquery.slimScroll.min.js" type="text/javascript"></script>
		<!-- FastClick -->
		<script src='plugins/fastclick/fastclick.min.js'></script>
		<!-- AdminLTE App -->
		<script src="dist/js/app.min.js" type="text/javascript"></script>
		<!-- AdminLTE for demo purposes -->
		<script src="dist/js/demo.js" type="text/javascript"></script>
		<!-- page script -->

		<script type="text/javascript">
		  	$(function () {
				$("#example1").dataTable();
				//$("#selectedProduct").dataTable();
				$('#example2').dataTable({
					"bPaginate": true,
					"bLengthChange": false,
					"bFilter": false,
					"bSort": true,
					"bInfo": true,
					"bAutoWidth": false
				});
		  	});
		</script>
		
		<script src="plugins/ckeditor/ckeditor.js"></script>
        <script src="plugins/ckeditor/adapters/jquery.js"></script>
		<script>
			$("#prddescription").ckeditor();

            CKEDITOR.config.toolbar = [
				{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Strike'] }
                //{ name: 'document', groups: [ 'mode', 'document', 'doctools' ], items: [ 'Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates' ] },
                //{ name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
                //{ name: 'editing', groups: [ 'find', 'selection', 'spellchecker' ], items: [ 'Find', 'Replace', '-', 'SelectAll', '-', 'Scayt' ] },
                //{ name: 'forms', items: [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },
                //'/',
                //{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] },
                //{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', //'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language' ] },
                //{ name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },
                //{ name: 'insert', items: [ 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe' ] },
                //{ name: 'insert', items: [ 'Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe' ] },
                //'/',
                //{ name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
                //{ name: 'colors', items: [ 'TextColor', 'BGColor' ] },
                //{ name: 'tools', items: [ 'Maximize', 'ShowBlocks' ] },
                //{ name: 'others', items: [ '-' ] },
                //{ name: 'about', items: [ 'About' ] }
            ];
        </script>
	</body>
	<?php
		}else{
			echo "<script language='javascript'>";
            echo "alert('โปรดกรุณาเข้าสู่ระบบก่อนการใช้งาน')";
            echo  "</script>";
            echo "<script language=\"javascript\">window.location='".basename("index.php")."'</script>";
		}
	?>
</html>