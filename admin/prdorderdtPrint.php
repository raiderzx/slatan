<!DOCTYPE html>
<html>
    <?php
        session_start();
        include "include/cdb.php";
        include "../class/prdorderClass.php";
        include "../class/authenClass.php";
        include "../class/dateClass.php";
        if($_SESSION['ADMIN_USER_ID'] != "" and $_SESSION['ADMIN_USER_NAME'] != "" and $_SESSION['ADMIN_STATUS'] == '1'){
    ?>
    <head>
        <meta charset="UTF-8">
        <title>Order Print</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- Bootstrap 3.3.2 -->
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- Font Awesome Icons -->
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />

        <style>
            .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
                padding: 2px;
                line-height: 1.42857143;
                vertical-align: top;
                border-top: 1px solid #ddd;
            }
        </style>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
        <body onload="window.print();">
            <div class="wrapper">
            <?php
				$objQueryPrdorderInfo = new prdorderClass;
				$objQueryPrdorderInfo->queryPrdorderInfoByAdmin($_GET['ORDERID']);
				/*
                $objQueryPrdorderInfo = new prdorderClass;
                $objQueryPrdorderInfo->queryPrdorderInfo($_GET['ORDERID'], $_SESSION['USER_ID']);
				*/
                
				/*
                $objGetUserinfo = new authenClass;
                $objGetUserinfo->getUserInfo($_SESSION['USER_ID']);
				*/
            ?>
                <!-- Main content -->
                <section class="invoice">
                    <!-- title row -->
                    <div class="row">
                        <div class='col-xs-12 page-header'>
                            <div class='row'>
                                <div class="col-xs-7">
                                    <h2 style='font-size: 90%; margin-top: 0px;'>
                                        <i class="fa fa-globe"></i> บริษัท สลาตัน เมดิคอลเซ็นเตอร์ จำกัด<br>
                                        
                                        <!--<small>247 - 279, Siriros Rd., Satang, Muang-Yala, Yala 95000</small><br>-->
                                        <small>247 - 249 ถนนสิโรรส ตำบลสะเตง อำเภอเมืองยะลา จังหวัดยะลา 95000</small>
                                        
                                        <!--<small class="pull-right">Date: 2/10/2014</small>-->
                                    </h2>
                                </div><!-- /.col -->
                                <div class='col-xs-5'>
                                    <small class='pull-right' style='font-size: 63%;'><i class='fa fa-phone'></i> 073-228520 &nbsp;&nbsp;&nbsp;<i class='fa fa-mobile'></i> 082 3919354 <br><i class='fa fa-fax'></i> 073-224422 &nbsp;&nbsp;<i class='fa fa-envelope'></i> salatanmedic@gmail.com</small>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class='col-sm-12'>
                            <center><h2 style='margin-top: 0px;'>Quotation</h2></center>
                        </div>
                    </div>
                    <!-- info row -->
                    <div class="row invoice-info">
                        <div class="col-sm-4 invoice-col" style='font-size: 8pt;'>
							ชื่อ-ที่อยู่ผู้สั่งสินค้า
                            <address>
                                <strong><?php echo $objQueryPrdorderInfo->USER_COMPANY; ?></strong><br>
                                    <?php echo $objQueryPrdorderInfo->USER_ADDRESS."<br>".$objQueryPrdorderInfo->USER_TEL." ".$objQueryPrdorderInfo->USER_EMAIL;?>
                            </address>
                        </div><!-- /.col -->
                        <!--
                        <div class="col-sm-4 invoice-col">
                            To
                            <address>
                                <strong>John Doe</strong><br>
                                795 Folsom Ave, Suite 600<br>
                                San Francisco, CA 94107<br>
                                Phone: (555) 539-1037<br/>
                                Email: john.doe@example.com
                            </address>
                        </div>--><!-- /.col -->
                        <div class="col-xs-offset-2 col-xs-4 invoice-col" style='font-size: 8pt;'>
                            <?php
                                $objConvertDate = new dateManage();
                                $date = $objConvertDate->convertDateBD($objQueryPrdorderInfo->D_ORDER);

                                $deliverDate = date('Y-m-d h:i:s', strtotime($objQueryPrdorderInfo->D_ORDER. ' + 21 days'));
                                $objConvertDeliverDate = new dateManage();


                                $objShowDate = new prdorderClass();
                                $objShowDate->showDateOrderAndDateDeliver($objQueryPrdorderInfo->ORDERID, $date, $objConvertDeliverDate->convertDateBD($deliverDate));
                            ?>
                        </div><!-- /.col -->
                    </div><!-- /.row -->

                    <!-- Table row -->
                    <div class="row">
                        <div class="col-xs-12 table-responsive table-bordered" style='font-size: 8pt;'>
                            <?php
								$objQueryPrdorderdt = new prdorderClass;
								$objQueryPrdorderdt->queryPrdorderDt($_GET['ORDERID'], $objQueryPrdorderInfo->DR_PROVIDER);
                            ?>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                    <br>
                    <div class="row">
                        <!-- accepted payments column -->
                        <div class="col-xs-6" style='font-size: 7pt;'>
                            
                            <p class="lead" style='font-size: 8pt;'>We are pleased to offer the undermentioned goods on the terms and conditions described as follows:</p>
                            <!--
                            <img src="dist/img/credit/visa.png" alt="Visa"/>
                            <img src="dist/img/credit/mastercard.png" alt="Mastercard"/>
                            <img src="dist/img/credit/american-express.png" alt="American Express"/>
                            <img src="dist/img/credit/paypal2.png" alt="Paypal"/>
                            -->
                            <p class="text-muted well well-sm no-shadow">
								1.กำหนดการส่งสินค้า<br>
								- เวชภัณฑ์: ภายใน 3 สัปดาห์หลังจากทำการสั่งสินค้า<br>
								- อุปกรณ์การแพทย์: แล้วแต่โรงงาน<br>
								2.หมายเหตุ: ราคานี้รวมภาษีมูลค่าเพิ่ม 7% เรียบร้อยแล้ว<br>
								3.การรับประกัน<br>
								- เวชภํณฑ์: ไม่มี<br>
								- อุปกรณ์การแพทย์: 2 ปี นับจากวันที่เริ่มใช้งาน<br>
								4.กำหนดการชำระเงิน: เงินค่าทำสัญญา 30%<br>
								30% ชำระในวันติดตั้งเครื่อง และส่วนที่เหลือ 40% ชำระภายใน 30 วัน หลังจากที่ติดตั้งเครื่องพร้อมใช้งานเสร็จเรียบร้อยแล้ว
                            </p>
                            
                        </div><!-- /.col -->
                        <div class="col-xs-6" style='font-size: 8pt;'>
                            <?php
                                $objQueryPrdorderdt->queryPrdorderDtSumPrice($_GET['ORDERID'], $objQueryPrdorderInfo->DR_PROVIDER);
                            ?>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                    <br>
                    <div class='row'>
                        <div class='col-xs-6' style='font-size: 8pt;'>
                            <center>
                                ผู้สั่งซื้อ______________________<br><br>
                                (____________________________)
                            </center>
                        </div>
                        <div class='col-xs-6' style='font-size: 8pt;'>
                            <center>
                                ผู้เสนอราคา____________________<br><br>
                                ( นางสาวหฤทัย พากย์ฉันทวัจน์)<br>
                                (Salatan Medical Center)
                            </center>
                        </div>
                    </div>
                </section><!-- /.content -->
            </div><!-- ./wrapper -->
        <!-- AdminLTE App -->
        <script src="dist/js/app.min.js" type="text/javascript"></script>
    </body>
  <?php
    }else{
        echo "<script language='javascript'>";
            echo "alert('โปรดกรุณาเข้าสู่ระบบก่อนการใช้งาน')";
            echo  "</script>";
            echo "<script language=\"javascript\">window.location='".basename("index.php")."'</script>";
        }
  ?>
</html>