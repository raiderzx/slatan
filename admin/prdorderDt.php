<!DOCTYPE html>
<html>
	<?php
		session_start();
		include "include/cdb.php";
		include "../class/prdorderClass.php";
		include "../class/authenClass.php";
		include "../class/picManageClass.php";
		include "../class/dateClass.php";
		if($_SESSION['ADMIN_USER_ID'] != "" and $_SESSION['ADMIN_USER_NAME'] != "" and $_SESSION['ADMIN_STATUS'] == '1'){
	?>
	<head>
		<meta charset="UTF-8">
		<title>Slatan's Dashboard</title>
		<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
		<!-- Bootstrap 3.3.2 -->
		<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<!-- Font Awesome Icons -->
		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
		<!-- Ionicons -->
		<link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />
		<!-- Theme style -->
		<link href="dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
		<!-- AdminLTE Skins. Choose a skin from the css/skins 
			 folder instead of downloading all of them to reduce the load. -->
		<link href="dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="skin-blue">
		<!-- Site wrapper -->
		<div class="wrapper">
			<?php
				include "include/topbar.php";
				include "include/sidebar.php";

				$objQueryPrdorderInfo = new prdorderClass;
				$objQueryPrdorderInfo->queryPrdorderInfoByAdmin($_GET['ORDERID']);
				
				//$objGetUserinfo = new authenClass;
				//$objGetUserinfo->getUserInfo($_SESSION['USER_ID']);
			?>

			<!-- =============================================== -->

			<div class="content-wrapper">
		        <!-- Content Header (Page header) -->
		        <section class="content-header">
		          	<h1>
						ใบเสนอราคา
			            <small></small>
		          	</h1>
		          	<ol class="breadcrumb">
			            <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
			            <li class="active">ประวัติการสั่งซื้อ</li>
		          	</ol>
		        </section>
				
				<!--
		        <div class="pad margin no-print">
		          	<div class="callout callout-info" style="margin-bottom: 0!important;">												
		            <h4><i class="fa fa-info"></i> Note:</h4>
		            	This page has been enhanced for printing. Click the print button at the bottom of the invoice to test.
		          	</div>
		        </div>
				-->

		        <!-- Main content -->
		        <section class="invoice">
		          	<!-- title row -->
		          	<div class="row">
		            	<div class="col-xs-12">
		              		<h2 class="page-header">
		                		<i class="fa fa-globe"></i> บริษัทสลาตันร่วมทุนจำกัด Slatan Co., Ltd.
		              		</h2>
		            	</div><!-- /.col -->
		          	</div>
		          	<!-- info row -->
		          	<div class="row invoice-info">
		            	<div class="col-sm-8 invoice-col">
			              	ชื่อ-ที่อยู่ผู้สั่งสินค้า
			              	<address>
				                <strong><?php echo $objQueryPrdorderInfo->USER_COMPANY; ?></strong><br>
				                <?php echo $objQueryPrdorderInfo->USER_ADDRESS."<br>".$objQueryPrdorderInfo->USER_TEL." ".$objQueryPrdorderInfo->USER_EMAIL;?>
			              	</address>
		            	</div><!-- /.col -->
						<!--
			            <div class="col-sm-4 invoice-col">
			              	To
			              	<address>
				                <strong>John Doe</strong><br>
				                795 Folsom Ave, Suite 600<br>
				                San Francisco, CA 94107<br>
				                Phone: (555) 539-1037<br/>
				                Email: john.doe@example.com
			              	</address>
		            	</div>
						-->
						<!-- /.col -->
		            	<div class="col-sm-4 invoice-col">
				            <?php
					        $objConvertDate = new dateManage();
							$date = $objConvertDate->convertDateBD($objQueryPrdorderInfo->D_ORDER);

							$deliverDate = date('Y-m-d h:i:s', strtotime($objQueryPrdorderInfo->D_ORDER. ' + 21 days'));
							$objConvertDeliverDate = new dateManage();


					        $objShowDate = new prdorderClass();
					        $objShowDate->showDateOrderAndDateDeliver($objQueryPrdorderInfo->ORDERID, $date, $objConvertDeliverDate->convertDateBD($deliverDate));
				        ?>
		            	</div><!-- /.col -->
		          	</div><!-- /.row -->

		          	<!-- Table row -->
		          	<div class="row">
		            	<div class="col-xs-12 table-responsive">
			              	<?php
			              		$objQueryPrdorderdt = new prdorderClass;
			              		$objQueryPrdorderdt->queryPrdorderDt($_GET['ORDERID'], $objQueryPrdorderInfo->DR_PROVIDER);
			              	?>
		            	</div><!-- /.col -->
		          	</div><!-- /.row -->

		          	<div class="row">
		            	<!-- accepted payments column -->
		            	<div class="col-xs-6">
							<!--
			              	<p class="lead">Payment Methods:</p>
			              	<img src="dist/img/credit/visa.png" alt="Visa"/>
			              	<img src="dist/img/credit/mastercard.png" alt="Mastercard"/>
			              	<img src="dist/img/credit/american-express.png" alt="American Express"/>
			              	<img src="dist/img/credit/paypal2.png" alt="Paypal"/>
			              	<p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
			                	Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, weebly ning heekya handango imeem plugg dopplr jibjab, movity jajah plickers sifteo edmodo ifttt zimbra.
			              	</p>
							-->
		            	</div><!-- /.col -->
		            	<div class="col-xs-6">
		              		<?php
		            			$objQueryPrdorderdt->queryPrdorderDtSumPrice($_GET['ORDERID'], $objQueryPrdorderInfo->DR_PROVIDER);
		            		?>
		            	</div><!-- /.col -->
		          	</div><!-- /.row -->

		          	<!-- this row will not appear when printing -->
		          	<div class="row no-print">
			            <div class="col-xs-6">
							<form role="form" action='' method='post'>	
								<a href="<?php echo "prdorderdtPrint.php?ORDERID=".$_GET['ORDERID'];?>" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a>

								<?php
									if($objQueryPrdorderInfo->CONFIRM_STATUS == 1){
										echo "<input type='hidden' name='confirmStat' value='notConfirm'>";
										echo "<button type='submit' class='btn btn-danger' name='confirm'><i class='fa fa-check-square' name='confirm'></i> เลิกรับรองเอกสาร</button>";
									}else{
										echo "<input type='hidden' name='confirmStat' value='Confirm'>";
										echo "<button type='submit' class='btn btn-info' name='confirm'><i class='fa fa-check-square-o'></i> รับรองเอกสาร</button>";
									}
								?>
							</form>
							<?php
								if(isset($_POST['confirm'])){
									if($_POST['confirmStat'] == "Confirm"){
										$confirmStat = 1;
									}else{
										$confirmStat = 0;
									}

									$objUpdatePOConfirmStat = new prdorderClass;
									if($objUpdatePOConfirmStat->changeConfirmStat($_GET['ORDERID'], $confirmStat) == true){
										echo "<script language='javascript'>";
										echo "alert('แก้ไขสถานะการรับรองแล้ว')";
										echo  "</script>";
									}else{
										echo "<script language='javascript'>";
										echo "alert('แก้ไขสถานะการรับรองไม่สำเร็จ โปรดติดต่อผู้ดูแลระบบ')";
										echo  "</script>";
									}

									echo "<script language=\"javascript\">window.location='".basename("prdorderDt.php?ORDERID=".$_GET['ORDERID'])."'</script>";
								}
							?>
			              	<!--<button class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Submit Payment</button>-->
							
			            </div>
			            <div class="col-xs-6">
			            	<?php
			            		if($objQueryPrdorderInfo->CONFIRM_PATH == ""){
			            			echo "<form role='form' enctype='multipart/form-data' action='' method='post'>";	
										echo "<p class='help-block'>ผู้ใช้ยังไม่ได้ส่งเอกสารยืนยัน</p>";
									echo "</form>";
			            		}else{
			            			echo "<form  role='form' enctype='multipart/form-data' action='' method='post'>";
				            			echo "<a href=\"../poConfirm/$objQueryPrdorderInfo->CONFIRM_PATH\" target='_blank'>เอกสารรับรอง</a>";
				            			//echo "&nbsp;";
	                  					//echo "<input type='submit' class='btn btn-danger' value='ลบ' name='delPRDIMG'>";
	                  					//echo "<input type='hidden' value='haveFile' name='fileStatus'>";
	                  				echo "</form>";
	                      			
			            		}
			            	?>
							
			              	<!--<button class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Submit Payment</button>-->
							<?php
								if(isset($_POST['uploadConfirm'])){
									if(!empty($_FILES['confirmPO']['name'])){
										$objUploadConfirmPO = new picManageClass;
										$picPath = $objUploadConfirmPO->uploadPOConfirmPic($_FILES['confirmPO']['tmp_name'], $_FILES['confirmPO']['name'], $_GET['ORDERID']);
										if($picPath != ""){
											$objUpdatePath = new prdorderClass;
											$updateStat = $objUpdatePath->updatePOConfirmPic($picPath, $_GET['ORDERID']);
											if($updateStat == true){
												echo "<script language='javascript'>";
												echo "alert('เพิ่มข้อมูลสำเร็จแล้ว')";
												echo  "</script>";
												echo "<script language=\"javascript\">window.location='".basename("prdorderHistory.php?ORDERID=".$_GET['ORDERID'])."'</script>";
											}else{
												echo "<script language='javascript'>";
												echo "alert('เพิ่มข้อมูลไม่สำเร็จโปรดลองใหม่อีกครั้ง')";
												echo  "</script>";
												echo "<script language=\"javascript\">window.location='".basename("prdorderHistory.php?ORDERID=".$_GET['ORDERID'])."'</script>";
											}
										}
									}
								}

								if(isset($_POST['delPRDIMG'])){
			                		$objDeleteFile = new picManageClass;
			                		$objDeleteFile->deletePOConfirmPic($_GET['ORDERID']);
			                		echo "<script language=\"javascript\">window.location='".basename("prdorderHistory.php?ORDERID=".$_GET['ORDERID'])."'</script>";
			                	}
							?>
			            </div>
		          	</div>
		        </section><!-- /.content -->
	        	<div class="clearfix"></div>
	    	</div><!-- /.content-wrapper -->
		</div><!-- ./wrapper -->

		<!-- jQuery 2.1.3 -->
		<script src="plugins/jQuery/jQuery-2.1.3.min.js"></script>
		<!-- Bootstrap 3.3.2 JS -->
		<script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
		<!-- SlimScroll -->
		<script src="plugins/slimScroll/jquery.slimScroll.min.js" type="text/javascript"></script>
		<!-- FastClick -->
		<script src='plugins/fastclick/fastclick.min.js'></script>
		<!-- AdminLTE App -->
		<script src="dist/js/app.min.js" type="text/javascript"></script>
		<!-- AdminLTE for demo purposes -->
		<script src="dist/js/demo.js" type="text/javascript"></script>
		
	</body>
	<?php
		}else{
			echo "<script language='javascript'>";
            echo "alert('โปรดกรุณาเข้าสู่ระบบก่อนการใช้งาน')";
            echo  "</script>";
            echo "<script language=\"javascript\">window.location='".basename("index.php")."'</script>";
		}
	?>
</html>