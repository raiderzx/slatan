<!DOCTYPE html>
<html>
	<?php
		session_start();
		include "include/cdb.php";
		include "../class/userClass.php";
		if($_SESSION['ADMIN_USER_ID'] != "" and $_SESSION['ADMIN_USER_NAME'] != "" and $_SESSION['ADMIN_STATUS'] == '1'){
	?>
	<head>
		<meta charset="UTF-8">
		<title>Slatan's User</title>
		<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
		<!-- Bootstrap 3.3.2 -->
		<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<!-- Font Awesome Icons -->
		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
		<!-- Ionicons -->
		<link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />
		<!-- Theme style -->
		<link href="dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
		<!-- AdminLTE Skins. Choose a skin from the css/skins 
			 folder instead of downloading all of them to reduce the load. -->
		<link href="dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="skin-blue">
		<!-- Site wrapper -->
		<div class="wrapper">
			<?php
				include "include/topbar.php";
				include "include/sidebar.php";

				$refreshPageURL = "userEdit.php?USER_ID=".$_GET['USER_ID'];
			?>

			<!-- =============================================== -->

			<!-- Right side column. Contains the navbar and content of the page -->
			<div class="content-wrapper">
				<!-- Content Header (Page header) -->
				<section class="content-header">
					<h1>
						แก้ไขข้อมูลผู้ใช้
						<small>แก้ไขข้อมูล partner</small>
					</h1>
					<ol class="breadcrumb">
						<li><a href="dashboard.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
						<li class='active'><a href="userList.php"><i class="fa fa-users"></i> แก้ไขข้อมูลผู้ใช้</a></li>
					</ol>
				</section>

				<?php
              		$objQueryUser = new userClass();
              		$objQueryUser->selectUserToEdit($_GET['USER_ID']);
              	?>
				<!-- Main content -->
				<section class="content">
					<div class='row'>

						<div class='col-md-12'>
							<div class="box">
				                <div class="box-header">
				                  	<h3 class="box-title">แก้ไข</h3>
				                </div><!-- /.box-header -->
				                <form role="form" enctype="multipart/form-data" action='' method='post'>
				                <div class="box-body table-responsive no-padding">
					                <div class="box-body">
					                	<div class="form-group">
					                      	<label for="USER_ID">ID</label>
					                      	<input type="text" class="form-control" id="USER_ID" placeholder="ID" name='USER_ID' value="<?php echo $objQueryUser->USER_ID;?>" readonly>
					                    </div>
					                    <div class="form-group">
					                      	<label for="USER_ACCOUNT">ชื่อบัญชี</label>
					                      	<input type="text" class="form-control" id="USER_ACCOUNT" placeholder="รหัสสินค้า" name='USER_ACCOUNT' value="<?php echo $objQueryUser->USER_ACCOUNT;?>" readonly>
					                    </div>
										<div class="form-group">
					                      	<label for="USER_PASSWORD">Password</label>
					                      	<input type="password" class="form-control" id="USER_PASSWORD" placeholder="ชื่อสินค้า" name='USER_PASSWORD' value="<?php echo $objQueryUser->USER_PASSWORD;?>">
					                    </div>
										<div class="form-group">
					                      	<label for="CONFIRM_USER_PASSWORD">Confirm Password</label>
					                      	<input type="password" class="form-control" id="CONFIRM_USER_PASSWORD" placeholder="ชื่อสินค้า" name='CONFIRM_USER_PASSWORD' value="<?php echo $objQueryUser->USER_PASSWORD;?>">
					                    </div>
										<div class="form-group">
					                      	<label for="USER_NAME">ชื่อผู้ใช้</label>
					                      	<input type="text" class="form-control" id="code" placeholder="รหัสสินค้า" name='USER_NAME' value="<?php echo $objQueryUser->USER_NAME;?>">
					                    </div>
										<div class="form-group">
					                      	<label for="USER_LNAME">นามสกุลผู้ใช้</label>
					                      	<input type="text" class="form-control" id="code" placeholder="รหัสสินค้า" name='USER_LNAME' value="<?php echo $objQueryUser->USER_LNAME;?>">
					                    </div>
										<div class="form-group">
					                      	<label for="USER_COMPANY">หน่วยงานผู้ใช้</label>
					                      	<input type="text" class="form-control" id="code" placeholder="รหัสสินค้า" name='USER_COMPANY' value="<?php echo $objQueryUser->USER_COMPANY;?>">
					                    </div>
										<div class="form-group">
					                      	<label for="USER_ADDRESS">ที่อยู่</label>
					                      	<input type="text" class="form-control" id="code" placeholder="รหัสสินค้า" name='USER_ADDRESS' value="<?php echo $objQueryUser->USER_ADDRESS;?>">
					                    </div>
										<div class="form-group">
					                      	<label for="USER_TEL">โทรศัพท์</label>
					                      	<input type="text" class="form-control" id="code" placeholder="รหัสสินค้า" name='USER_TEL' value="<?php echo $objQueryUser->USER_TEL;?>">
					                    </div>
										<div class="form-group">
					                      	<label for="USER_EMAIL">อีเมล์</label>
					                      	<input type="text" class="form-control" id="code" placeholder="รหัสสินค้า" name='USER_EMAIL' value="<?php echo $objQueryUser->USER_EMAIL;?>">
					                    </div>
										<div class="form-group">
					                      	<label for="USER_ADD_DATE">วันที่เพิ่มผู้ใช้</label>
					                      	<input type="text" class="form-control" id="code" placeholder="รหัสสินค้า" name='USER_ADD_DATE' value="<?php echo $objQueryUser->USER_ADD_DATE;?>" readonly>
					                    </div>
					                    <div class="checkbox">
					                      	<label>
					                      		<?php
					                      			if($objQueryUser->STATUS == 1){
					                      				echo "<input type='checkbox' name='STATUS' value='status' checked> สถานะการเปิดใช้งาน";
					                      			}else{
					                      				echo "<input type='checkbox' name='STATUS' value='status'> สถานะการเปิดใช้งาน";
					                      			}
					                      		?>
					                        	
					                      	</label>
					                    </div>
					                </div><!-- /.box-body -->
					                <div class="box-footer">
					                    <button type="submit" class="btn btn-primary" name='editUserData'>แก้ไขข้อมูล</button>
					                </div>
				                </div><!-- /.box-body -->
				                <?php
				                	if(isset($_POST['editUserData'])){
										if($_POST['USER_PASSWORD'] == $_POST['CONFIRM_USER_PASSWORD']){
											$objEditUser = new userClass();

											if(isset($_POST['STATUS'])){
												$status = 1;
											}else{
												$status = 0;
											}
											
											/*
											if(!empty($_FILES['PRDIMG_PATH']['name'])){
												$objUploadPicGetPath = new picManageClass;
												$filePath = $objUploadPicGetPath->uploadPrdPic($_FILES['PRDIMG_PATH']['tmp_name'], $_FILES['PRDIMG_PATH']['name'], $_POST['PRDCODE']);
											}
											*/
											
											$objEditUser->editUserInfo($_POST['USER_ID'], $_POST['USER_PASSWORD'], $_POST['USER_NAME'], $_POST['USER_LNAME'], $_POST['USER_COMPANY'], $_POST['USER_ADDRESS'], $_POST['USER_TEL'], $_POST['USER_EMAIL'], $status);
											echo "<script language=\"javascript\">window.location='".basename($refreshPageURL)."'</script>";
										}else{
											echo "<script language='javascript'>";
											echo "alert('ท่านระบุ Password ไม่ตรงกัน, กรุณาระบุ Password ให้ตรงกัน')";
											echo  "</script>";
										}
				                	}
									
									/*
				                	if(isset($_POST['delPRDIMG'])){
				                		$objDeletePic = new picManageClass;
				                		$objDeletePic->deletePrdPic($_POST['PRDCODE']);
				                		echo "<script language=\"javascript\">window.location='".basename($refreshPageURL)."'</script>";
				                	}
									*/
				                ?>
				                </form>
				            </div><!-- /.box -->
						</div>

					</div>
				</section><!-- /.content -->
			</div><!-- /.content-wrapper -->

			
		</div><!-- ./wrapper -->

		<!-- jQuery 2.1.3 -->
		<script src="plugins/jQuery/jQuery-2.1.3.min.js"></script>
		<!-- Bootstrap 3.3.2 JS -->
		<script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
		<!-- SlimScroll -->
		<script src="plugins/slimScroll/jquery.slimScroll.min.js" type="text/javascript"></script>
		<!-- FastClick -->
		<script src='plugins/fastclick/fastclick.min.js'></script>
		<!-- AdminLTE App -->
		<script src="dist/js/app.min.js" type="text/javascript"></script>
		<!-- AdminLTE for demo purposes -->
		<script src="dist/js/demo.js" type="text/javascript"></script>
		<!-- page script -->

		<script type="text/javascript">
		  	$(function () {
				$("#example1").dataTable();
				//$("#selectedProduct").dataTable();
				$('#example2').dataTable({
					"bPaginate": true,
					"bLengthChange": false,
					"bFilter": false,
					"bSort": true,
					"bInfo": true,
					"bAutoWidth": false
				});
		  	});
		</script>
	</body>
	<?php
		}else{
			echo "<script language='javascript'>";
            echo "alert('โปรดกรุณาเข้าสู่ระบบก่อนการใช้งาน')";
            echo  "</script>";
            echo "<script language=\"javascript\">window.location='".basename("index.php")."'</script>";
		}
	?>
</html>