-- phpMyAdmin SQL Dump
-- version 4.3.11.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 08, 2016 at 08:52 AM
-- Server version: 5.5.41-MariaDB
-- PHP Version: 5.5.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `salatanm_admin`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `USER_ID` int(15) NOT NULL COMMENT 'PK',
  `USER_ACCOUNT` varchar(15) DEFAULT NULL COMMENT 'ชื่อ login',
  `USER_PASSWORD` varchar(15) DEFAULT NULL COMMENT 'password',
  `USER_NAME` varchar(50) DEFAULT NULL COMMENT 'Name',
  `USER_LNAME` varchar(50) DEFAULT NULL COMMENT 'Surname',
  `USER_ADD_DATE` datetime DEFAULT NULL COMMENT 'become member date',
  `USER_EDIT_DATE` datetime DEFAULT NULL COMMENT 'edit member date',
  `STATUS` tinyint(1) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`USER_ID`, `USER_ACCOUNT`, `USER_PASSWORD`, `USER_NAME`, `USER_LNAME`, `USER_ADD_DATE`, `USER_EDIT_DATE`, `STATUS`) VALUES
(1, 'admin', '1234', 'Admin', 'Anukul', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `masprdcats`
--

CREATE TABLE IF NOT EXISTS `masprdcats` (
  `PRDCAT` int(11) NOT NULL,
  `CATNAME` varchar(75) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `masprdcats`
--

INSERT INTO `masprdcats` (`PRDCAT`, `CATNAME`) VALUES
(1, 'อุปกรณ์ทางการแพทย์'),
(2, 'เวชภัณฑ์'),
(3, 'เคมีภัณฑ์'),
(4, 'อื่นๆ');

-- --------------------------------------------------------

--
-- Table structure for table `masprdtomanufacturer`
--

CREATE TABLE IF NOT EXISTS `masprdtomanufacturer` (
  `IDFACT` int(11) NOT NULL,
  `IDMASPRODUCT` int(11) DEFAULT NULL,
  `IDMANUFACTURER` int(11) DEFAULT NULL,
  `PRICE` int(11) DEFAULT NULL,
  `LASTCOST` int(11) DEFAULT NULL,
  `AVGCOST` int(11) DEFAULT NULL,
  `STATUS` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `masprdtomanufacturer`
--

INSERT INTO `masprdtomanufacturer` (`IDFACT`, `IDMASPRODUCT`, `IDMANUFACTURER`, `PRICE`, `LASTCOST`, `AVGCOST`, `STATUS`) VALUES
(1, 1, 3, 120, 100, 100, 0),
(2, 2, 3, 100, 10, 10, 0),
(3, 7, 3, 1000, 1000, 1000, 0),
(4, 38, 3, 600000, 600000, 600000, 1),
(5, 16, 3, 500, 500, 500, 1),
(6, 35, 3, 450000, 450000, 450000, 1),
(7, 18, 3, 5000000, 5000000, 5000000, 1),
(8, 7, 4, 1500, 1500, 1500, 1),
(9, 48, 4, 14000000, 14000000, 14000000, 1),
(10, 9, 4, 750, 750, 750, 1),
(11, 3, 4, 1000, 1000, 1000, 1),
(12, 11, 4, 200, 200, 200, 0),
(13, 31, 4, 500, 500, 500, 1),
(14, 8, 4, 500, 500, 500, 1),
(15, 48, 5, 2700, 2700, 2700, 1),
(16, 36, 5, 2500, 2500, 2500, 1),
(17, 21, 5, 10000, 10000, 10000, 1),
(18, 18, 5, 4500000, 4500000, 4500000, 1);

-- --------------------------------------------------------

--
-- Table structure for table `masproduct`
--

CREATE TABLE IF NOT EXISTS `masproduct` (
  `PRDCODE` bigint(20) NOT NULL,
  `CODE` varchar(20) NOT NULL,
  `PRDNAME` varchar(75) DEFAULT NULL,
  `PRDDESC` text,
  `PRDCAT` int(11) NOT NULL,
  `UNIT` varchar(45) DEFAULT NULL,
  `PRICE` decimal(12,2) DEFAULT NULL,
  `LAST_COST` decimal(12,2) DEFAULT NULL,
  `AVG_COST` decimal(12,2) DEFAULT NULL,
  `STATUS` tinyint(4) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=424 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `masproduct`
--

INSERT INTO `masproduct` (`PRDCODE`, `CODE`, `PRDNAME`, `PRDDESC`, `PRDCAT`, `UNIT`, `PRICE`, `LAST_COST`, `AVG_COST`, `STATUS`) VALUES
(1, 'S001', 'Probac 10 Plus', NULL, 2, 'box', '250.50', NULL, NULL, 1),
(2, 'S002', 'Ascorbic acid', NULL, 2, 'box', '108.75', NULL, NULL, 1),
(3, 'T001', 'Myers cocktail', NULL, 2, 'ea', '1200.00', NULL, NULL, 1),
(4, 'T002', 'Procaine HCL 2%', NULL, 2, 'Bottle', '5600.00', NULL, NULL, 1),
(5, 'M001', 'สำลี', '', 1, 'kg.', '550.00', '45.00', '475.00', 1),
(6, 'M002', 'ผ้าพันแผล', NULL, 1, 'Pack', '1200.00', NULL, NULL, 1),
(7, 'M003', 'Mask', NULL, 1, 'Box', '450.00', NULL, NULL, 1),
(8, 'M004', 'ผ้าอ้อม', NULL, 1, 'Pack', '450.00', NULL, NULL, 1),
(9, 'M005', 'Syringe', NULL, 1, 'Pack', '1000.00', NULL, NULL, 1),
(10, 'M006', 'Cotton Budge', NULL, 1, 'Pack', '100.00', NULL, NULL, 1),
(11, 'M007', 'หมวกคลุมผม', NULL, 1, 'Box', '250.00', NULL, NULL, 1),
(12, 'M008', 'กรรไกรตัดไหม', NULL, 1, 'ea', '50.00', NULL, NULL, 1),
(13, 'M009', 'ผ้าก๊อซ(เส้นใยไม่ติดแผล)', NULL, 1, 'Pack', '250.00', NULL, NULL, 1),
(14, 'M010', 'กระจกส่องฟัน', NULL, 1, 'ea', '110.00', NULL, NULL, 1),
(15, 'M011', 'ไฟฉายส่องหู', NULL, 1, 'ea', '560.00', NULL, NULL, 1),
(16, 'S003', 'Ergotamine tartrate', NULL, 2, 'Box', '700.00', NULL, NULL, 1),
(17, 'S004', 'Hard Mask', 'ผ้าปิดปากปิดจมูกแบบแข็ง', 1, 'Piece', '30.00', '11.00', '10.00', 1),
(18, 'ECOV9', 'EcoView9 Plus', '<p><strong>Digital Radiography System with Dual Flat Panel Detectors</strong></p>\r\n\r\n<p>50kW HF Generator(630mA/150kV) with LCD OP Console</p>\r\n\r\n<p>High speed starter(Include in the generator)</p>\r\n\r\n<p>300kHU rotating anode tube(0.6/1.2mm focal spot)</p>\r\n\r\n<p>Ceiling Suspended Tube Stand (Up-Down motorized) with auto-tracking</p>\r\n\r\n<p>Auto Collimator</p>\r\n\r\n<p>6way Elevating table with 1717 fixed FPD(Synchronized to X-ray tube)</p>\r\n\r\n<p>Up-Righr Wall stand with 1717 fixed FPD (Synchronized to X-ray Tube)</p>\r\n\r\n<p>DR Grid 2ea</p>\r\n\r\n<p>EoView DR S/W with workstation</p>\r\n\r\n<p>HV cable 12mm long</p>\r\n\r\n<p>380VAC 50/60Hz Three phase</p>\r\n', 1, 'เครื่อง', '5100000.00', '5000000.00', '5000000.00', 1),
(19, 'PX300HF', 'PX-300HF', '<p>1. 30kW HF Generator</p>\r\n\r\n<p>2. X-ray Tube:0.6/1.2mm focal</p>\r\n\r\n<p>3. Motor driven system: rear wheel Drive</p>\r\n\r\n<p>4. X-Ray tube support with telescopic SID:1500mm Tube Are reach: 1310mm</p>\r\n\r\n<p>5. Manual Collimator</p>\r\n\r\n<p>6. WiFi Wireless FPD</p>\r\n\r\n<p>7. Acquisition S/W with Workstation, Monitor</p>\r\n\r\n<p>8. Input power:100-240VAC, 1KVA</p>\r\n', 1, 'เครื่อง', '3500000.00', '3000000.00', '3000000.00', 1),
(20, 'PVDRP', 'Pink View DR Plus', 'Mammography System Pinkview-DR\r\n1) Generator\r\n - Type : High Frequency Inverter\r\n - Input Power : Single phase 220V AC, 50/60 Hz\r\n - Power Rating\r\n     Large Focus : 20-39kV / 100-720mAs\r\n     Small Focus : 20-39kV / 100-350mAs\r\n - Max . mA Current 100mA@28kV\r\n2) X-Ray Tube\r\n - Focal Spot Size : 0.1mm / 0.3mm\r\n - Target Material : W(Tungsten Totating Anode)\r\n - Anode Heat Storage : 300kHU\r\n - Additional Filtration : 0.05mm Rh\r\n3) Radiographic Stand (C-arm)\r\n - Vertical Movement : 715 mm (605 ~ 1320 mm) Motorized Moving\r\n - Rotating Degrees : R180°/L180° moterized Moving\r\n - SID : 660mm\r\n - Tissue Compression \r\n    Manual(Max. under 300N / IEC60601-2-45\r\n    Motorized by MICOM control (Max. 200N)\r\n - Positioning condition panel\r\n    3.5" Full Color LCD\r\n    Device condition\r\n    Compression Force Level\r\n    Compression Thickness Data\r\n    Degree Position\r\n    AEC Sensor Position\r\n - Main Operation Panel :7" Full Color Touch LCD\r\n - Internal Colimator Plate : Selectable 24 X 30 cm\r\n4) Bucky Device\r\n - Grid : 4:1, 36Line/cm', 1, 'เครื่อง', '4500000.00', '4500000.00', '4500000.00', 1),
(21, 'HF525P', 'HF525Plus', '<p>Radiographic kV range Accuracy: 40-125kV</p>\r\n\r\n<p>Power Output Ratings(@0.1sec): 500mA@80kV</p>\r\n\r\n<p>Tube: Focal 1.0/2.0mm(140KHU)</p>\r\n\r\n<p>Exposure Time Range Accuracy: 0.001-6sec(81steps)</p>\r\n\r\n<p>Frequency: 40kHz mAs Range: 0.1-500mAs High Voltage</p>\r\n\r\n<p>Ripple: &lt;1kV@100kV</p>\r\n\r\n<p>Qutomatic Exposure Control: Optional</p>\r\n\r\n<p><span style="line-height:1.6em">Bucky: Standard Anatomical Programming: 288 APR(Standard) </span></p>\r\n\r\n<p><span style="line-height:1.6em">Line Voltage: 220VAC, 50/60Hz, Single Phase .- Automatic Line Compensation: &plusmn;15%</span></p>\r\n', 1, 'ชุด', '950000.00', '900000.00', '900000.00', 1),
(22, 'ULH60', 'H60', '1. Main unit\r\n2. Convex Probe(C1-4) for Radiology\r\n3. Linear Probe(L5-13)\r\n5. B/W Thermal Printer\r\n6. High Gross Paper\r\n7. UPS 1KVA\r\n8. Ultasound Gel\r\n9. DMR Function\r\n10. Dicom 3.0\r\n11. Cleansing Paper\r\n', 1, 'เครื่อง', '1700000.00', '1200000.00', '1150000.00', 1),
(23, 'ECOV9C', 'EcoView9 Classic', '<p><strong>Digital Radiography System with Flat Panel Detectors </strong></p>\r\n\r\n<p>1. 630mA/150kV X-Ray system</p>\r\n\r\n<p>- 50kW high frequency x-ray generator with high speed rotar</p>\r\n\r\n<p>- Floor to mounted x-ray tube stand</p>\r\n\r\n<p>- X-ray 4 way floating x-ray table</p>\r\n\r\n<p>- DR grid</p>\r\n\r\n<p>2. Universal Detector Stand with Flat panel Detector</p>\r\n\r\n<p>- A-silicon TFT LCD Flat panel Detector(17&quot;x17&quot;)</p>\r\n\r\n<p>3. Acquisition S/W and Workstation</p>\r\n\r\n<p>- 21&quot; LCD monitor and PC 380VAC 50/60Hz three Phase</p>\r\n', 1, 'เครื่อง', '2900000.00', '0.00', '0.00', 1),
(24, 'ECV9S', 'EcoView9 (Smart)', '<p>1. 50kW(630mA/150kV) X-ray system with LCD OP Console</p>\r\n\r\n<p>- High Speed Starter(included in the generator)</p>\r\n\r\n<p>- 300kHU rotating anode tube(0.6/1.2mm focal spots)</p>\r\n\r\n<p>- Floor mounted tube stand (Electromagnetic breaker)</p>\r\n\r\n<p>- manual collimator</p>\r\n\r\n<p>- HV cable (8m long)</p>\r\n\r\n<p>- 4way floating table with rotating bucky device</p>\r\n\r\n<p>- Wall bucky stand</p>\r\n\r\n<p>- DR grid</p>\r\n\r\n<p>2. Portable Detector(1417inch), WiFi type(wireless)</p>\r\n\r\n<p>3. DR S/W with workstation 21&quot;LCD monitor 380VAC 50/60Hz Three phase</p>\r\n', 1, 'เครื่อง', '3100000.00', '0.00', '0.00', 1),
(25, 'ECV9SD', 'EcoView9 (Smart Dual)', '1. 50kW(630mA/150kV) X-ray system with LCD OP Console\r\n.- High Speed Starter(included in the generator)\r\n.- 300kHU rotating anode tube(0.6/1.2mm focal spots)\r\n.- Floor mounted tube stand (Electromagnetic breaker)\r\n.- manual collimator\r\n.- HV cable (8m long)\r\n.- 4 way floating table\r\n.- Wall bucky stand\r\n.- DR grid x 2ea\r\n.- Workstation with 21"LCD monitor, DR S/W\r\n2. A-si Flat Panel Detector x 2ea(Fixed type)\r\n3. DR S/W with workstation 21" LCD monitor\r\n380 VAC 50/60Hz three phase', 1, 'เครื่อง', '3900000.00', '0.00', '0.00', 1),
(26, 'ECV9C', 'EcoView9 (Chest)', '<p>1. 500mA/125kVp X-ray system&nbsp;</p>\r\n\r\n<p>- 40kW high frequency x-rya generator</p>\r\n\r\n<p>- X-ray tube: 1.0/2.0mm focal spot, 140kHU</p>\r\n\r\n<p>2. Synchronized Tube Stand and Detector Stand</p>\r\n\r\n<p>3. A-si Flat Panel Detector (1717), Fixed type</p>\r\n\r\n<p>- Ecoview DR S/W and Workstation 220VAC 50/60Hz single phase</p>\r\n', 1, 'เครื่อง', '2500000.00', '0.00', '0.00', 1),
(27, 'DYDR', 'Dynamic DR', '<p>1. Digital Radiography</p>\r\n\r\n<p>2. Digital Fluoroscopy</p>\r\n\r\n<p>3. Digital Gastrointestinal</p>\r\n\r\n<p>4. Digital Angiography</p>\r\n\r\n<p>- Digital Detector: Blue Plus CCD</p>\r\n\r\n<p>- Effective View Field: 17x17&quot;</p>\r\n\r\n<p>- Static Pixel Matrix: 3072*3072</p>\r\n\r\n<p>- Output Gray: 16 Bit</p>\r\n\r\n<p>- Gemnerator: High Frequency X-ray Machine: 50kW</p>\r\n\r\n<p>- Max output Current: 630mA</p>\r\n\r\n<p>- X-ray Tube: Focus: 0.6mm/1.2mm</p>\r\n\r\n<p>- Max OutPut Voltage: 150kV</p>\r\n', 1, 'เครื่อง', '5500000.00', '0.00', '0.00', 1),
(28, 'PLX7000A', 'PLX-7000A', '1. High Frequency Inverter Power Supply\r\n.- Output Power: 6kW\r\n.- Inverter Frequency: 60kHz\r\n2. Automatic, Manual Continuos Fluoroscopy\r\n.- Tube voltage: 40kV~125kV\r\n.- Tube current: 0.3mA~4mA\r\n3. Automatic, Manual Intensifying Fluoroscopy\r\n.- Tube voltage: 40kV~125kV\r\n.- Tube current: 0.3mA~8mA\r\n4. Automatic, Manual Pulse Fluoroscopy\r\n.- Tube voltage: 40kV~125kV\r\n.- Tube current: 0.3mA~8mA\r\n.- Pulse Frequency: 0.1-10frame/s\r\n5. Radiography Tube Voltage, mA: 40kV~125kV, 120mA\r\n6. X-Ray Tube Special For High Frequency\r\n.- Rotary anode focus: 0.3/0.6\r\n.- Anode thermal capacity: 212kJ\r\n', 1, 'เครื่อง', '1900000.00', '0.00', '0.00', 1),
(29, 'PLX7000B', 'PLX-7000B', '1. High Frequency Inverter Power Supply\r\n.- Output Power: 12kW\r\n.- Inverter Frequency: 60kHz\r\n2. Automatic, Manual Continuos Fluoroscopy\r\n.- Tube voltage: 40kV~125kV\r\n.- Tube current: 0.3mA~4mA\r\n3. Automatic, Manual Intensifying Fluoroscopy\r\n.- Tube voltage: 40kV~125kV\r\n.- Tube current: 0.3mA~8mA\r\n4. Automatic, Manual Pulse Fluoroscopy\r\n.- Tube voltage: 40kV~125kV\r\n.- Tube current: 0.3mA~30mA\r\n.- Pulse Frequency: 0.1-10frame/s\r\n5. DSI Digital Spot File: 0.1-10 frame/s\r\n6. Radiography Tube Voltage, mA: 40kV~125kV, 160mA\r\n7. X-Ray Tube Special For High Frequency\r\n.- Rotary anode focus: 0.3/0.6\r\n.- Anode thermal capacity: 212kJ\r\n', 1, 'เครื่อง', '2400000.00', '0.00', '0.00', 1),
(30, 'PLX7000C', 'PLX-7000C', '1. High Frequency Inverter Power Supply\r\n.- Output Power: 16kW\r\n.- Inverter Frequency: 60kHz\r\n2. Automatic, Manual Continuos Fluoroscopy\r\n.- Tube voltage: 40kV~125kV\r\n.- Tube current: 0.3mA~4mA\r\n3. Automatic, Manual Intensifying Fluoroscopy\r\n.- Tube voltage: 40kV~125kV\r\n.- Tube current: 0.3mA~8mA\r\n4. Automatic, Manual Pulse Fluoroscopy\r\n.- Tube voltage: 40kV~125kV\r\n.- Tube current: 0.3mA~30mA\r\n.- Pulse Frequency: 0.1-10frame/s\r\n5. DSI Digital Spot File: 0.1-10 frame/s\r\n6. Radiography Tube Voltage, mA: 40kV~125kV, 200mA\r\n7. X-Ray Tube Special For High Frequency\r\n.- Rotary anode focus: 0.3/0.6\r\n.- Anode thermal capacity: 212kJ\r\n', 1, 'เครื่อง', '2800000.00', '0.00', '0.00', 1),
(31, 'SPN3G', 'Spinel 3G', '<p>1. H.V Generator High Frequency Inverter type : 40kHz, Max.12KW Radio-mode: kVp output: 40-120kVp mA output: 20-150mA Fluoro-mode: kVp output: 40-120kVp mA output: 0.5-5.0mA/8mA(SNAP SHOT)/20mA(BOOST)</p>\r\n\r\n<p>2. X_Ray tube</p>\r\n\r\n<p>3. Image Intersifier :9&quot; (3 field : 9&quot;-6&quot;-4.5&quot;)</p>\r\n\r\n<p>4.CCD Camera - 1k 5. Digital Image System 6. ABS (Automatic Brightness Control) system</p>\r\n\r\n<p>7. Memory fuction - Last image holder</p>\r\n\r\n<p>8. X-Ray Monitor : B/W TFT 19&quot; medical monitors* 2(1000cd/cm2)</p>\r\n\r\n<p>9.Power requirement : 1&Oslash;,200V , 50/60Hz</p>\r\n\r\n<p>10. Laser Guide</p>\r\n\r\n<p>11. DSA(Digital Substraction Angiography) Option</p>\r\n', 1, 'เครื่อง', '2700000.00', '0.00', '0.00', 1),
(32, 'SPN12HD', 'Spinel 12HD', '1. H.V Generator\r\nHigh Frequency Inverter type : 40kHz, Max.12KW\r\nRadio-mode: kVp output: 40-120kVp\r\n                      mA output: 20-150mA\r\nFluoro-mode: kVp output: 40-120kVp\r\n                     mA output: 0.5-5.0mA/8mA(SNAP SHOT)/20mA(BOOST)\r\n2. X-Ray tube\r\n - Focal spot: 0.3/0.6mm\r\n - Anode HU: 300KHU\r\n3.Fluoroscopy TFT Detector\r\n- Cesium Iodide (Csl)\r\n"- Active Area : 261(H)×287(V)mm \r\n(10.3 x 11.3 inch)"\r\n- Pixel Size :  184 µm, 1420(H)×1560(V) pixels\r\n4. OP Control Panel\r\n5. Digital Imaging System\r\n6. ABS (Automatic Brightness Control) system\r\n7. Laser Guide\r\n8. X-ray Monitor : B/W TFT 19" medical monitors * 2(1000cd/cm2) \r\n9. Power requirement : 1Ø, 220V, 50/60Hz\r\n10. Premium Monitor Cart\r\n', 1, 'เครื่อง', '5800000.00', '0.00', '0.00', 1),
(33, 'SOUL', 'soul', 'C-Arm Assembly\r\n.- Vertical Range: 640mm to 1490mm Motorized\r\n.- SID: 650mm±10mm\r\n.- Rotation Angle: +180 to -180, Motorized, Isocentric\r\n.- Safe Handles: 2ea\r\n.- Vertical Range of Compression Device: 320mm\r\n.- Face Shield: Removable\r\nX-Ray Tube\r\n.- Target Material: Tungsten(W)\r\n.- Filteration: Rhodium/Silver(Rh/Ag)\r\n.- Anode: Rotation\r\n.- Focal Spot Size: Large 0.3mm, Small 0.1mm\r\n.- Target Tube Angle: Large 16, Small 10\r\n.- Anode Heat Capacity: 300,000HU\r\nGenerator\r\n.- Type: Ultra High Frequency Inverter, 100kHz\r\n.- mA Range: Large 10mA to 200mA, Small 10mA to 60mA\r\n.- mAs Range: 1-600mAs\r\n.- kVp Range: 20kVp to 40kVp(9.5Vp Step)\r\n.- Power Input Required: 180-264VAC(50/60Hz), 6kVA(5KW), 1Phase\r\n.- Exposure Mode: Auto, Manual\r\nDisplay Module\r\n.- Main Display: 7 segment\r\nCollimation System\r\n.- Mode: Fully Automatic or User Selectable\r\nCompression System\r\n.- Compression Type: Motorized and Manual\r\n.- Max Compression Force: 20daN\r\n.- Pre-Compression: Smart compression control technology\r\n.- Compression Release: On/Off, Set up the Height\r\nMagnification\r\n.- Magnification Factor: 1.8x, 1.5x\r\n', 1, 'เครื่อง', '4800000.00', '0.00', '0.00', 1),
(34, 'T7000', 'T7000', 'Operating Table with Embedded Battery\r\nMain Device\r\n.- Electro-Hydraulic Operating Table\r\n.- Remote Controll wire/wireless Button Type\r\n.- Lateral/Tendelenburg ±18°/±28°\r\n.- Back section refraction +80°~-40°\r\n.- Leg sention refraction +15°~-90°\r\n.- Head section refraction +15°~-90°\r\n', 1, 'เตียง', '1200000.00', '0.00', '0.00', 1),
(35, 'T2000', 'T2000', 'Main Device\r\n.- Electro-Hydraulic Operating Table\r\n.- Lateral/Tendelenburg ±19°/±29°\r\n.- Back section refraction +70°~-40°\r\n.- Leg sention refraction +15°~-90°\r\n.- Head section refraction +15°~-90°\r\n', 1, 'เตียง', '580000.00', '0.00', '0.00', 1),
(36, 'E1000', 'E1000', '<p>1. Feature</p>\r\n\r\n<p>● Wireless foot switch system</p>\r\n\r\n<p>● Display lifting height and back plate degree for easy operation</p>\r\n\r\n<p>● Pre-setting function for preferred height and backplate degree</p>\r\n\r\n<p>● Arm rest for convenience of patient</p>\r\n\r\n<p>2. Dimension</p>\r\n\r\n<p>Overall Size W650*L1270*H480~1015mm</p>\r\n\r\n<p>Table Top W650*L1270mm</p>\r\n\r\n<p>Back Section Refraction . -5&deg;~50&deg;</p>\r\n\r\n<p>Hip Section Refraction 0&deg;~30&deg;</p>\r\n\r\n<p>Power Source AC220V, 50/60Hz</p>\r\n\r\n<p>Consumption 480W</p>\r\n\r\n<p>Fuse 5A</p>\r\n\r\n<p>Net Weight About 135kg</p>\r\n\r\n<p>Accessories Weight About 35kg</p>\r\n', 1, 'เตียง', '320000.00', '0.00', '0.00', 1),
(37, 'EX820', 'EX820', '1. Feature\r\n● Electro-hydra system\r\n● Memory function for automatic positioning\r\n● Comportable with low noise during operation\r\n● Can be used for operating table in emergency\r\n● Seat Warming System\r\n● Urology available\r\n2. Dimension\r\nOverall Size    W655*L1250*H460~1015mm\r\nTable Top     W655*L1250mm\r\nBack Section Refraction   . -10°~50°\r\nHip Section Refraction     0°~30°\r\nPower Source    AC220V, 50/60Hz\r\nConsumption   500W\r\nSeat Warming System     Electro-Hydrauric Mechanism', 1, 'เตียง', '290000.00', '0.00', '0.00', 1),
(38, 'KTR240FEE', 'KT-R240FEE', '2MP Medical Monitor\r\n27 inch LED Color IPS mode\r\n.- Pixel Pitch: 0.270(H) x 0.270(V) mm\r\n.- Contrast Ratio: 1000:1\r\n.- Luminance(Max): 350cd/m2\r\n', 1, 'ตัว', '80000.00', '0.00', '0.00', 1),
(39, 'KTD213Q5E', 'KT-D213Q5E', '3MP Medical Monitor\r\n.- CMC-3M\r\n.- 21.3inch TFT monochrome LCD IPS mode\r\n.- Display Area: 433.152(H) x 324.864(V) mm\r\n.- Aspect Ratio: 4:3\r\n', 1, 'จอ', '190000.00', '0.00', '0.00', 1),
(40, 'KTD213V5E', 'KT-D213V5E', '5MP Medical Monitor\r\n.- CMC-5M\r\n.- 21.3inch TFT monochrome LCD IPS mode\r\n.- Display Area: 422.4(H) x 337.9(V) mm\r\n.- Aspect Ratio: 5:4\r\n', 1, 'จอ', '300000.00', '0.00', '0.00', 1),
(41, '1417WCC', '1417WCC', '<p><strong>14x17 in 423 x 358 mm</strong></p>\r\n\r\n<p>Amorphous Silicon with TFT Cesium-Iodide(Cesium) &lt; 5.0sec.</p>\r\n\r\n<p>Data Acquisition time min. 50% / type 65% DQE Wireless type Manual Mode &amp; Auto Trigger Mode</p>\r\n', 2, 'ชิ้น', '1600000.00', '1.00', '1.00', 1),
(42, 'EVS4343', 'Exprimer EVS4343', '.- Pixel Pitch: 140nm\r\n.- Scintillator: CsI(Cesium Iodide)\r\n.- Image Matix Size: 3072 x 3072 pixels\r\n.- Effective Imaging Area(HxV): 430 x 430 mm\r\n.- Image Acquisition and Transfer Time: <3 sec.\r\n.- Spartial Resolution: Min. 3.5 line pair/mm\r\n.- Power Supply: DC+12V 2A Max.\r\n.- Power Consumption: 12W\r\n.- Network Interface: Gigabit Ethernet(1000Base-T)\r\n.- Dimension(mm): 460 x 460 x 15\r\n.- Weight: 4.5kg\r\n', 1, 'ชิ้น', '1300000.00', '0.00', '0.00', 1),
(43, '1717SCC', '1717SCC', '<p>17x17 in<br />\r\n423 x 423 mm<br />\r\nAmorphous Silicon with TFT<br />\r\nCsI:TI<br />\r\n&lt;3.9kg<br />\r\n3328 x 3328 pixel<br />\r\n127nm<br />\r\n40-150kV<br />\r\n14Bit<br />\r\n&lt; 5.0sec.<br />\r\ntype 65%<br />\r\nManual Mode &amp; Auto Trigger Mode<br />\r\nIndirect Type&nbsp;<br />\r\nPortable type</p>\r\n\r\n<p>&nbsp;</p>\r\n', 1, 'ชิ้น', '1400000.00', '0.00', '0.00', 1),
(44, 'FIRECR40', 'FIRECR40', 'Up to 30 plate/hour(IP 14x17")\r\nUniversal Cassette1417 & 1014\r\nData Capture 16bit per pixel, 65,000graytone\r\nDesktop & vertical wall mount set up type\r\nDICOM3.0 compliant\r\nDICOM Query send, receive\r\n1417 cassette 1ea 1014 cassette 1ea\r\n', 1, 'เครื่อง', '620000.00', '0.00', '0.00', 1),
(45, 'FIRECR60', 'FIRECR60', 'Up to 50 plate/hour(IP 14x17")\r\nUniversal Cassette1417 & 1014\r\nData Capture 16bit per pixel, 65,000graytone\r\nDesktop & vertical wall mount set up type\r\nDICOM3.0 compliant\r\nDICOM Query send, receive\r\n1417 cassette 1ea 1014 cassette 1ea\r\n', 1, 'เครื่อง', '710000.00', '0.00', '0.00', 1),
(46, 'FireCR80', 'FireCR80', 'Up to 70 plate/hour(IP 14x17")\r\nUniversal Cassette1417 & 1014\r\nData Capture 16bit per pixel, 65,000graytone\r\nDesktop & vertical wall mount set up type\r\nDICOM3.0 compliant\r\nDICOM Query send, receive\r\n1417 cassette 1ea 1014 cassette 1ea\r\n', 1, 'เครื่อง', '810000.00', '0.00', '0.00', 1),
(47, 'OSO PRO', 'Osteo Pro', '.- Classification: Claas 1, Type BF\r\n.- Ultrasound Parameter: BUA(Broadband Ultrasound Attenuation)\r\n.- SOS(Speed of Sound)\r\n.- OI(Osteoporosis Index)\r\n.- Diagnosing Parameter: OI, SOS, T-Score, Z-Score, %Young Adult, %Age Match, OPR\r\n.- Method and Transducer: Single Element Flat Type, Center Frequency\r\n.- Measurement Time: 15sec.\r\n.- Precision OI-in vivo <0.7%\r\n.- BUA - in vivo<0.2%\r\n.- SOS - in vivo<0.2%\r\n', 1, 'เครื่อง', '240000.00', '0.00', '0.00', 1),
(48, 'ACV A30', 'Accuvix A30', '1. Main Unit\r\n2. 3D Convex Probe(3DV4-8 or 3DV2-6) and 4D Option\r\n3. Covex Probe(C2-6IC) for OB/Gyn\r\n4. Vaginal Probe(VR5-9)\r\n5. USB DMR\r\n6. B/W Printer + Paper 4 rolls\r\n7. Gel 5 Liter\r\n8. UPS 2K\r\n9. Dicom\r\n', 1, 'เครื่อง', '3200000.00', '0.00', '0.00', 1),
(49, 'R7', 'R7', '.- USB DMR\r\n.- B/W Printer + Paper 4 rolls\r\n.- Gel 5 Liter\r\n.- UPS 1K\r\n.- 3D Convex Probe and 3D Option\r\n.- Covex Probe(C2-8)\r\n.- Vaginal Probe(EV4-9)\r\n.- Dicom support\r\n', 1, 'เครื่อง', '1500000.00', '0.00', '0.00', 1),
(50, 'HLUXLED120', 'Honey LUX LED 120', '.- Total Power Input: 160W\r\n.- Housing Diameter: 620mm\r\n.- Housing Controller: Membrance\r\n.- Wall Controller: Touch Button\r\n.- Max Illumination: 120,000Lux\r\n.- The number of LED: 88\r\n.- Service Life: 30,000 Hrs\r\n.- Focusing Function\r\n.- Light Field Diameter: 22-30cm\r\n.- Color Temperature: 4,300k\r\n.- CRI(Ra): >94\r\n.- Illumination Depth: 150cm\r\n.- Endo Light Function: 10Steps(5-100%)\r\n', 1, 'ชิ้น', '800000.00', '0.00', '0.00', 1),
(51, 'LUXLED 160', 'Honey LUX LED 160', '.- Total Power Input: 200W\r\n.- Housing Diameter: 770mm\r\n.- Housing Controller: Membrance\r\n.- Wall Controller: Touch Button\r\n.- Max Illumination: 160,000Lux\r\n.- The number of LED: 120\r\n.- Service Life: 30,000 Hrs\r\n.- Focusing Function\r\n.- Light Field Diameter: 25-34cm\r\n.- Color Temperature: 4,300k\r\n.- CRI(Ra): >94\r\n.- Illumination Depth: 113cm\r\n.- Endo Light Function: 10Steps(5-100%)\r\n', 1, 'ชิ้น', '900000.00', '0.00', '0.00', 1),
(52, 'DLUX D50', 'Dialux D50', '<p>- Diameter of Housing(mm): 500</p>\r\n\r\n<p>- Power Input: AC220V, 50/60Hz</p>\r\n\r\n<p>- Max Illumination: 80,000Lux</p>\r\n\r\n<p>- Color Temperature: 4,300k</p>\r\n\r\n<p>- Power of Bulb: 120W</p>\r\n\r\n<p>- Teperature Rise: &lt;3&deg;C</p>\r\n\r\n<p>- Lighting adjusting mode: 10 steps</p>\r\n\r\n<p>- Focusable Light Field: 120-180mm</p>\r\n\r\n<p>- Life of Bulb: 1,000hrs</p>\r\n', 1, 'ชิ้น', '550000.00', '0.00', '0.00', 1),
(53, 'DLUX D70', 'Dialux D70', '<p>- Diameter of Housing(mm): 700</p>\r\n\r\n<p>- Power Input: AC220V, 50/60Hz</p>\r\n\r\n<p>- Max Illumination: 150,000Lux</p>\r\n\r\n<p>- Color Temperature: 4,300k</p>\r\n\r\n<p>- Power of Bulb: 150W</p>\r\n\r\n<p>- Teperature Rise: &lt;3&deg;C</p>\r\n\r\n<p>- Lighting adjusting mode: 10 steps</p>\r\n\r\n<p>- Focusable Light Field: 140-200mm</p>\r\n\r\n<p>- Life of Bulb: 1,000hrs</p>\r\n', 1, 'ชิ้น', '600000.00', '0.00', '0.00', 1),
(54, 'PLD8600', 'PLD8600', 'X-Ray Generator(CPI Canada)\r\nTube(Toshiba)\r\nImage Intensifier(Toshiba)\r\nCCD(Adimec Mega Pixel CCD_Netherland)\r\n1. Power Output: 80kW\r\n2. Inverter Frequency: 200kHz\r\n3. Dual Focus: Large focus: 0.6, Small focus: 1.2\r\n4. Voltage: 380V±38V\r\n5. Frequency: 50Hz±1Hz\r\n6. Photography\r\n   6.1 Tube Voltage: 40kV~150kV\r\n   6.2 Tube Current: 10mA~1000mA\r\n   6.3 Exposure time: 1.0ms~6300ms\r\n   6.4 Control Interface: LCD touch screen\r\n7. Fluoroscopy\r\n   7.1 Tube Voltage: 40kV~125kV\r\n   7.2 Tube Current: 0.5mA~6mA\r\n  7.3 Qutomatic Brightness Tracking multiple setting beforehand\r\n', 1, 'เครื่อง', '4400000.00', '0.00', '0.00', 1),
(55, 'test', 'test', '<p>sffasdfasd</p>\r\n', 4, 'test', '100.00', '100.00', '100.00', 0),
(332, 'MS003', 'MS003 AIR WAY #0 (60MM)', NULL, 0, 'ชิ้น', '43.35', NULL, NULL, 1),
(333, 'MS005', 'MS005 AIR WAY #2 (80MM)', NULL, 0, 'ชิ้น', '43.50', NULL, NULL, 1),
(334, 'MS006', 'MS006 AIR WAY #3 (90MM)', NULL, 0, 'ชิ้น', '35.50', NULL, NULL, 1),
(335, 'MS007', 'MS007 AIR WAY #4 (100MM)', NULL, 0, 'ชิ้น', '38.99', NULL, NULL, 1),
(336, 'MS013', 'MS013 ไม้พันสำลี #M 6 inch', NULL, 0, 'ชิ้น', '2.45', NULL, NULL, 1),
(337, 'MS019', 'MS019 KNEE BRACE # S/M (SHORT)', NULL, 0, 'ชิ้น', '1396.35', NULL, NULL, 1),
(338, 'MS020', 'MS020 L-S SUPPORT # L (36 นิ้ว-40 นิ้ว)', '', 1, 'ชิ้น', '355.56', '0.00', '0.00', 1),
(339, 'MS038', 'MS038 ไม้พันสำลี #L 6 inch', NULL, 0, 'ชิ้น', '2.45', NULL, NULL, 1),
(340, 'MS051', 'MS051 KNEE SUPPORT #M', NULL, 0, 'ชิ้น', '191.35', NULL, NULL, 1),
(341, 'MS075', 'MS075 OXYGEN MASK WITH BAG ADULT', NULL, 0, 'ชิ้น', '86.65', NULL, NULL, 1),
(342, 'MS078', 'MS078 DIS.SYR. 5ML', NULL, 0, 'ชิ้น', '1.65', NULL, NULL, 1),
(343, 'MS086', 'MS086 ELASTIC BANDAGE 2 inch', NULL, 0, 'ชิ้น', '15.45', NULL, NULL, 1),
(344, 'MS087', 'MS087 ELASTIC BANDAGE 3 inch', NULL, 0, 'ชิ้น', '18.85', NULL, NULL, 1),
(345, 'MS088', 'MS088 ELASTIC BANDAGE 4 inch', NULL, 0, 'ชิ้น', '25.75', NULL, NULL, 1),
(346, 'MS089', 'MS089 ELASTIC BANDAGE 6 inch', NULL, 0, 'ชิ้น', '34.50', NULL, NULL, 1),
(347, 'MS102', 'MS102 SOFT COLLAR #XL', NULL, 0, 'ชิ้น', '156.15', NULL, NULL, 1),
(348, 'MS115', 'MS115 ENDO TUBE (O)#7.0(OR)', NULL, 0, 'ชิ้น', '269.65', NULL, NULL, 1),
(349, 'MS118', 'MS118 ENDO TUBE (O/N)#3.0 (PORTEX)', NULL, 0, 'ชิ้น', '123.65', NULL, NULL, 1),
(350, 'MS122', 'MS122 ENDO TUBE (O)#5.0(OR)', NULL, 0, 'ชิ้น', '269.00', NULL, NULL, 1),
(351, 'MS131', 'MS131 ENDO TUBE (O/N)#5.5 (PORTEX)', NULL, 0, 'ชิ้น', '115.00', NULL, NULL, 1),
(352, 'MS137', 'MS137 ENDO TUBE (O/N)#2.5 (PORTEX)', NULL, 0, 'ชิ้น', '123.65', NULL, NULL, 1),
(353, 'MS138', 'MS138 ENDO TUBE (O/N)#3.5 (PORTEX)', NULL, 0, 'ชิ้น', '123.65', NULL, NULL, 1),
(354, 'MS140', 'MS140 ENDO TUBE (O/N)#4.O (PORTEX)', NULL, 0, 'ชิ้น', '123.65', NULL, NULL, 1),
(355, 'MS142', 'MS142 ENDO TUBE (O/N)#5.0 (PORTEX)', NULL, 0, 'ชิ้น', '168.99', NULL, NULL, 1),
(356, 'MS146', 'MS146 EYE PAD 1 5/8 X 2 5/8 (HIVAN)', NULL, 0, 'ชิ้น', '4.85', NULL, NULL, 1),
(357, 'MS147', 'MS147 EYE SHIELD RIGHT(HIVAN)', NULL, 0, 'ชิ้น', '5.25', NULL, NULL, 1),
(358, 'MS148', 'MS148 EYE SHIELD LEFT (HIVAN)', NULL, 0, 'ชิ้น', '5.25', NULL, NULL, 1),
(359, 'MS150', 'MS150 EXTENSION TUBE 36 inch', '', 1, 'ชิ้น', '4.26', '0.00', '0.00', 1),
(360, 'MS157', 'MS157 FOLEY CATH #12 2WAY', NULL, 0, 'ชิ้น', '24.75', NULL, NULL, 1),
(361, 'MS158', 'MS158 FOLEY CATH #14 2WAY', '', 1, 'ชิ้น', '24.63', '0.00', '0.00', 1),
(362, 'MS159', 'MS159 FOLEY CATH #16 2WAY', NULL, 0, 'ชิ้น', '24.75', NULL, NULL, 1),
(363, 'MS160', 'MS160 FOLEY CATH #18 2WAY', NULL, 0, 'ชิ้น', '24.75', NULL, NULL, 1),
(364, 'MS161', 'MS161 FOLEY CATH #20 2WAY', NULL, 0, 'ชิ้น', '23.45', NULL, NULL, 1),
(365, 'MS162', 'MS162 FOLEY CATH #22 2WAY', NULL, 0, 'ชิ้น', '24.75', NULL, NULL, 1),
(366, 'MS163', 'MS163 FOLEY CATH #24 2WAY', NULL, 0, 'ชิ้น', '24.75', NULL, NULL, 1),
(367, 'MS164', 'MS164 FOLEY CATH #18 3WAY', NULL, 0, 'ชิ้น', '152.50', NULL, NULL, 1),
(368, 'MS166', 'MS166 FOLEY CATH #22 3WAY', NULL, 0, 'ชิ้น', '152.50', NULL, NULL, 1),
(369, 'MS183', 'MS183 GLOVE # 6', NULL, 0, 'ชิ้น', '23.65', NULL, NULL, 1),
(370, 'MS184', 'MS184 GLOVE # 6.5', '', 1, 'ชิ้น', '23.72', '0.00', '0.00', 1),
(371, 'MS185', 'MS185 GLOVE # 7', '', 1, 'ชิ้น', '23.72', '0.00', '0.00', 1),
(372, 'MS186', 'MS186 GLOVE # 7.5', '', 1, 'ชิ้น', '23.18', '0.00', '0.00', 1),
(373, 'MS187', 'MS187 GLOVE # 8', NULL, 0, 'ชิ้น', '22.00', NULL, NULL, 1),
(374, 'MS188', 'MS188 GLOVE #M NON-STERILE', NULL, 0, 'ชิ้น', '2.35', NULL, NULL, 1),
(375, 'MS200', 'MS200 HARD COLLAR #M', NULL, 0, 'ชิ้น', '192.60', NULL, NULL, 1),
(376, 'MS201', 'MS201 HARD COLLAR #L', NULL, 0, 'ชิ้น', '192.60', NULL, NULL, 1),
(377, 'MS205', 'MS205 MEDICUT #18 1 1/4 inch(TPC)', NULL, 0, 'ชิ้น', '18.55', NULL, NULL, 1),
(378, 'MS206', 'MS206 MEDICUT #20 1 1/4 inch(TPC)', NULL, 0, 'ชิ้น', '16.99', NULL, NULL, 1),
(379, 'MS209', 'MS209 MEDICUT #24 3/4 inch(BACK CUT)', NULL, 0, 'ชิ้น', '16.85', NULL, NULL, 1),
(380, 'MS223', 'MS223 L-S SUPPORT # M (32 นิ้ว-35 นิ้ว)', '', 1, 'ชิ้น', '355.56', '0.00', '0.00', 1),
(381, 'MS250', 'MS250 NASAL CANNULA ADULT', '', 1, 'ชิ้น', '25.87', '0.00', '0.00', 1),
(382, 'MS293', 'MS293 SOFT COLLAR #L', NULL, 0, 'ชิ้น', '144.45', NULL, NULL, 1),
(383, 'MS294', 'MS294 SOFT COLLAR #M', NULL, 0, 'ชิ้น', '144.45', NULL, NULL, 1),
(384, 'MS295', 'MS295 SOFT COLLAR #S', NULL, 0, 'ชิ้น', '144.45', NULL, NULL, 1),
(385, 'MS305', 'MS305 SUCTION CATH #5', NULL, 0, 'ชิ้น', '5.55', NULL, NULL, 1),
(386, 'MS306', 'MS306 SUCTION CATH #6', NULL, 0, 'ชิ้น', '6.25', NULL, NULL, 1),
(387, 'MS307', 'MS307 SUCTION CATH #8', '', 1, 'ชิ้น', '6.66', '0.00', '0.00', 1),
(388, 'MS308', 'MS308 SUCTION CATH #10', NULL, 0, 'ชิ้น', '6.65', NULL, NULL, 1),
(389, 'MS309', 'MS309 SUCTION CATH #12', NULL, 0, 'ชิ้น', '6.65', NULL, NULL, 1),
(390, 'MS310', 'MS310 SUCTION CATH #14', '', 1, 'ชิ้น', '7.18', '0.00', '0.00', 1),
(391, 'MS311', 'MS311 SUCTION CATH #16', NULL, 0, 'ชิ้น', '6.65', NULL, NULL, 1),
(392, 'MS312', 'MS312 SUCTION CATH #18', NULL, 0, 'ชิ้น', '6.25', NULL, NULL, 1),
(393, 'MS314', 'MS314 NG TUBE #6/100 CM', NULL, 0, 'ชิ้น', '9.90', NULL, NULL, 1),
(394, 'MS315', 'MS315 NG TUBE # 8/50CM', '', 1, 'ชิ้น', '8.61', '0.00', '0.00', 1),
(395, 'MS316', 'MS316 NG TUBE #8/100 CM', NULL, 0, 'ชิ้น', '8.95', NULL, NULL, 1),
(396, 'MS317', 'MS317 NG TUBE #12/125 CM', NULL, 0, 'ชิ้น', '10.60', NULL, NULL, 1),
(397, 'MS318', 'MS318 NG TUBE #14/125 CM', NULL, 0, 'ชิ้น', '16.55', NULL, NULL, 1),
(398, 'MS319', 'MS319 NG TUBE #16/125 CM', NULL, 0, 'ชิ้น', '17.50', NULL, NULL, 1),
(399, 'MS320', 'MS320 NG TUBE #18/125 CM', NULL, 0, 'ชิ้น', '10.65', NULL, NULL, 1),
(400, 'MS328', 'MS328 THREE WAY STOPCOCK', '', 1, 'ชิ้น', '14.53', '0.00', '0.00', 1),
(401, 'MS387', 'MS387 ชุดดึงไหปลาร้า # S', NULL, 0, 'ชิ้น', '173.35', NULL, NULL, 1),
(402, 'MS389', 'MS389 ชุดดึงไหปลาร้า # L', NULL, 0, 'ชิ้น', '173.35', NULL, NULL, 1),
(403, 'MS410', 'MS410 MEDICUT #22 1 inch(BACK CUT)', NULL, 0, 'ชิ้น', '16.85', NULL, NULL, 1),
(404, 'MS477', 'MS477 SPINAL NEED.#29 3 1/2', NULL, 0, 'ชิ้น', '49.25', NULL, NULL, 1),
(405, 'MS487', 'MS487 INJECTION PLUG', '', 1, 'ชิ้น', '10.94', '0.00', '0.00', 1),
(406, 'MS501', 'MS501 NG TUBE #5/50 CM', NULL, 0, 'ชิ้น', '7.55', NULL, NULL, 1),
(407, 'MS654', 'MS654 ENDO TUBE (O/N)#7.5 (PORTEX)', '', 1, 'ชิ้น', '168.16', '0.00', '0.00', 1),
(408, 'MS656', 'MS656 ENDO TUBE (O/N)#8.0 (PORTEX)', NULL, 0, 'ชิ้น', '168.99', NULL, NULL, 1),
(409, 'MS658', 'MS658 NEBULIZER WITH MASK(ADULT)1493', '', 1, 'ชิ้น', '95.80', '0.00', '0.00', 1),
(410, 'MS659', 'MS659 NEBULIZER WITH MASK (PED)1886', NULL, 0, 'ชิ้น', '115.00', NULL, NULL, 1),
(411, 'MS661', 'MS661 FOLEY CATH #6 2WAY', NULL, 0, 'ชิ้น', '195.99', NULL, NULL, 1),
(412, 'MS706', 'MS706 L-S SUPPORT # XL (41 นิ้ว-45 นิ้ว)', NULL, 0, 'ชิ้น', '346.65', NULL, NULL, 1),
(413, 'MS730', 'MS730 ENDO TUBE (O/N)#6.0 (PORTEX)', NULL, 0, 'ชิ้น', '168.99', NULL, NULL, 1),
(414, 'MS732', 'MS732 ENDO TUBE (O/N)#7.0 (PORTEX)', '', 1, 'ชิ้น', '168.16', '0.00', '0.00', 1),
(415, 'MS740', 'MS740 ARM SLING (GIRL)', NULL, 0, 'ชิ้น', '46.35', NULL, NULL, 1),
(416, 'MS741', 'MS741 ARM SLING (BOY)', NULL, 0, 'ชิ้น', '46.35', NULL, NULL, 1),
(417, 'MS759', 'MS759 KNEE BRACE # S/M (LONG)', NULL, 0, 'ชิ้น', '1588.95', NULL, NULL, 1),
(418, 'MS760', 'MS760 KNEE BRACE # L/XL (LONG)', NULL, 0, 'ชิ้น', '1588.95', NULL, NULL, 1),
(419, 'MS769', 'MS769 L-S SUPPORT # XXL', NULL, 0, 'ชิ้น', '346.75', NULL, NULL, 1),
(420, 'MS792', 'MS792 EXTENSION ขนาด 12 inch', NULL, 0, 'ชิ้น', '4.85', NULL, NULL, 1),
(421, 'MS793', 'MS793 EXTENSION ขนาด 18 inch', NULL, 0, 'ชิ้น', '4.80', NULL, NULL, 1),
(422, 'MS913', 'MS913 COLOSTOMY BAG # 38 MM', NULL, 0, 'ชิ้น', '11.00', NULL, NULL, 1),
(423, 'MS956', 'MS956 IV CONNECTOR WITH EXTENSION TUBE10CM', NULL, 0, 'ชิ้น', '82.45', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `prdimage`
--

CREATE TABLE IF NOT EXISTS `prdimage` (
  `PRD_IMAGE_ID` bigint(20) NOT NULL,
  `PRDCODE` bigint(20) NOT NULL,
  `PRD_IMAGE_PATH` varchar(255) NOT NULL,
  `STATUS` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `prdimage`
--

INSERT INTO `prdimage` (`PRD_IMAGE_ID`, `PRDCODE`, `PRD_IMAGE_PATH`, `STATUS`) VALUES
(10, 41, '41r6.jpg', 1),
(6, 41, '41r44.jpg', 1),
(5, 55, '55r22.jpg', 1),
(8, 55, '55r31.jpg', 1),
(9, 21, '21r61.png', 1),
(11, 41, '41r18.jpg', 1),
(12, 43, '43r70.jpg', 1),
(13, 43, '43r53.jpg', 1),
(14, 52, '52r4.jpg', 1),
(15, 53, '53r53.jpg', 1),
(16, 27, '27r23.jpg', 1),
(17, 27, '27r44.jpg', 1),
(18, 36, '36r98.jpg', 1),
(19, 36, '36r65.jpg', 1),
(20, 18, '18r47.jpg', 1),
(21, 23, '23r32.jpg', 1),
(22, 26, '26r59.jpg', 1),
(23, 24, '24r52.jpg', 1),
(24, 31, '31r37.jpg', 1),
(25, 19, '19r28.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `prdorder`
--

CREATE TABLE IF NOT EXISTS `prdorder` (
  `ORDERID` bigint(10) NOT NULL,
  `DR_PROVIDER` int(11) DEFAULT NULL,
  `D_ORDER` datetime DEFAULT NULL,
  `CONFIRM_PATH` varchar(250) DEFAULT NULL,
  `CONFIRM_STATUS` tinyint(1) NOT NULL DEFAULT '0',
  `STATUS` tinyint(1) DEFAULT NULL,
  `COMMENTORDER` text
) ENGINE=MyISAM AUTO_INCREMENT=65 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `prdorder`
--

INSERT INTO `prdorder` (`ORDERID`, `DR_PROVIDER`, `D_ORDER`, `CONFIRM_PATH`, `CONFIRM_STATUS`, `STATUS`, `COMMENTORDER`) VALUES
(30, 2, '2015-05-07 17:36:37', NULL, 1, 1, 'ราคารอ'),
(29, 2, '2015-05-07 17:20:01', NULL, 1, 1, NULL),
(28, 2, '2015-04-29 22:40:20', NULL, 1, 1, NULL),
(41, 3, '2015-05-19 11:46:05', NULL, 1, 0, NULL),
(26, 2, '2015-04-29 22:33:36', NULL, 1, 1, NULL),
(24, 2, '2015-04-29 22:26:21', '24r52.jpg', 1, 1, NULL),
(42, 3, '2015-05-19 12:00:19', NULL, 1, 0, NULL),
(43, 3, '2015-05-20 13:54:37', NULL, 1, 1, ''),
(44, 3, '2015-06-01 23:23:00', NULL, 1, 1, NULL),
(31, 2, '2015-05-07 18:56:01', NULL, 1, 1, ''),
(32, 2, '2015-05-08 17:26:58', NULL, 1, 1, ''),
(33, 2, '2015-05-08 17:42:08', NULL, 1, 1, 'dasdas'),
(34, 2, '2015-05-08 17:45:51', NULL, 0, 1, ''),
(35, 2, '2015-05-08 18:18:43', NULL, 0, 1, NULL),
(36, 2, '2015-05-08 18:21:58', NULL, 0, 1, NULL),
(37, 2, '2015-05-08 18:23:46', NULL, 0, 1, NULL),
(38, 2, '2015-05-08 18:24:14', NULL, 0, 1, NULL),
(39, 2, '2015-05-12 18:04:38', NULL, 1, 1, NULL),
(40, 3, '2015-05-19 08:21:01', '40r78.jpg', 1, 1, NULL),
(45, 3, '2015-06-05 10:26:13', NULL, 1, 1, NULL),
(46, 3, '2015-06-08 23:26:41', NULL, 1, 1, NULL),
(47, 3, '2015-06-09 18:37:48', NULL, 1, 1, NULL),
(48, 3, '2015-06-14 17:56:46', '48r51.pdf', 1, 1, NULL),
(49, 3, '2015-06-20 17:35:50', NULL, 0, 1, NULL),
(50, 3, '2015-06-20 17:45:07', NULL, 0, 1, NULL),
(51, 3, '2015-06-20 17:47:27', NULL, 0, 0, NULL),
(52, 3, '2015-06-20 17:55:44', NULL, 0, 0, NULL),
(53, 3, '2015-06-20 17:59:23', NULL, 0, 1, 'ตรวจสอบก่อน ทดสอบการเปลี่ยนความกว้างของคอลัมน์'),
(54, 3, '2015-07-02 11:10:31', NULL, 0, 0, NULL),
(55, 3, '2015-07-02 11:18:35', NULL, 0, 0, NULL),
(56, 9, '2015-07-03 10:51:42', '56r44.jpg', 1, 1, NULL),
(57, 9, '2015-07-03 11:26:28', NULL, 0, 0, NULL),
(58, 9, '2015-07-03 11:31:53', NULL, 1, 1, ''),
(59, 3, '2015-07-09 16:32:11', NULL, 0, 1, NULL),
(60, 3, '2015-07-09 16:35:00', NULL, 0, 1, NULL),
(61, 3, '2015-07-09 16:36:49', NULL, 0, 1, NULL),
(62, 3, '2015-07-09 16:40:07', NULL, 0, 1, NULL),
(63, 3, '2015-07-24 15:24:52', NULL, 0, 1, NULL),
(64, 3, '2015-07-24 15:25:36', NULL, 0, 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `prdorderdt`
--

CREATE TABLE IF NOT EXISTS `prdorderdt` (
  `ID` bigint(20) NOT NULL,
  `ORDERID` bigint(20) DEFAULT NULL,
  `PRDCODE` bigint(20) DEFAULT NULL,
  `QTY` float DEFAULT NULL,
  `PRICE` bigint(20) DEFAULT NULL,
  `STATUS` tinyint(1) DEFAULT '1',
  `COMMENTORDERDT` text,
  `CONFIRM_STATUS` tinyint(1) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=170 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `prdorderdt`
--

INSERT INTO `prdorderdt` (`ID`, `ORDERID`, `PRDCODE`, `QTY`, `PRICE`, `STATUS`, `COMMENTORDERDT`, `CONFIRM_STATUS`) VALUES
(80, 41, 10, 10, NULL, 1, NULL, 0),
(86, 44, 18, 1, NULL, 1, NULL, 0),
(85, 43, 21, 2, NULL, 1, '', 2),
(84, 43, 18, 1, NULL, 1, '', 2),
(87, 44, 21, 1, NULL, 1, NULL, 0),
(79, 41, 18, 2, NULL, 1, NULL, 0),
(26, 24, 4, 2, NULL, 1, NULL, 0),
(27, 24, 3, 2, NULL, 1, NULL, 2),
(83, 42, 5, 7, NULL, 1, NULL, 0),
(82, 42, 21, 8, NULL, 1, NULL, 0),
(30, 26, 5, 1, NULL, 1, NULL, 0),
(81, 42, 18, 1, NULL, 1, NULL, 0),
(32, 28, 5, 10, NULL, 1, NULL, 0),
(33, 28, 7, 10, NULL, 1, NULL, 2),
(34, 29, 9, 2, NULL, 1, 'กหฟกฟห', 2),
(35, 29, 11, 1, NULL, 1, NULL, 0),
(36, 29, 7, 4, NULL, 1, NULL, 0),
(37, 29, 13, 5, NULL, 1, NULL, 0),
(38, 29, 5, 7, NULL, 1, NULL, 0),
(39, 29, 15, 8, NULL, 1, NULL, 0),
(40, 30, 5, 80, NULL, 1, 'ทดวอบการกฟหก', 0),
(41, 30, 6, 50, NULL, 1, 'ทดกฟหกฟห', 0),
(42, 31, 5, 10, NULL, 1, '', 1),
(43, 31, 6, 20, NULL, 1, '', 1),
(44, 31, 7, 30, NULL, 1, '', 1),
(45, 31, 8, 40, NULL, 1, '', 1),
(46, 31, 9, 50, NULL, 1, '', 1),
(47, 31, 10, 60, NULL, 1, '', 1),
(48, 31, 11, 70, NULL, 1, '', 2),
(49, 31, 12, 80, NULL, 1, '', 1),
(50, 31, 13, 90, NULL, 1, '', 1),
(51, 31, 14, 100, NULL, 1, '', 1),
(52, 31, 15, 200, NULL, 1, '', 1),
(53, 31, 1, 300, NULL, 1, '', 1),
(54, 31, 2, 400, NULL, 1, '', 1),
(55, 31, 16, 500, NULL, 1, '', 1),
(56, 31, 17, 600, NULL, 1, '', 1),
(57, 31, 3, 700, NULL, 1, '', 2),
(58, 31, 4, 800, NULL, 1, '', 1),
(59, 32, 5, 1, NULL, 1, '', 1),
(60, 32, 7, 2, NULL, 1, '', 2),
(61, 32, 9, 3, NULL, 1, '', 2),
(62, 32, 12, 4, NULL, 1, '', 1),
(63, 32, 6, 5, NULL, 1, '', 1),
(64, 33, 5, 2, NULL, 1, '', 1),
(65, 34, 5, 1, NULL, 1, '55', 0),
(66, 35, 5, 1, NULL, 1, 'ds', 0),
(67, 35, 6, 2, NULL, 1, NULL, 0),
(68, 35, 8, 3, NULL, 1, NULL, 0),
(69, 36, 6, 2, NULL, 1, NULL, 0),
(70, 36, 7, 1, NULL, 1, NULL, 0),
(71, 37, 5, 9, NULL, 1, NULL, 0),
(72, 37, 6, 8, NULL, 1, NULL, 0),
(73, 38, 5, 10, NULL, 1, NULL, 0),
(74, 38, 6, 9, NULL, 1, NULL, 0),
(75, 39, 2, 12, NULL, 1, NULL, 0),
(76, 39, 3, 1, NULL, 1, NULL, 0),
(77, 40, 5, 100, NULL, 1, NULL, 0),
(78, 40, 14, 5, NULL, 1, NULL, 0),
(88, 45, 43, 4, NULL, 1, NULL, 0),
(89, 45, 48, 3, NULL, 1, NULL, 0),
(90, 46, 21, 10, NULL, 1, NULL, 0),
(91, 46, 51, 9, NULL, 1, NULL, 0),
(92, 46, 46, 8, NULL, 1, NULL, 0),
(93, 46, 50, 7, NULL, 1, NULL, 0),
(94, 46, 38, 6, NULL, 1, NULL, 0),
(95, 46, 45, 5, NULL, 1, NULL, 0),
(96, 46, 16, 4, NULL, 1, NULL, 0),
(97, 46, 44, 3, NULL, 1, NULL, 0),
(98, 46, 53, 2, NULL, 1, NULL, 0),
(99, 46, 35, 1, NULL, 1, NULL, 0),
(100, 46, 15, 10, NULL, 1, NULL, 0),
(101, 46, 5, 9, NULL, 1, NULL, 0),
(102, 46, 52, 8, NULL, 1, NULL, 0),
(103, 46, 12, 7, NULL, 1, NULL, 0),
(104, 46, 32, 6, NULL, 1, NULL, 0),
(105, 46, 4, 5, NULL, 1, NULL, 0),
(106, 47, 43, 10, NULL, 1, NULL, 0),
(107, 47, 48, 9, NULL, 1, NULL, 0),
(108, 47, 52, 8, NULL, 1, NULL, 0),
(109, 47, 53, 7, NULL, 1, NULL, 0),
(110, 47, 27, 6, NULL, 1, NULL, 0),
(111, 47, 36, 5, NULL, 1, NULL, 0),
(112, 47, 18, 4, NULL, 1, NULL, 0),
(113, 47, 23, 3, NULL, 1, NULL, 0),
(114, 47, 26, 2, NULL, 1, NULL, 0),
(115, 47, 24, 1, NULL, 1, NULL, 0),
(116, 47, 25, 10, NULL, 1, NULL, 0),
(117, 47, 42, 9, NULL, 1, NULL, 0),
(118, 47, 37, 8, NULL, 1, NULL, 0),
(119, 47, 44, 7, NULL, 1, NULL, 0),
(120, 47, 45, 6, NULL, 1, NULL, 0),
(121, 47, 46, 5, NULL, 1, NULL, 0),
(122, 47, 21, 4, NULL, 1, NULL, 0),
(123, 47, 50, 3, NULL, 1, NULL, 0),
(124, 47, 39, 2, NULL, 1, NULL, 0),
(125, 47, 40, 1, NULL, 1, NULL, 0),
(126, 47, 38, 10, NULL, 1, NULL, 0),
(127, 47, 51, 9, NULL, 1, NULL, 0),
(128, 47, 5, 8, NULL, 1, NULL, 0),
(129, 47, 6, 7, NULL, 1, NULL, 0),
(130, 47, 7, 6, NULL, 1, NULL, 0),
(131, 47, 8, 5, NULL, 1, NULL, 0),
(132, 47, 9, 4, NULL, 1, NULL, 0),
(133, 47, 10, 3, NULL, 1, NULL, 0),
(134, 47, 11, 2, NULL, 1, NULL, 0),
(135, 47, 12, 1, NULL, 1, NULL, 0),
(136, 47, 13, 10, NULL, 1, NULL, 0),
(137, 47, 14, 9, NULL, 1, NULL, 0),
(138, 47, 15, 8, NULL, 1, NULL, 0),
(139, 47, 47, 7, NULL, 1, NULL, 0),
(140, 47, 54, 6, NULL, 1, NULL, 0),
(141, 47, 28, 5, NULL, 1, NULL, 0),
(142, 47, 29, 4, NULL, 1, NULL, 0),
(143, 47, 30, 3, NULL, 1, NULL, 0),
(144, 47, 20, 2, NULL, 1, NULL, 0),
(145, 47, 19, 1, NULL, 1, NULL, 0),
(146, 47, 49, 10, NULL, 1, NULL, 0),
(147, 47, 1, 9, NULL, 1, NULL, 0),
(148, 47, 2, 8, NULL, 1, NULL, 0),
(149, 47, 16, 7, NULL, 1, NULL, 0),
(150, 47, 17, 6, NULL, 1, NULL, 0),
(151, 47, 33, 5, NULL, 1, NULL, 0),
(152, 47, 32, 4, NULL, 1, NULL, 0),
(153, 47, 31, 3, NULL, 1, NULL, 0),
(154, 47, 3, 2, NULL, 1, NULL, 0),
(155, 47, 4, 1, NULL, 1, NULL, 0),
(156, 48, 43, 1, 1400000, 1, NULL, 0),
(157, 53, 41, 2, 1, 1, NULL, 0),
(158, 54, 361, 100, 25, 1, NULL, 0),
(159, 54, 366, 100, 25, 1, NULL, 0),
(160, 55, 27, 1, 5500000, 1, NULL, 0),
(161, 56, 48, 1, 3200000, 1, NULL, 0),
(162, 57, 53, 1, 600000, 1, NULL, 0),
(163, 58, 18, 2, 5100000, 1, '', 2),
(164, 59, 41, 1, 1600000, 1, NULL, 0),
(165, 60, 26, 2, 2500000, 1, NULL, 0),
(166, 61, 43, 2, 1400000, 1, NULL, 0),
(167, 62, 18, 2, 5100000, 1, NULL, 0),
(168, 63, 41, 1, 1600000, 1, NULL, 0),
(169, 64, 43, 1, 1400000, 1, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `prdorderfact`
--

CREATE TABLE IF NOT EXISTS `prdorderfact` (
  `ORDERFACID` int(11) NOT NULL,
  `USERFAC_ID` int(11) DEFAULT NULL,
  `D_ORDER` datetime DEFAULT NULL,
  `CONFIRM_STATUS` tinyint(1) DEFAULT NULL,
  `STATUS` tinyint(1) DEFAULT NULL,
  `COMMENTORDERFACT` text,
  `D_CONFIRM` datetime DEFAULT NULL,
  `D_FINISH` datetime DEFAULT NULL,
  `CONFIRM_FIN` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `prdorderfact`
--

INSERT INTO `prdorderfact` (`ORDERFACID`, `USERFAC_ID`, `D_ORDER`, `CONFIRM_STATUS`, `STATUS`, `COMMENTORDERFACT`, `D_CONFIRM`, `D_FINISH`, `CONFIRM_FIN`) VALUES
(1, 3, NULL, 0, 1, NULL, '0000-00-00 00:00:00', NULL, 0),
(2, 4, '2015-06-22 09:41:26', 1, 1, '0', NULL, NULL, 0),
(3, 4, '2015-06-22 09:41:26', 0, 1, 'dsadasdas', NULL, NULL, 0),
(4, 5, '2015-07-03 11:15:07', 0, 1, '', NULL, NULL, 0),
(5, 5, '2015-07-24 15:29:35', 1, 1, '', NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `prdorderfactdt`
--

CREATE TABLE IF NOT EXISTS `prdorderfactdt` (
  `IDORDERFACTDT` int(11) NOT NULL,
  `IDPRDORDER` int(11) DEFAULT NULL,
  `QTY` int(11) DEFAULT NULL,
  `ORDERFACID` int(11) DEFAULT NULL,
  `PRICE` int(11) DEFAULT NULL,
  `COMMENTORDERDT` text,
  `CONFIRM_STATUS` tinyint(1) DEFAULT NULL,
  `STATUS` tinyint(1) DEFAULT NULL,
  `IDMASPRODUCT` int(11) DEFAULT NULL,
  `IDPRDORDERDT` int(11) DEFAULT NULL,
  `CONFIRM_FIN` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `prdorderfactdt`
--

INSERT INTO `prdorderfactdt` (`IDORDERFACTDT`, `IDPRDORDER`, `QTY`, `ORDERFACID`, `PRICE`, `COMMENTORDERDT`, `CONFIRM_STATUS`, `STATUS`, `IDMASPRODUCT`, `IDPRDORDERDT`, `CONFIRM_FIN`) VALUES
(2, 28, 10, 2, 1500, '', 0, 1, 7, 33, 0),
(3, 29, 2, 2, 750, '', 0, 1, 9, 34, 0),
(4, 24, 2, 2, 1000, '', 0, 1, 3, 27, 0),
(5, 28, 10, 2, 1500, '', 0, 1, 7, 33, 0),
(7, 24, 2, 2, 1000, '', 0, 1, 3, 27, 0),
(8, 29, 2, 3, 750, '', 0, 1, 9, 34, 0),
(10, 31, 70, 3, 200, '', 0, 1, 11, 48, 0),
(12, 31, 700, 3, 1000, '', 0, 1, 3, 57, 0),
(13, 32, 3, 3, 750, '', 0, 1, 9, 61, 0),
(17, 32, 2, 3, 1500, '', 0, 1, 7, 60, 0),
(19, 58, 2, 5, 4500000, '', 1, 1, 18, 163, 0),
(20, 43, 2, 5, 10000, '', 1, 1, 21, 85, 0),
(21, 43, 1, 5, 4500000, '', 1, 1, 18, 84, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `USER_ID` int(15) NOT NULL COMMENT 'PK',
  `USER_ACCOUNT` varchar(15) DEFAULT NULL COMMENT 'ชื่อ login',
  `USER_PASSWORD` varchar(15) DEFAULT NULL COMMENT 'password',
  `USER_NAME` varchar(50) DEFAULT NULL COMMENT 'Name',
  `USER_LNAME` varchar(50) DEFAULT NULL COMMENT 'Surname',
  `USER_COMPANY` varchar(150) DEFAULT NULL,
  `USER_ADDRESS` text COMMENT 'Address',
  `USER_TEL` varchar(50) DEFAULT NULL COMMENT 'Telephone',
  `USER_EMAIL` varchar(50) DEFAULT NULL COMMENT 'Email',
  `USER_ADD_DATE` datetime DEFAULT NULL COMMENT 'become member date',
  `USER_EDIT_DATE` datetime DEFAULT NULL COMMENT 'edit member date',
  `STATUS` tinyint(1) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`USER_ID`, `USER_ACCOUNT`, `USER_PASSWORD`, `USER_NAME`, `USER_LNAME`, `USER_COMPANY`, `USER_ADDRESS`, `USER_TEL`, `USER_EMAIL`, `USER_ADD_DATE`, `USER_EDIT_DATE`, `STATUS`) VALUES
(2, 'test', '1234', 'ทดสอบ', 'ระบบ', 'โรงพยาบาลสุโขทัยธรรมธิราชนครินทร์', '127/55 หมู่บ้านสายลม ตำบลสายรัก อำเภออกหัก จังหวัดรักเธอ นะจ๊ะ', '074-225776-896', 'example@example.com', '2015-05-03 16:08:00', '2015-08-14 10:35:25', 0),
(3, 'chavee', '1234', 'ชาวี', 'อมราภรณ์', 'อมราภรณ์ คอเปอเรชั่น', 'อมราภรณ์คอเปอเรชั่น แขวงบางคอแหลม ลาดปลาเค้า กรุงเทพมหานคร 10400', '02-6665555', 'chavee@hotmail.com', '2015-05-18 22:34:14', '2015-06-30 13:44:09', 1),
(4, 'araya', '1234', 'อารยา', 'อมราภรณ์', 'อมราภรณ์ คอเปอเรชั่น', 'อมราภรณ์คอเปอเรชั่น แขวงบางคอแหลม ลาดปลาเค้า กรุงเทพมหานคร 10400', '02-7778899', 'araya_not_chompoo@gmail.com', '2015-05-18 22:38:22', '2015-06-09 08:47:23', 0),
(5, 'dasdsa', 'dasdsad', 'ทดสอบ', 'dasdas', 'as', 'dasdasdas', 'as', 'asda', '2015-06-08 10:13:12', '2015-06-08 23:11:31', 1),
(6, 'Seanine', '2558', 'คุณต้า', '', 'Seanine', '119 ถ.ราษฎรร์ยินดี  ต.หาดใหญ่ อ.หาดใหญ่  สงขลา', '1234567890', '', '2015-07-02 10:53:06', '2015-07-02 10:53:06', 0),
(7, 'Seanineป', '2558', 'คุณต้า', '', 'Seanine', '119 ถ.ราษฎรร์ยินดี  ต.หาดใหญ่ อ.หาดใหญ่  สงขลา', '1234567890', '', '2015-07-02 10:54:48', '2015-07-02 10:54:48', 0),
(8, '', '', '', '', '', '', '', '', '2015-07-02 11:00:14', '2015-07-02 11:00:14', 0),
(9, 'Beer', '2558', 'Beer', 'Panichgul', 'Beer MEd', '119 xxxx', '0931249097', 'bpanichgul@gmail.com', '2015-07-03 10:50:52', '2015-07-03 10:50:52', 1),
(10, 'โรงพยาบาลสิโรรส', 'siroros', '', '', 'แผนกจัดซื้อ', '246-249 ถนน สิโรรส ตำบล สะเตง อำเภอ เมืองยะลา จังหวัด ยะลา 95000', '073-228-520', 'tur2513@gmail.com', '2015-08-14 10:34:12', '2015-08-14 10:38:37', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_manufacturer`
--

CREATE TABLE IF NOT EXISTS `user_manufacturer` (
  `USERFAC_ID` int(11) NOT NULL,
  `USERFAC_ACCOUNT` varchar(15) DEFAULT NULL,
  `USERFAC_PASSWORD` varchar(15) DEFAULT NULL,
  `USERFAC_NAME` varchar(45) DEFAULT NULL,
  `USERFAC_LNAME` varchar(45) DEFAULT NULL,
  `USERFAC_COMPANY` varchar(100) DEFAULT NULL,
  `USERFAC_ADDRESS` text,
  `USERFAC_TEL` varchar(45) DEFAULT NULL,
  `USERFAC_EMAIL` varchar(45) DEFAULT NULL,
  `USERFAC_ADD_DATE` datetime DEFAULT NULL,
  `USERFAC_EDIT_DATE` datetime DEFAULT NULL,
  `STATUS` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_manufacturer`
--

INSERT INTO `user_manufacturer` (`USERFAC_ID`, `USERFAC_ACCOUNT`, `USERFAC_PASSWORD`, `USERFAC_NAME`, `USERFAC_LNAME`, `USERFAC_COMPANY`, `USERFAC_ADDRESS`, `USERFAC_TEL`, `USERFAC_EMAIL`, `USERFAC_ADD_DATE`, `USERFAC_EDIT_DATE`, `STATUS`) VALUES
(1, 'June', 'Admin', 'นายยุทธกร', 'ทิพย์รักษ์', 'dasdas', 'rajdasdasdas dasdaseoeqweqweqweqwe', '093108714', 'junezuku@hotmail.com', '2015-06-08 11:21:33', '2015-06-11 17:36:43', 1),
(2, '', '', '', '', '', '', '', '', '2015-06-08 13:35:47', '2015-06-08 13:35:47', 1),
(3, 'ฟหกฟหกฟห', 'กฟหก', 'กฟหกฟหกฟหกฟหก', 'ฟหกฟหกฟห', 'กฟหกฟหก', 'กฟหกฟหก', 'ฟหก', 'กฟหกฟห', '2015-06-09 08:48:03', '2015-06-09 08:48:03', 1),
(4, 'yingyah', '1234', 'คุณหญิงย่า', 'อมราภรณ์', 'อมราภรณ์คอเปอเรชั่น ลิมิเตด เอดิชั่น', '500 - 1000 ถ.อโศกเพชรบุรี แขวง บางมด อ.เมือง จ.กรุงเทพมหานครฯ 10400', '0558498498', 'yingyah@gmail.com', NULL, NULL, 1),
(5, 'atoz2558', '2555', 'Udomchai', '', 'Atoz', '112  ถ.ราษฎร์อุทิศ  ต.คอหงษ์  อ.หาดใหญ่  สงขลา', '2345678901', '', '2015-07-02 11:05:25', '2015-07-02 11:05:25', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`USER_ID`);

--
-- Indexes for table `masprdcats`
--
ALTER TABLE `masprdcats`
  ADD PRIMARY KEY (`PRDCAT`);

--
-- Indexes for table `masprdtomanufacturer`
--
ALTER TABLE `masprdtomanufacturer`
  ADD PRIMARY KEY (`IDFACT`);

--
-- Indexes for table `masproduct`
--
ALTER TABLE `masproduct`
  ADD PRIMARY KEY (`PRDCODE`), ADD UNIQUE KEY `CODE` (`CODE`);

--
-- Indexes for table `prdimage`
--
ALTER TABLE `prdimage`
  ADD PRIMARY KEY (`PRD_IMAGE_ID`);

--
-- Indexes for table `prdorder`
--
ALTER TABLE `prdorder`
  ADD PRIMARY KEY (`ORDERID`);

--
-- Indexes for table `prdorderdt`
--
ALTER TABLE `prdorderdt`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `prdorderfact`
--
ALTER TABLE `prdorderfact`
  ADD PRIMARY KEY (`ORDERFACID`);

--
-- Indexes for table `prdorderfactdt`
--
ALTER TABLE `prdorderfactdt`
  ADD PRIMARY KEY (`IDORDERFACTDT`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`USER_ID`), ADD UNIQUE KEY `USER_ACCOUNT` (`USER_ACCOUNT`);

--
-- Indexes for table `user_manufacturer`
--
ALTER TABLE `user_manufacturer`
  ADD PRIMARY KEY (`USERFAC_ID`), ADD UNIQUE KEY `USERFAC_ACCOUNT_UNIQUE` (`USERFAC_ACCOUNT`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `USER_ID` int(15) NOT NULL AUTO_INCREMENT COMMENT 'PK',AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `masprdcats`
--
ALTER TABLE `masprdcats`
  MODIFY `PRDCAT` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `masprdtomanufacturer`
--
ALTER TABLE `masprdtomanufacturer`
  MODIFY `IDFACT` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `masproduct`
--
ALTER TABLE `masproduct`
  MODIFY `PRDCODE` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=424;
--
-- AUTO_INCREMENT for table `prdimage`
--
ALTER TABLE `prdimage`
  MODIFY `PRD_IMAGE_ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `prdorder`
--
ALTER TABLE `prdorder`
  MODIFY `ORDERID` bigint(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=65;
--
-- AUTO_INCREMENT for table `prdorderdt`
--
ALTER TABLE `prdorderdt`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=170;
--
-- AUTO_INCREMENT for table `prdorderfact`
--
ALTER TABLE `prdorderfact`
  MODIFY `ORDERFACID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `prdorderfactdt`
--
ALTER TABLE `prdorderfactdt`
  MODIFY `IDORDERFACTDT` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `USER_ID` int(15) NOT NULL AUTO_INCREMENT COMMENT 'PK',AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `user_manufacturer`
--
ALTER TABLE `user_manufacturer`
  MODIFY `USERFAC_ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
