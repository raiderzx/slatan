-- phpMyAdmin SQL Dump
-- version 3.5.8.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 03, 2015 at 04:31 PM
-- Server version: 5.1.73
-- PHP Version: 5.3.28

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `rajyinde_slatan`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `USER_ID` int(15) NOT NULL AUTO_INCREMENT COMMENT 'PK',
  `USER_ACCOUNT` varchar(15) DEFAULT NULL COMMENT 'ชื่อ login',
  `USER_PASSWORD` varchar(15) DEFAULT NULL COMMENT 'password',
  `USER_NAME` varchar(50) DEFAULT NULL COMMENT 'Name',
  `USER_LNAME` varchar(50) DEFAULT NULL COMMENT 'Surname',
  `USER_ADD_DATE` datetime DEFAULT NULL COMMENT 'become member date',
  `USER_EDIT_DATE` datetime DEFAULT NULL COMMENT 'edit member date',
  `STATUS` tinyint(1) NOT NULL,
  PRIMARY KEY (`USER_ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`USER_ID`, `USER_ACCOUNT`, `USER_PASSWORD`, `USER_NAME`, `USER_LNAME`, `USER_ADD_DATE`, `USER_EDIT_DATE`, `STATUS`) VALUES
(1, 'au', '1234', 'Admin', 'Anukul', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `masprdcats`
--

CREATE TABLE IF NOT EXISTS `masprdcats` (
  `PRDCAT` int(11) NOT NULL AUTO_INCREMENT,
  `CATNAME` varchar(75) NOT NULL,
  PRIMARY KEY (`PRDCAT`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `masprdcats`
--

INSERT INTO `masprdcats` (`PRDCAT`, `CATNAME`) VALUES
(1, 'อุปกรณ์ทางการแพทย์'),
(2, 'เวชภัณฑ์'),
(3, 'เคมีภัณฑ์'),
(4, 'อื่นๆ');

-- --------------------------------------------------------

--
-- Table structure for table `masproduct`
--

CREATE TABLE IF NOT EXISTS `masproduct` (
  `PRDCODE` bigint(20) NOT NULL AUTO_INCREMENT,
  `CODE` varchar(20) NOT NULL,
  `PRDNAME` varchar(75) DEFAULT NULL,
  `PRDDESC` text,
  `PRDCAT` int(11) NOT NULL,
  `UNIT` varchar(45) DEFAULT NULL,
  `PRDIMG_PATH` varchar(255) DEFAULT NULL,
  `PRICE` decimal(12,2) DEFAULT NULL,
  `LAST_COST` decimal(12,2) DEFAULT NULL,
  `AVG_COST` decimal(12,2) DEFAULT NULL,
  `STATUS` tinyint(4) NOT NULL,
  PRIMARY KEY (`PRDCODE`),
  UNIQUE KEY `CODE` (`CODE`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=55 ;

--
-- Dumping data for table `masproduct`
--

INSERT INTO `masproduct` (`PRDCODE`, `CODE`, `PRDNAME`, `PRDDESC`, `PRDCAT`, `UNIT`, `PRDIMG_PATH`, `PRICE`, `LAST_COST`, `AVG_COST`, `STATUS`) VALUES
(1, 'S001', 'Probac 10 Plus', NULL, 2, 'box', NULL, '250.50', NULL, NULL, 1),
(2, 'S002', 'Ascorbic acid', NULL, 2, 'box', NULL, '108.75', NULL, NULL, 1),
(3, 'T001', 'Myers cocktail', NULL, 2, 'ea', NULL, '1200.00', NULL, NULL, 1),
(4, 'T002', 'Procaine HCL 2%', NULL, 2, 'Bottle', NULL, '5600.00', NULL, NULL, 1),
(5, 'M001', 'สำลี', '', 1, 'kg.', NULL, '550.00', '45.00', '475.00', 1),
(6, 'M002', 'ผ้าพันแผล', NULL, 1, 'Pack', NULL, '1200.00', NULL, NULL, 1),
(7, 'M003', 'Mask', NULL, 1, 'Box', NULL, '450.00', NULL, NULL, 1),
(8, 'M004', 'ผ้าอ้อม', NULL, 1, 'Pack', NULL, '450.00', NULL, NULL, 1),
(9, 'M005', 'Syringe', NULL, 1, 'Pack', NULL, '1000.00', NULL, NULL, 1),
(10, 'M006', 'Cotton Budge', NULL, 1, 'Pack', NULL, '100.00', NULL, NULL, 1),
(11, 'M007', 'หมวกคลุมผม', NULL, 1, 'Box', NULL, '250.00', NULL, NULL, 1),
(12, 'M008', 'กรรไกรตัดไหม', NULL, 1, 'ea', NULL, '50.00', NULL, NULL, 1),
(13, 'M009', 'ผ้าก๊อซ(เส้นใยไม่ติดแผล)', NULL, 1, 'Pack', NULL, '250.00', NULL, NULL, 1),
(14, 'M010', 'กระจกส่องฟัน', NULL, 1, 'ea', NULL, '110.00', NULL, NULL, 1),
(15, 'M011', 'ไฟฉายส่องหู', NULL, 1, 'ea', NULL, '560.00', NULL, NULL, 1),
(16, 'S003', 'Ergotamine tartrate', NULL, 2, 'Box', NULL, '700.00', NULL, NULL, 1),
(17, 'S004', 'Hard Mask', 'ผ้าปิดปากปิดจมูกแบบแข็ง', 1, 'Piece', '17r40.jpg', '30.00', '11.00', '10.00', 1),
(18, 'ECOV9', 'EcoView9 Plus', 'Digital Radiography System with Dual Flat Panel Detectors\r\n50kW HF Generator(630mA/150kV) with LCD OP Console\r\nHigh speed starter(Include in the generator)\r\n300kHU rotating anode tube(0.6/1.2mm focal spot)\r\nCeiling Suspended Tube Stand (Up-Down motorized) with auto-tracking\r\nAuto Collimator\r\n6way Elevating table with 1717 fixed FPD(Synchronized to X-ray tube)\r\nUp-Righr Wall stand with 1717 fixed FPD (Synchronized to X-ray Tube)\r\nDR Grid 2ea\r\nEoView DR S/W with workstation\r\nHV cable 12mm long\r\n380VAC 50/60Hz Three phase', 1, 'เครื่อง', 'r42.jpg', '5100000.00', '5000000.00', '5000000.00', 1),
(19, 'PX300HF', 'PX-300HF', '1. 30kW HF Generator\r\n2. X-ray Tube:0.6/1.2mm focal\r\n3. Motor driven system: rear wheel Drive\r\n4. X-Ray tube support with telescopic\r\nSID:1500mm\r\nTube Are reach: 1310mm\r\n5. Manual Collimator\r\n6. WiFi Wireless FPD\r\n7. Acquisition S/W with Workstation, Monitor\r\n8. Input power:100-240VAC, 1KVA', 1, 'เครื่อง', 'r99.jpg', '3500000.00', '3000000.00', '3000000.00', 1),
(20, 'PVDRP', 'Pink View DR Plus', 'Mammography System Pinkview-DR\r\n1) Generator\r\n - Type : High Frequency Inverter\r\n - Input Power : Single phase 220V AC, 50/60 Hz\r\n - Power Rating\r\n     Large Focus : 20-39kV / 100-720mAs\r\n     Small Focus : 20-39kV / 100-350mAs\r\n - Max . mA Current 100mA@28kV\r\n2) X-Ray Tube\r\n - Focal Spot Size : 0.1mm / 0.3mm\r\n - Target Material : W(Tungsten Totating Anode)\r\n - Anode Heat Storage : 300kHU\r\n - Additional Filtration : 0.05mm Rh\r\n3) Radiographic Stand (C-arm)\r\n - Vertical Movement : 715 mm (605 ~ 1320 mm) Motorized Moving\r\n - Rotating Degrees : R180°/L180° moterized Moving\r\n - SID : 660mm\r\n - Tissue Compression \r\n    Manual(Max. under 300N / IEC60601-2-45\r\n    Motorized by MICOM control (Max. 200N)\r\n - Positioning condition panel\r\n    3.5" Full Color LCD\r\n    Device condition\r\n    Compression Force Level\r\n    Compression Thickness Data\r\n    Degree Position\r\n    AEC Sensor Position\r\n - Main Operation Panel :7" Full Color Touch LCD\r\n - Internal Colimator Plate : Selectable 24 X 30 cm\r\n4) Bucky Device\r\n - Grid : 4:1, 36Line/cm', 1, 'เครื่อง', '20r50.jpg', '4500000.00', '4500000.00', '4500000.00', 1),
(21, 'HF525P', 'HF525Plus', 'Radiographic kV range Accuracy: 40-125kV\r\nPower Output Ratings(@0.1sec): 500mA@80kV\r\nTube: Focal 1.0/2.0mm(140KHU)\r\nExposure Time Range Accuracy: 0.001-6sec(81steps)\r\nFrequency: 40kHz\r\nmAs Range: 0.1-500mAs\r\nHigh Voltage Ripple: <1kV@100kV\r\nQutomatic Exposure Control: Optional\r\nBucky: Standard\r\nAnatomical Programming: 288 APR(Standard)\r\nLine Voltage: 220VAC, 50/60Hz, Single Phase\r\n.- Automatic Line Compensation: ±15%', 1, 'ชุด', 'r46.jpg', '950000.00', '900000.00', '900000.00', 1),
(22, 'ULH60', 'H60', '1. Main unit\r\n2. Convex Probe(C1-4) for Radiology\r\n3. Linear Probe(L5-13)\r\n5. B/W Thermal Printer\r\n6. High Gross Paper\r\n7. UPS 1KVA\r\n8. Ultasound Gel\r\n9. DMR Function\r\n10. Dicom 3.0\r\n11. Cleansing Paper\r\n', 1, 'เครื่อง', 'r3.jpg', '1700000.00', '1200000.00', '1150000.00', 1),
(23, 'ECOV9C', 'EcoView9 Classic', 'Digital Radiography System with Flat Panel Detectors\r\n1. 630mA/150kV X-Ray system\r\n.- 50kW high frequency x-ray generator with high speed rotar\r\n.- Floor to mounted x-ray tube stand\r\n.- X-ray 4 way floating x-ray table\r\n.- DR grid\r\n2. Universal Detector Stand with Flat panel Detector\r\n.- A-silicon TFT LCD Flat panel Detector(17"x17")\r\n3. Acquisition S/W and Workstation\r\n.- 21" LCD monitor and PC\r\n380VAC 50/60Hz three Phase\r\n', 1, 'เครื่อง', 'r65.jpg', '2900000.00', '0.00', '0.00', 1),
(24, 'ECV9S', 'EcoView9 (Smart)', '1. 50kW(630mA/150kV) X-ray system with LCD OP Console\r\n.- High Speed Starter(included in the generator)\r\n.- 300kHU rotating anode tube(0.6/1.2mm focal spots)\r\n.- Floor mounted tube stand (Electromagnetic breaker)\r\n.- manual collimator\r\n.- HV cable (8m long)\r\n.- 4way floating table with rotating bucky device\r\n.- Wall bucky stand\r\n.- DR grid\r\n2. Portable Detector(1417inch), WiFi type(wireless)\r\n3. DR S/W with workstation 21"LCD monitor\r\n380VAC 50/60Hz Three phase\r\n', 1, 'เครื่อง', '', '3100000.00', '0.00', '0.00', 1),
(25, 'ECV9SD', 'EcoView9 (Smart Dual)', '1. 50kW(630mA/150kV) X-ray system with LCD OP Console\r\n.- High Speed Starter(included in the generator)\r\n.- 300kHU rotating anode tube(0.6/1.2mm focal spots)\r\n.- Floor mounted tube stand (Electromagnetic breaker)\r\n.- manual collimator\r\n.- HV cable (8m long)\r\n.- 4 way floating table\r\n.- Wall bucky stand\r\n.- DR grid x 2ea\r\n.- Workstation with 21"LCD monitor, DR S/W\r\n2. A-si Flat Panel Detector x 2ea(Fixed type)\r\n3. DR S/W with workstation 21" LCD monitor\r\n380 VAC 50/60Hz three phase', 1, 'เครื่อง', '', '3900000.00', '0.00', '0.00', 1),
(26, 'ECV9C', 'EcoView9 (Chest)', '1. 500mA/125kVp X-ray system\r\n.- 40kW high frequency x-rya generator\r\n.- X-ray tube: 1.0/2.0mm focal spot, 140kHU\r\n2. Synchronized Tube Stand and Detector Stand\r\n3. A-si Flat Panel Detector (1717), Fixed type\r\n.- Ecoview DR S/W and Workstation\r\n220VAC 50/60Hz single phase\r\n', 1, 'เครื่อง', 'r72.jpg', '2500000.00', '0.00', '0.00', 1),
(27, 'DYDR', 'Dynamic DR', '1. Digital Radiography\r\n2. Digital Fluoroscopy\r\n3. Digital Gastrointestinal\r\n4. Digital Angiography\r\n.- Digital Detector: Blue Plus CCD\r\n.- Effective View Field: 17x17"\r\n.- Static Pixel Matrix: 3072*3072\r\n.- Output Gray: 16 Bit\r\n.- Gemnerator: High Frequency X-ray Machine: 50kW\r\n.- Max output Current: 630mA\r\n.- X-ray Tube: Focus: 0.6mm/1.2mm\r\n.- Max OutPut Voltage: 150kV\r\n', 1, 'เครื่อง', 'r27.jpg', '5500000.00', '0.00', '0.00', 1),
(28, 'PLX7000A', 'PLX-7000A', '1. High Frequency Inverter Power Supply\r\n.- Output Power: 6kW\r\n.- Inverter Frequency: 60kHz\r\n2. Automatic, Manual Continuos Fluoroscopy\r\n.- Tube voltage: 40kV~125kV\r\n.- Tube current: 0.3mA~4mA\r\n3. Automatic, Manual Intensifying Fluoroscopy\r\n.- Tube voltage: 40kV~125kV\r\n.- Tube current: 0.3mA~8mA\r\n4. Automatic, Manual Pulse Fluoroscopy\r\n.- Tube voltage: 40kV~125kV\r\n.- Tube current: 0.3mA~8mA\r\n.- Pulse Frequency: 0.1-10frame/s\r\n5. Radiography Tube Voltage, mA: 40kV~125kV, 120mA\r\n6. X-Ray Tube Special For High Frequency\r\n.- Rotary anode focus: 0.3/0.6\r\n.- Anode thermal capacity: 212kJ\r\n', 1, 'เครื่อง', '', '1900000.00', '0.00', '0.00', 1),
(29, 'PLX7000B', 'PLX-7000B', '1. High Frequency Inverter Power Supply\r\n.- Output Power: 12kW\r\n.- Inverter Frequency: 60kHz\r\n2. Automatic, Manual Continuos Fluoroscopy\r\n.- Tube voltage: 40kV~125kV\r\n.- Tube current: 0.3mA~4mA\r\n3. Automatic, Manual Intensifying Fluoroscopy\r\n.- Tube voltage: 40kV~125kV\r\n.- Tube current: 0.3mA~8mA\r\n4. Automatic, Manual Pulse Fluoroscopy\r\n.- Tube voltage: 40kV~125kV\r\n.- Tube current: 0.3mA~30mA\r\n.- Pulse Frequency: 0.1-10frame/s\r\n5. DSI Digital Spot File: 0.1-10 frame/s\r\n6. Radiography Tube Voltage, mA: 40kV~125kV, 160mA\r\n7. X-Ray Tube Special For High Frequency\r\n.- Rotary anode focus: 0.3/0.6\r\n.- Anode thermal capacity: 212kJ\r\n', 1, 'เครื่อง', 'r60.jpg', '2400000.00', '0.00', '0.00', 1),
(30, 'PLX7000C', 'PLX-7000C', '1. High Frequency Inverter Power Supply\r\n.- Output Power: 16kW\r\n.- Inverter Frequency: 60kHz\r\n2. Automatic, Manual Continuos Fluoroscopy\r\n.- Tube voltage: 40kV~125kV\r\n.- Tube current: 0.3mA~4mA\r\n3. Automatic, Manual Intensifying Fluoroscopy\r\n.- Tube voltage: 40kV~125kV\r\n.- Tube current: 0.3mA~8mA\r\n4. Automatic, Manual Pulse Fluoroscopy\r\n.- Tube voltage: 40kV~125kV\r\n.- Tube current: 0.3mA~30mA\r\n.- Pulse Frequency: 0.1-10frame/s\r\n5. DSI Digital Spot File: 0.1-10 frame/s\r\n6. Radiography Tube Voltage, mA: 40kV~125kV, 200mA\r\n7. X-Ray Tube Special For High Frequency\r\n.- Rotary anode focus: 0.3/0.6\r\n.- Anode thermal capacity: 212kJ\r\n', 1, 'เครื่อง', 'r35.jpg', '2800000.00', '0.00', '0.00', 1),
(31, 'SPN3G', 'Spinel 3G', '1. H.V Generator\r\nHigh Frequency Inverter type : 40kHz, Max.12KW\r\nRadio-mode: kVp output: 40-120kVp\r\n                      mA output: 20-150mA\r\nFluoro-mode: kVp output: 40-120kVp\r\n                     mA output: 0.5-5.0mA/8mA(SNAP SHOT)/20mA(BOOST)\r\n2. X_Ray tube\r\n3. Image Intersifier :9" (3 field : 9"-6"-4.5")\r\n4.CCD Camera - 1k\r\n5. Digital Image System\r\n6. ABS (Automatic Brightness Control) system\r\n7. Memory fuction - Last image holder\r\n8. X-Ray Monitor : B/W TFT 19" medical monitors* 2(1000cd/cm2)\r\n9.Power requirement : 1Ø,200V , 50/60Hz\r\n10. Laser Guide\r\n11. DSA(Digital Substraction Angiography) Option\r\n', 1, 'เครื่อง', 'r1.jpg', '2700000.00', '0.00', '0.00', 1),
(32, 'SPN12HD', 'Spinel 12HD', '1. H.V Generator\r\nHigh Frequency Inverter type : 40kHz, Max.12KW\r\nRadio-mode: kVp output: 40-120kVp\r\n                      mA output: 20-150mA\r\nFluoro-mode: kVp output: 40-120kVp\r\n                     mA output: 0.5-5.0mA/8mA(SNAP SHOT)/20mA(BOOST)\r\n2. X-Ray tube\r\n - Focal spot: 0.3/0.6mm\r\n - Anode HU: 300KHU\r\n3.Fluoroscopy TFT Detector\r\n- Cesium Iodide (Csl)\r\n"- Active Area : 261(H)×287(V)mm \r\n(10.3 x 11.3 inch)"\r\n- Pixel Size :  184 µm, 1420(H)×1560(V) pixels\r\n4. OP Control Panel\r\n5. Digital Imaging System\r\n6. ABS (Automatic Brightness Control) system\r\n7. Laser Guide\r\n8. X-ray Monitor : B/W TFT 19" medical monitors * 2(1000cd/cm2) \r\n9. Power requirement : 1Ø, 220V, 50/60Hz\r\n10. Premium Monitor Cart\r\n', 1, 'เครื่อง', 'r24.jpg', '5800000.00', '0.00', '0.00', 1),
(33, 'SOUL', 'soul', 'C-Arm Assembly\r\n.- Vertical Range: 640mm to 1490mm Motorized\r\n.- SID: 650mm±10mm\r\n.- Rotation Angle: +180 to -180, Motorized, Isocentric\r\n.- Safe Handles: 2ea\r\n.- Vertical Range of Compression Device: 320mm\r\n.- Face Shield: Removable\r\nX-Ray Tube\r\n.- Target Material: Tungsten(W)\r\n.- Filteration: Rhodium/Silver(Rh/Ag)\r\n.- Anode: Rotation\r\n.- Focal Spot Size: Large 0.3mm, Small 0.1mm\r\n.- Target Tube Angle: Large 16, Small 10\r\n.- Anode Heat Capacity: 300,000HU\r\nGenerator\r\n.- Type: Ultra High Frequency Inverter, 100kHz\r\n.- mA Range: Large 10mA to 200mA, Small 10mA to 60mA\r\n.- mAs Range: 1-600mAs\r\n.- kVp Range: 20kVp to 40kVp(9.5Vp Step)\r\n.- Power Input Required: 180-264VAC(50/60Hz), 6kVA(5KW), 1Phase\r\n.- Exposure Mode: Auto, Manual\r\nDisplay Module\r\n.- Main Display: 7 segment\r\nCollimation System\r\n.- Mode: Fully Automatic or User Selectable\r\nCompression System\r\n.- Compression Type: Motorized and Manual\r\n.- Max Compression Force: 20daN\r\n.- Pre-Compression: Smart compression control technology\r\n.- Compression Release: On/Off, Set up the Height\r\nMagnification\r\n.- Magnification Factor: 1.8x, 1.5x\r\n', 1, 'เครื่อง', '', '4800000.00', '0.00', '0.00', 1),
(34, 'T7000', 'T7000', 'Operating Table with Embedded Battery\r\nMain Device\r\n.- Electro-Hydraulic Operating Table\r\n.- Remote Controll wire/wireless Button Type\r\n.- Lateral/Tendelenburg ±18°/±28°\r\n.- Back section refraction +80°~-40°\r\n.- Leg sention refraction +15°~-90°\r\n.- Head section refraction +15°~-90°\r\n', 1, 'เตียง', 'r96.jpg', '1200000.00', '0.00', '0.00', 1),
(35, 'T2000', 'T2000', 'Main Device\r\n.- Electro-Hydraulic Operating Table\r\n.- Lateral/Tendelenburg ±19°/±29°\r\n.- Back section refraction +70°~-40°\r\n.- Leg sention refraction +15°~-90°\r\n.- Head section refraction +15°~-90°\r\n', 1, 'เตียง', 'r65.jpg', '580000.00', '0.00', '0.00', 1),
(36, 'E1000', 'E1000', '1. Feature\r\n● Wireless foot switch system\r\n● Display lifting height and back plate degree for easy operation\r\n● Pre-setting function for preferred height and backplate degree\r\n● Arm rest for convenience of patient\r\n2. Dimension\r\nOverall Size      W650*L1270*H480~1015mm\r\nTable Top    W650*L1270mm\r\nBack Section Refraction    . -5°~50°\r\nHip Section Refraction     0°~30°\r\nPower Source    AC220V, 50/60Hz\r\nConsumption    480W\r\nFuse    5A\r\nNet Weight      About 135kg\r\nAccessories Weight    About 35kg\r\n', 1, 'เตียง', 'r76.jpg', '320000.00', '0.00', '0.00', 1),
(37, 'EX820', 'EX820', '1. Feature\r\n● Electro-hydra system\r\n● Memory function for automatic positioning\r\n● Comportable with low noise during operation\r\n● Can be used for operating table in emergency\r\n● Seat Warming System\r\n● Urology available\r\n2. Dimension\r\nOverall Size    W655*L1250*H460~1015mm\r\nTable Top     W655*L1250mm\r\nBack Section Refraction   . -10°~50°\r\nHip Section Refraction     0°~30°\r\nPower Source    AC220V, 50/60Hz\r\nConsumption   500W\r\nSeat Warming System     Electro-Hydrauric Mechanism', 1, 'เตียง', 'r73.jpg', '290000.00', '0.00', '0.00', 1),
(38, 'KTR240FEE', 'KT-R240FEE', '2MP Medical Monitor\r\n27 inch LED Color IPS mode\r\n.- Pixel Pitch: 0.270(H) x 0.270(V) mm\r\n.- Contrast Ratio: 1000:1\r\n.- Luminance(Max): 350cd/m2\r\n', 1, 'ตัว', 'r31.jpg', '80000.00', '0.00', '0.00', 1),
(39, 'KTD213Q5E', 'KT-D213Q5E', '3MP Medical Monitor\r\n.- CMC-3M\r\n.- 21.3inch TFT monochrome LCD IPS mode\r\n.- Display Area: 433.152(H) x 324.864(V) mm\r\n.- Aspect Ratio: 4:3\r\n', 1, 'จอ', 'r95.jpg', '190000.00', '0.00', '0.00', 1),
(40, 'KTD213V5E', 'KT-D213V5E', '5MP Medical Monitor\r\n.- CMC-5M\r\n.- 21.3inch TFT monochrome LCD IPS mode\r\n.- Display Area: 422.4(H) x 337.9(V) mm\r\n.- Aspect Ratio: 5:4\r\n', 1, 'จอ', 'r78.jpg', '300000.00', '0.00', '0.00', 1),
(41, '1417WCC', '1417WCC', '14x17 in\r\n423 x 358 mm\r\nAmorphous Silicon with TFT\r\nCesium-Iodide(Cesium)\r\n< 5.0sec. Data Acquisition time\r\nmin. 50% / type 65% DQE\r\nWireless type\r\nManual Mode & Auto Trigger Mode\r\n', 1, 'ชิ้น', 'r10.jpg', '1600000.00', '0.00', '0.00', 1),
(42, 'EVS4343', 'Exprimer EVS4343', '.- Pixel Pitch: 140nm\r\n.- Scintillator: CsI(Cesium Iodide)\r\n.- Image Matix Size: 3072 x 3072 pixels\r\n.- Effective Imaging Area(HxV): 430 x 430 mm\r\n.- Image Acquisition and Transfer Time: <3 sec.\r\n.- Spartial Resolution: Min. 3.5 line pair/mm\r\n.- Power Supply: DC+12V 2A Max.\r\n.- Power Consumption: 12W\r\n.- Network Interface: Gigabit Ethernet(1000Base-T)\r\n.- Dimension(mm): 460 x 460 x 15\r\n.- Weight: 4.5kg\r\n', 1, 'ชิ้น', 'r78.jpg', '1300000.00', '0.00', '0.00', 1),
(43, '1717SCC', '1717SCC', '17x17 in\r\n423 x 423 mm\r\nAmorphous Silicon with TFT\r\nCsI:TI\r\n<3.9kg\r\n3328 x 3328 pixel\r\n127nm\r\n40-150kV\r\n14Bit\r\n< 5.0sec.\r\ntype 65%\r\nManual Mode & Auto Trigger Mode\r\nIndirect Type \r\nPortable type\r\n', 1, 'ชิ้น', 'r84.jpg', '1400000.00', '0.00', '0.00', 1),
(44, 'FIRECR40', 'FIRECR40', 'Up to 30 plate/hour(IP 14x17")\r\nUniversal Cassette1417 & 1014\r\nData Capture 16bit per pixel, 65,000graytone\r\nDesktop & vertical wall mount set up type\r\nDICOM3.0 compliant\r\nDICOM Query send, receive\r\n1417 cassette 1ea 1014 cassette 1ea\r\n', 1, 'เครื่อง', NULL, '620000.00', '0.00', '0.00', 1),
(45, 'FIRECR60', 'FIRECR60', 'Up to 50 plate/hour(IP 14x17")\r\nUniversal Cassette1417 & 1014\r\nData Capture 16bit per pixel, 65,000graytone\r\nDesktop & vertical wall mount set up type\r\nDICOM3.0 compliant\r\nDICOM Query send, receive\r\n1417 cassette 1ea 1014 cassette 1ea\r\n', 1, 'เครื่อง', 'r43.jpg', '710000.00', '0.00', '0.00', 1),
(46, 'FireCR80', 'FireCR80', 'Up to 70 plate/hour(IP 14x17")\r\nUniversal Cassette1417 & 1014\r\nData Capture 16bit per pixel, 65,000graytone\r\nDesktop & vertical wall mount set up type\r\nDICOM3.0 compliant\r\nDICOM Query send, receive\r\n1417 cassette 1ea 1014 cassette 1ea\r\n', 1, 'เครื่อง', 'r10.jpg', '810000.00', '0.00', '0.00', 1),
(47, 'OSO PRO', 'Osteo Pro', '.- Classification: Claas 1, Type BF\r\n.- Ultrasound Parameter: BUA(Broadband Ultrasound Attenuation)\r\n.- SOS(Speed of Sound)\r\n.- OI(Osteoporosis Index)\r\n.- Diagnosing Parameter: OI, SOS, T-Score, Z-Score, %Young Adult, %Age Match, OPR\r\n.- Method and Transducer: Single Element Flat Type, Center Frequency\r\n.- Measurement Time: 15sec.\r\n.- Precision OI-in vivo <0.7%\r\n.- BUA - in vivo<0.2%\r\n.- SOS - in vivo<0.2%\r\n', 1, 'เครื่อง', 'r61.jpg', '240000.00', '0.00', '0.00', 1),
(48, 'ACV A30', 'Accuvix A30', '1. Main Unit\r\n2. 3D Convex Probe(3DV4-8 or 3DV2-6) and 4D Option\r\n3. Covex Probe(C2-6IC) for OB/Gyn\r\n4. Vaginal Probe(VR5-9)\r\n5. USB DMR\r\n6. B/W Printer + Paper 4 rolls\r\n7. Gel 5 Liter\r\n8. UPS 2K\r\n9. Dicom\r\n', 1, 'เครื่อง', 'r48.jpg', '3200000.00', '0.00', '0.00', 1),
(49, 'R7', 'R7', '.- USB DMR\r\n.- B/W Printer + Paper 4 rolls\r\n.- Gel 5 Liter\r\n.- UPS 1K\r\n.- 3D Convex Probe and 3D Option\r\n.- Covex Probe(C2-8)\r\n.- Vaginal Probe(EV4-9)\r\n.- Dicom support\r\n', 1, 'เครื่อง', 'r89.jpg', '1500000.00', '0.00', '0.00', 1),
(50, 'HLUXLED120', 'Honey LUX LED 120', '.- Total Power Input: 160W\r\n.- Housing Diameter: 620mm\r\n.- Housing Controller: Membrance\r\n.- Wall Controller: Touch Button\r\n.- Max Illumination: 120,000Lux\r\n.- The number of LED: 88\r\n.- Service Life: 30,000 Hrs\r\n.- Focusing Function\r\n.- Light Field Diameter: 22-30cm\r\n.- Color Temperature: 4,300k\r\n.- CRI(Ra): >94\r\n.- Illumination Depth: 150cm\r\n.- Endo Light Function: 10Steps(5-100%)\r\n', 1, 'ชิ้น', '', '800000.00', '0.00', '0.00', 1),
(51, 'LUXLED 160', 'Honey LUX LED 160', '.- Total Power Input: 200W\r\n.- Housing Diameter: 770mm\r\n.- Housing Controller: Membrance\r\n.- Wall Controller: Touch Button\r\n.- Max Illumination: 160,000Lux\r\n.- The number of LED: 120\r\n.- Service Life: 30,000 Hrs\r\n.- Focusing Function\r\n.- Light Field Diameter: 25-34cm\r\n.- Color Temperature: 4,300k\r\n.- CRI(Ra): >94\r\n.- Illumination Depth: 113cm\r\n.- Endo Light Function: 10Steps(5-100%)\r\n', 1, 'ชิ้น', '', '900000.00', '0.00', '0.00', 1),
(52, 'DLUX D50', 'Dialux D50', '.- Diameter of Housing(mm): 500\r\n.- Power Input: AC220V, 50/60Hz\r\n.- Max Illumination: 80,000Lux\r\n.- Color Temperature: 4,300k\r\n.- Power of Bulb: 120W\r\n.- Teperature Rise: <3°C\r\n.- Lighting adjusting mode: 10 steps\r\n.- Focusable Light Field: 120-180mm\r\n.- Life of Bulb: 1,000hrs\r\n', 1, 'ชิ้น', '', '550000.00', '0.00', '0.00', 1),
(53, 'DLUX D70', 'Dialux D70', '.- Diameter of Housing(mm): 700\r\n.- Power Input: AC220V, 50/60Hz\r\n.- Max Illumination: 150,000Lux\r\n.- Color Temperature: 4,300k\r\n.- Power of Bulb: 150W\r\n.- Teperature Rise: <3°C\r\n.- Lighting adjusting mode: 10 steps\r\n.- Focusable Light Field: 140-200mm\r\n.- Life of Bulb: 1,000hrs\r\n', 1, 'ชิ้น', 'r17.jpg', '600000.00', '0.00', '0.00', 1),
(54, 'PLD8600', 'PLD8600', 'X-Ray Generator(CPI Canada)\r\nTube(Toshiba)\r\nImage Intensifier(Toshiba)\r\nCCD(Adimec Mega Pixel CCD_Netherland)\r\n1. Power Output: 80kW\r\n2. Inverter Frequency: 200kHz\r\n3. Dual Focus: Large focus: 0.6, Small focus: 1.2\r\n4. Voltage: 380V±38V\r\n5. Frequency: 50Hz±1Hz\r\n6. Photography\r\n   6.1 Tube Voltage: 40kV~150kV\r\n   6.2 Tube Current: 10mA~1000mA\r\n   6.3 Exposure time: 1.0ms~6300ms\r\n   6.4 Control Interface: LCD touch screen\r\n7. Fluoroscopy\r\n   7.1 Tube Voltage: 40kV~125kV\r\n   7.2 Tube Current: 0.5mA~6mA\r\n  7.3 Qutomatic Brightness Tracking multiple setting beforehand\r\n', 1, 'เครื่อง', '', '4400000.00', '0.00', '0.00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `prdimage`
--

CREATE TABLE IF NOT EXISTS `prdimage` (
  `PRD_IMAGE_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `PRDCODE` bigint(20) NOT NULL,
  `PRD_IMAGE_PATH` varchar(255) NOT NULL,
  `STATUS` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`PRD_IMAGE_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `prdorder`
--

CREATE TABLE IF NOT EXISTS `prdorder` (
  `ORDERID` bigint(10) NOT NULL AUTO_INCREMENT,
  `DR_PROVIDER` int(11) DEFAULT NULL,
  `D_ORDER` datetime DEFAULT NULL,
  `CONFIRM_PATH` varchar(250) DEFAULT NULL,
  `CONFIRM_STATUS` tinyint(1) NOT NULL DEFAULT '0',
  `STATUS` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`ORDERID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=45 ;

--
-- Dumping data for table `prdorder`
--

INSERT INTO `prdorder` (`ORDERID`, `DR_PROVIDER`, `D_ORDER`, `CONFIRM_PATH`, `CONFIRM_STATUS`, `STATUS`) VALUES
(30, 2, '2015-05-07 17:36:37', NULL, 0, 1),
(29, 2, '2015-05-07 17:20:01', NULL, 0, 1),
(28, 2, '2015-04-29 22:40:20', NULL, 0, 1),
(41, 3, '2015-05-19 11:46:05', NULL, 0, 1),
(26, 2, '2015-04-29 22:33:36', NULL, 0, 1),
(24, 2, '2015-04-29 22:26:21', '24r52.jpg', 1, 1),
(42, 3, '2015-05-19 12:00:19', NULL, 0, 1),
(43, 3, '2015-05-20 13:54:37', NULL, 0, 1),
(44, 3, '2015-06-01 23:23:00', NULL, 0, 1),
(31, 2, '2015-05-07 18:56:01', NULL, 0, 1),
(32, 2, '2015-05-08 17:26:58', NULL, 0, 1),
(33, 2, '2015-05-08 17:42:08', NULL, 0, 1),
(34, 2, '2015-05-08 17:45:51', NULL, 0, 1),
(35, 2, '2015-05-08 18:18:43', NULL, 0, 1),
(36, 2, '2015-05-08 18:21:58', NULL, 0, 1),
(37, 2, '2015-05-08 18:23:46', NULL, 0, 1),
(38, 2, '2015-05-08 18:24:14', NULL, 0, 1),
(39, 2, '2015-05-12 18:04:38', NULL, 0, 1),
(40, 3, '2015-05-19 08:21:01', '40r38.pdf', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `prdorderdt`
--

CREATE TABLE IF NOT EXISTS `prdorderdt` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ORDERID` bigint(20) DEFAULT NULL,
  `PRDCODE` bigint(20) DEFAULT NULL,
  `QTY` float DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=88 ;

--
-- Dumping data for table `prdorderdt`
--

INSERT INTO `prdorderdt` (`ID`, `ORDERID`, `PRDCODE`, `QTY`) VALUES
(80, 41, 10, 10),
(86, 44, 18, 1),
(85, 43, 21, 2),
(84, 43, 18, 1),
(87, 44, 21, 1),
(79, 41, 18, 2),
(26, 24, 4, 2),
(27, 24, 3, 2),
(83, 42, 5, 7),
(82, 42, 21, 8),
(30, 26, 5, 1),
(81, 42, 18, 1),
(32, 28, 5, 10),
(33, 28, 7, 10),
(34, 29, 9, 2),
(35, 29, 11, 1),
(36, 29, 7, 4),
(37, 29, 13, 5),
(38, 29, 5, 7),
(39, 29, 15, 8),
(40, 30, 5, 80),
(41, 30, 6, 50),
(42, 31, 5, 10),
(43, 31, 6, 20),
(44, 31, 7, 30),
(45, 31, 8, 40),
(46, 31, 9, 50),
(47, 31, 10, 60),
(48, 31, 11, 70),
(49, 31, 12, 80),
(50, 31, 13, 90),
(51, 31, 14, 100),
(52, 31, 15, 200),
(53, 31, 1, 300),
(54, 31, 2, 400),
(55, 31, 16, 500),
(56, 31, 17, 600),
(57, 31, 3, 700),
(58, 31, 4, 800),
(59, 32, 5, 1),
(60, 32, 7, 2),
(61, 32, 9, 3),
(62, 32, 12, 4),
(63, 32, 6, 5),
(64, 33, 5, 2),
(65, 34, 5, 1),
(66, 35, 5, 1),
(67, 35, 6, 2),
(68, 35, 8, 3),
(69, 36, 6, 2),
(70, 36, 7, 1),
(71, 37, 5, 9),
(72, 37, 6, 8),
(73, 38, 5, 10),
(74, 38, 6, 9),
(75, 39, 2, 12),
(76, 39, 3, 1),
(77, 40, 5, 100),
(78, 40, 14, 5);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `USER_ID` int(15) NOT NULL AUTO_INCREMENT COMMENT 'PK',
  `USER_ACCOUNT` varchar(15) DEFAULT NULL COMMENT 'ชื่อ login',
  `USER_PASSWORD` varchar(15) DEFAULT NULL COMMENT 'password',
  `USER_NAME` varchar(50) DEFAULT NULL COMMENT 'Name',
  `USER_LNAME` varchar(50) DEFAULT NULL COMMENT 'Surname',
  `USER_COMPANY` varchar(150) DEFAULT NULL,
  `USER_ADDRESS` text COMMENT 'Address',
  `USER_TEL` varchar(50) DEFAULT NULL COMMENT 'Telephone',
  `USER_EMAIL` varchar(50) DEFAULT NULL COMMENT 'Email',
  `USER_ADD_DATE` datetime DEFAULT NULL COMMENT 'become member date',
  `USER_EDIT_DATE` datetime DEFAULT NULL COMMENT 'edit member date',
  `STATUS` tinyint(1) NOT NULL,
  PRIMARY KEY (`USER_ID`),
  UNIQUE KEY `USER_ACCOUNT` (`USER_ACCOUNT`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`USER_ID`, `USER_ACCOUNT`, `USER_PASSWORD`, `USER_NAME`, `USER_LNAME`, `USER_COMPANY`, `USER_ADDRESS`, `USER_TEL`, `USER_EMAIL`, `USER_ADD_DATE`, `USER_EDIT_DATE`, `STATUS`) VALUES
(2, 'test', '1234', 'ทดสอบ', 'ระบบ', 'โรงพยาบาลสุโขทัยธรรมธิราชนครินทร์', '127/55 หมู่บ้านสายลม ตำบลสายรัก อำเภออกหัก จังหวัดรักเธอ นะจ๊ะ', '074-225776-896', 'example@example.com', '2015-05-03 16:08:00', '2015-05-19 08:28:36', 1),
(3, 'chavee', '1234', 'ชาวี', 'อมราภรณ์', 'อมราภรณ์ คอเปอเรชั่น', 'อมราภรณ์คอเปอเรชั่น แขวงบางคอแหลม ลาดปลาเค้า กรุงเทพมหานคร 10400', '02-6665555', 'chavee@hotmail.com', '2015-05-18 22:34:14', '2015-05-18 22:36:02', 1),
(4, 'araya', '1234', 'อารยา', 'อมราภรณ์', 'อมราภรณ์ คอเปอเรชั่น', 'อมราภรณ์คอเปอเรชั่น แขวงบางคอแหลม ลาดปลาเค้า กรุงเทพมหานคร 10400', '02-7778899', 'araya_not_chompoo@gmail.com', '2015-05-18 22:38:22', NULL, 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
